"""Unit tests of relationship managers."""


import unittest
from dlkit_runtime import PROXY_SESSION, proxy_example
from dlkit_runtime.managers import Runtime
REQUEST = proxy_example.TestRequest()
CONDITION = PROXY_SESSION.get_proxy_condition()
CONDITION.set_http_request(REQUEST)
PROXY = PROXY_SESSION.get_proxy(CONDITION)

from dlkit.primordium.type.primitives import Type
DEFAULT_TYPE = Type(**{'identifier': 'DEFAULT', 'namespace': 'DEFAULT', 'authority': 'DEFAULT',})

from dlkit.abstract_osid.type.objects import TypeList as abc_type_list
from dlkit.abstract_osid.osid import errors


class TestRelationshipProfile(unittest.TestCase):
    """Tests for RelationshipProfile"""

    @classmethod
    def setUpClass(cls):
        cls.mgr = Runtime().get_service_manager('RELATIONSHIP', proxy=PROXY, implementation='TEST_SERVICE')

    def test_supports_relationship_lookup(self):
        """Tests supports_relationship_lookup"""
        self.assertTrue(isinstance(self.mgr.supports_relationship_lookup(), bool))

    def test_supports_relationship_admin(self):
        """Tests supports_relationship_admin"""
        self.assertTrue(isinstance(self.mgr.supports_relationship_admin(), bool))

    def test_supports_family_lookup(self):
        """Tests supports_family_lookup"""
        self.assertTrue(isinstance(self.mgr.supports_family_lookup(), bool))

    def test_supports_family_admin(self):
        """Tests supports_family_admin"""
        self.assertTrue(isinstance(self.mgr.supports_family_admin(), bool))

    def test_supports_family_hierarchy(self):
        """Tests supports_family_hierarchy"""
        self.assertTrue(isinstance(self.mgr.supports_family_hierarchy(), bool))

    def test_supports_family_hierarchy_design(self):
        """Tests supports_family_hierarchy_design"""
        self.assertTrue(isinstance(self.mgr.supports_family_hierarchy_design(), bool))

    def test_get_relationship_record_types(self):
        """Tests get_relationship_record_types"""
        self.assertTrue(isinstance(self.mgr.get_relationship_record_types(), abc_type_list))

    def test_get_relationship_search_record_types(self):
        """Tests get_relationship_search_record_types"""
        self.assertTrue(isinstance(self.mgr.get_relationship_search_record_types(), abc_type_list))

    def test_get_family_record_types(self):
        """Tests get_family_record_types"""
        self.assertTrue(isinstance(self.mgr.get_family_record_types(), abc_type_list))

    def test_get_family_search_record_types(self):
        """Tests get_family_search_record_types"""
        self.assertTrue(isinstance(self.mgr.get_family_search_record_types(), abc_type_list))


class TestRelationshipManager(unittest.TestCase):
    """Tests for RelationshipManager"""

    @classmethod
    def setUpClass(cls):
        cls.svc_mgr = Runtime().get_service_manager('RELATIONSHIP', proxy=PROXY, implementation='TEST_SERVICE')
        create_form = cls.svc_mgr.get_family_form_for_create([])
        create_form.display_name = 'Test Family'
        create_form.description = 'Test Family for relationship manager tests'
        catalog = cls.svc_mgr.create_family(create_form)
        cls.catalog_id = catalog.get_id()
        cls.mgr = Runtime().get_manager('RELATIONSHIP', 'TEST_MONGO_1', (3, 0, 0))

    @classmethod
    def tearDownClass(cls):
        cls.svc_mgr.delete_family(cls.catalog_id)

    def test_get_relationship_lookup_session(self):
        """Tests get_relationship_lookup_session"""
        if self.mgr.supports_relationship_lookup():
            self.mgr.get_relationship_lookup_session()

    def test_get_relationship_lookup_session_for_family(self):
        """Tests get_relationship_lookup_session_for_family"""
        if self.mgr.supports_relationship_lookup():
            self.mgr.get_relationship_lookup_session_for_family(self.catalog_id)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_relationship_lookup_session_for_family()

    def test_get_relationship_admin_session(self):
        """Tests get_relationship_admin_session"""
        if self.mgr.supports_relationship_admin():
            self.mgr.get_relationship_admin_session()

    def test_get_relationship_admin_session_for_family(self):
        """Tests get_relationship_admin_session_for_family"""
        if self.mgr.supports_relationship_admin():
            self.mgr.get_relationship_admin_session_for_family(self.catalog_id)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_relationship_admin_session_for_family()

    def test_get_family_lookup_session(self):
        """Tests get_family_lookup_session"""
        if self.mgr.supports_family_lookup():
            self.mgr.get_family_lookup_session()

    def test_get_family_admin_session(self):
        """Tests get_family_admin_session"""
        if self.mgr.supports_family_admin():
            self.mgr.get_family_admin_session()

    def test_get_family_hierarchy_session(self):
        """Tests get_family_hierarchy_session"""
        if self.mgr.supports_family_hierarchy():
            self.mgr.get_family_hierarchy_session()

    def test_get_family_hierarchy_design_session(self):
        """Tests get_family_hierarchy_design_session"""
        if self.mgr.supports_family_hierarchy_design():
            self.mgr.get_family_hierarchy_design_session()

    def test_get_relationship_batch_manager(self):
        """Tests get_relationship_batch_manager"""
        if self.mgr.supports_relationship_batch():
            self.mgr.get_relationship_batch_manager()

    def test_get_relationship_rules_manager(self):
        """Tests get_relationship_rules_manager"""
        if self.mgr.supports_relationship_rules():
            self.mgr.get_relationship_rules_manager()


class TestRelationshipProxyManager(unittest.TestCase):
    """Tests for RelationshipProxyManager"""

    @classmethod
    def setUpClass(cls):
        cls.svc_mgr = Runtime().get_service_manager('RELATIONSHIP', proxy=PROXY, implementation='TEST_SERVICE')
        create_form = cls.svc_mgr.get_family_form_for_create([])
        create_form.display_name = 'Test Family'
        create_form.description = 'Test Family for relationship proxy manager tests'
        catalog = cls.svc_mgr.create_family(create_form)
        cls.catalog_id = catalog.get_id()
        cls.mgr = Runtime().get_proxy_manager('RELATIONSHIP', 'TEST_MONGO_1', (3, 0, 0))

    @classmethod
    def tearDownClass(cls):
        cls.svc_mgr.delete_family(cls.catalog_id)

    def test_get_relationship_lookup_session(self):
        """Tests get_relationship_lookup_session"""
        if self.mgr.supports_relationship_lookup():
            self.mgr.get_relationship_lookup_session(PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_relationship_lookup_session()

    def test_get_relationship_lookup_session_for_family(self):
        """Tests get_relationship_lookup_session_for_family"""
        if self.mgr.supports_relationship_lookup():
            self.mgr.get_relationship_lookup_session_for_family(self.catalog_id, PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_relationship_lookup_session_for_family()

    def test_get_relationship_admin_session(self):
        """Tests get_relationship_admin_session"""
        if self.mgr.supports_relationship_admin():
            self.mgr.get_relationship_admin_session(PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_relationship_admin_session()

    def test_get_relationship_admin_session_for_family(self):
        """Tests get_relationship_admin_session_for_family"""
        if self.mgr.supports_relationship_admin():
            self.mgr.get_relationship_admin_session_for_family(self.catalog_id, PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_relationship_admin_session_for_family()

    def test_get_family_lookup_session(self):
        """Tests get_family_lookup_session"""
        if self.mgr.supports_family_lookup():
            self.mgr.get_family_lookup_session(PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_family_lookup_session()

    def test_get_family_admin_session(self):
        """Tests get_family_admin_session"""
        if self.mgr.supports_family_admin():
            self.mgr.get_family_admin_session(PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_family_admin_session()

    def test_get_family_hierarchy_session(self):
        """Tests get_family_hierarchy_session"""
        if self.mgr.supports_family_hierarchy():
            self.mgr.get_family_hierarchy_session(PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_family_hierarchy_session()

    def test_get_family_hierarchy_design_session(self):
        """Tests get_family_hierarchy_design_session"""
        if self.mgr.supports_family_hierarchy_design():
            self.mgr.get_family_hierarchy_design_session(PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_family_hierarchy_design_session()

    def test_get_relationship_batch_proxy_manager(self):
        """Tests get_relationship_batch_proxy_manager"""
        if self.mgr.supports_relationship_batch():
            self.mgr.get_relationship_batch_proxy_manager()

    def test_get_relationship_rules_proxy_manager(self):
        """Tests get_relationship_rules_proxy_manager"""
        if self.mgr.supports_relationship_rules():
            self.mgr.get_relationship_rules_proxy_manager()


