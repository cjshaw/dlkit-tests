"""Unit tests of relationship objects."""


import unittest


class TestRelationship(unittest.TestCase):
    """Tests for Relationship"""


    @unittest.skip('unimplemented test')
    def test_get_source_id(self):
        """Tests get_source_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_destination_id(self):
        """Tests get_destination_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationship_record(self):
        """Tests get_relationship_record"""
        pass


class TestRelationshipForm(unittest.TestCase):
    """Tests for RelationshipForm"""


    @unittest.skip('unimplemented test')
    def test_get_relationship_form_record(self):
        """Tests get_relationship_form_record"""
        pass


class TestRelationshipList(unittest.TestCase):
    """Tests for RelationshipList"""

    @unittest.skip('unimplemented test')
    def test_get_next_relationship(self):
        """Tests get_next_relationship"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_relationships(self):
        """Tests get_next_relationships"""
        pass


class TestFamily(unittest.TestCase):
    """Tests for Family"""


    @unittest.skip('unimplemented test')
    def test_get_family_record(self):
        """Tests get_family_record"""
        pass


class TestFamilyForm(unittest.TestCase):
    """Tests for FamilyForm"""


    @unittest.skip('unimplemented test')
    def test_get_family_form_record(self):
        """Tests get_family_form_record"""
        pass


class TestFamilyList(unittest.TestCase):
    """Tests for FamilyList"""

    @unittest.skip('unimplemented test')
    def test_get_next_family(self):
        """Tests get_next_family"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_families(self):
        """Tests get_next_families"""
        pass


