"""Unit tests of relationship queries."""


import unittest


class TestRelationshipQuery(unittest.TestCase):
    """Tests for RelationshipQuery"""

    @unittest.skip('unimplemented test')
    def test_match_source_id(self):
        """Tests match_source_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_source_id_terms(self):
        """Tests clear_source_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_destination_id(self):
        """Tests match_destination_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_destination_id_terms(self):
        """Tests clear_destination_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_same_peer_id(self):
        """Tests match_same_peer_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_same_peer_id_terms(self):
        """Tests clear_same_peer_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_family_id(self):
        """Tests match_family_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_family_id_terms(self):
        """Tests clear_family_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_family_query(self):
        """Tests supports_family_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_family_query(self):
        """Tests get_family_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_family_terms(self):
        """Tests clear_family_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationship_query_record(self):
        """Tests get_relationship_query_record"""
        pass


class TestFamilyQuery(unittest.TestCase):
    """Tests for FamilyQuery"""

    @unittest.skip('unimplemented test')
    def test_match_relationship_id(self):
        """Tests match_relationship_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_relationship_id_terms(self):
        """Tests clear_relationship_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_relationship_query(self):
        """Tests supports_relationship_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationship_query(self):
        """Tests get_relationship_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_relationship(self):
        """Tests match_any_relationship"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_relationship_terms(self):
        """Tests clear_relationship_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_ancestor_family_id(self):
        """Tests match_ancestor_family_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_ancestor_family_id_terms(self):
        """Tests clear_ancestor_family_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_ancestor_family_query(self):
        """Tests supports_ancestor_family_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_ancestor_family_query(self):
        """Tests get_ancestor_family_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_ancestor_family(self):
        """Tests match_any_ancestor_family"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_ancestor_family_terms(self):
        """Tests clear_ancestor_family_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_descendant_family_id(self):
        """Tests match_descendant_family_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_descendant_family_id_terms(self):
        """Tests clear_descendant_family_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_descendant_family_query(self):
        """Tests supports_descendant_family_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_descendant_family_query(self):
        """Tests get_descendant_family_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_descendant_family(self):
        """Tests match_any_descendant_family"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_descendant_family_terms(self):
        """Tests clear_descendant_family_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_family_query_record(self):
        """Tests get_family_query_record"""
        pass


