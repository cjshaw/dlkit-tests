"""Unit tests of relationship sessions."""


import unittest
from dlkit_runtime import PROXY_SESSION, proxy_example
from dlkit_runtime.managers import Runtime
REQUEST = proxy_example.TestRequest()
CONDITION = PROXY_SESSION.get_proxy_condition()
CONDITION.set_http_request(REQUEST)
PROXY = PROXY_SESSION.get_proxy(CONDITION)

from dlkit.primordium.type.primitives import Type
DEFAULT_TYPE = Type(**{'identifier': 'DEFAULT', 'namespace': 'DEFAULT', 'authority': 'DEFAULT',})

from dlkit.abstract_osid.osid import errors


class TestRelationshipLookupSession(unittest.TestCase):
    """Tests for RelationshipLookupSession"""

    @classmethod
    def setUpClass(cls):
        cls.relationship_list = list()
        cls.relationship_ids = list()
        cls.svc_mgr = Runtime().get_service_manager('RELATIONSHIP', proxy=PROXY, implementation='TEST_SERVICE')
        create_form = cls.svc_mgr.get_family_form_for_create([])
        create_form.display_name = 'Test Family'
        create_form.description = 'Test Family for RelationshipLookupSession tests'
        cls.catalog = cls.svc_mgr.create_family(create_form)
        for num in [0, 1]:
            create_form = cls.catalog.get_relationship_form_for_create([])
            create_form.display_name = 'Test Relationship ' + str(num)
            create_form.description = 'Test Relationship for RelationshipLookupSession tests'
            obj = cls.catalog.create_relationship(create_form)
            cls.relationship_list.append(obj)
            cls.relationship_ids.append(obj.ident)

    @classmethod
    def tearDownClass(cls):
        #for obj in cls.catalog.get_relationships():
        #    cls.catalog.delete_relationship(obj.ident)
        #for catalog in cls.catalogs:
        #    cls.svc_mgr.delete_family(catalog.ident)
        for catalog in cls.svc_mgr.get_families():
            for obj in catalog.get_relationships():
                catalog.delete_relationship(obj.ident)
            cls.svc_mgr.delete_family(catalog.ident)

    def test_get_family_id(self):
        """Tests get_family_id"""
        self.assertEqual(self.catalog.get_family_id(), self.catalog.ident)

    @unittest.skip('unimplemented test')
    def test_get_family(self):
        """Tests get_family"""
        pass

    def test_can_lookup_relationships(self):
        """Tests can_lookup_relationships"""
        self.assertTrue(isinstance(self.catalog.can_lookup_relationships(), bool))

    def test_use_comparative_relationship_view(self):
        """Tests use_comparative_relationship_view"""
        self.catalog.use_comparative_relationship_view()

    def test_use_plenary_relationship_view(self):
        """Tests use_plenary_relationship_view"""
        self.catalog.use_plenary_relationship_view()

    def test_use_federated_family_view(self):
        """Tests use_federated_family_view"""
        self.catalog.use_federated_family_view()

    def test_use_isolated_family_view(self):
        """Tests use_isolated_family_view"""
        self.catalog.use_isolated_family_view()

    @unittest.skip('unimplemented test')
    def test_use_effective_relationship_view(self):
        """Tests use_effective_relationship_view"""
        pass

    @unittest.skip('unimplemented test')
    def test_use_any_effective_relationship_view(self):
        """Tests use_any_effective_relationship_view"""
        pass

    def test_get_relationship(self):
        """Tests get_relationship"""
        obj = self.catalog.get_relationship(self.relationship_list[0].ident)
        self.assertEqual(obj.ident, self.relationship_list[0].ident)

    def test_get_relationships_by_ids(self):
        """Tests get_relationships_by_ids"""
        from dlkit.abstract_osid.relationship.objects import RelationshipList
        objects = self.catalog.get_relationships_by_ids(self.relationship_ids)
        self.assertTrue(isinstance(objects, RelationshipList))
        self.catalog.use_federated_family_view()
        objects = self.catalog.get_relationships_by_ids(self.relationship_ids)

    def test_get_relationships_by_genus_type(self):
        """Tests get_relationships_by_genus_type"""
        from dlkit.abstract_osid.relationship.objects import RelationshipList
        objects = self.catalog.get_relationships_by_genus_type(DEFAULT_TYPE)
        self.assertTrue(isinstance(objects, RelationshipList))
        self.catalog.use_federated_family_view()
        objects = self.catalog.get_relationships_by_genus_type(DEFAULT_TYPE)

    def test_get_relationships_by_parent_genus_type(self):
        """Tests get_relationships_by_parent_genus_type"""
        from dlkit.abstract_osid.relationship.objects import RelationshipList
        objects = self.catalog.get_relationships_by_parent_genus_type(DEFAULT_TYPE)
        self.assertTrue(isinstance(objects, RelationshipList))
        self.catalog.use_federated_family_view()
        objects = self.catalog.get_relationships_by_parent_genus_type(DEFAULT_TYPE)

    def test_get_relationships_by_record_type(self):
        """Tests get_relationships_by_record_type"""
        from dlkit.abstract_osid.relationship.objects import RelationshipList
        objects = self.catalog.get_relationships_by_record_type(DEFAULT_TYPE)
        self.assertTrue(isinstance(objects, RelationshipList))
        self.catalog.use_federated_family_view()
        objects = self.catalog.get_relationships_by_record_type(DEFAULT_TYPE)

    @unittest.skip('unimplemented test')
    def test_get_relationships_on_date(self):
        """Tests get_relationships_on_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationships_for_source(self):
        """Tests get_relationships_for_source"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationships_for_source_on_date(self):
        """Tests get_relationships_for_source_on_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationships_by_genus_type_for_source(self):
        """Tests get_relationships_by_genus_type_for_source"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationships_by_genus_type_for_source_on_date(self):
        """Tests get_relationships_by_genus_type_for_source_on_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationships_for_destination(self):
        """Tests get_relationships_for_destination"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationships_for_destination_on_date(self):
        """Tests get_relationships_for_destination_on_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationships_by_genus_type_for_destination(self):
        """Tests get_relationships_by_genus_type_for_destination"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationships_by_genus_type_for_destination_on_date(self):
        """Tests get_relationships_by_genus_type_for_destination_on_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationships_for_peers(self):
        """Tests get_relationships_for_peers"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationships_for_peers_on_date(self):
        """Tests get_relationships_for_peers_on_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationships_by_genus_type_for_peers(self):
        """Tests get_relationships_by_genus_type_for_peers"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationships_by_genus_type_for_peers_on_date(self):
        """Tests get_relationships_by_genus_type_for_peers_on_date"""
        pass

    def test_get_relationships(self):
        """Tests get_relationships"""
        from dlkit.abstract_osid.relationship.objects import RelationshipList
        objects = self.catalog.get_relationships()
        self.assertTrue(isinstance(objects, RelationshipList))
        self.catalog.use_federated_family_view()
        objects = self.catalog.get_relationships()


class TestRelationshipAdminSession(unittest.TestCase):
    """Tests for RelationshipAdminSession"""

    @classmethod
    def setUpClass(cls):
        cls.svc_mgr = Runtime().get_service_manager('RELATIONSHIP', proxy=PROXY, implementation='TEST_SERVICE')
        create_form = cls.svc_mgr.get_family_form_for_create([])
        create_form.display_name = 'Test Family'
        create_form.description = 'Test Family for RelationshipAdminSession tests'
        cls.catalog = cls.svc_mgr.create_family(create_form)

    @classmethod
    def tearDownClass(cls):
        for obj in cls.catalog.get_relationships():
            cls.catalog.delete_relationship(obj.ident)
        cls.svc_mgr.delete_family(cls.catalog.ident)

    def test_get_family_id(self):
        """Tests get_family_id"""
        self.assertEqual(self.catalog.get_family_id(), self.catalog.ident)

    @unittest.skip('unimplemented test')
    def test_get_family(self):
        """Tests get_family"""
        pass

    def test_can_create_relationships(self):
        """Tests can_create_relationships"""
        self.assertTrue(isinstance(self.catalog.can_create_relationships(), bool))

    def test_can_create_relationship_with_record_types(self):
        """Tests can_create_relationship_with_record_types"""
        self.assertTrue(isinstance(self.catalog.can_create_relationship_with_record_types(DEFAULT_TYPE), bool))

    @unittest.skip('unimplemented test')
    def test_get_relationship_form_for_create(self):
        """Tests get_relationship_form_for_create"""
        pass

    @unittest.skip('unimplemented test')
    def test_create_relationship(self):
        """Tests create_relationship"""
        pass

    def test_can_update_relationships(self):
        """Tests can_update_relationships"""
        self.assertTrue(isinstance(self.catalog.can_update_relationships(), bool))

    @unittest.skip('unimplemented test')
    def test_get_relationship_form_for_update(self):
        """Tests get_relationship_form_for_update"""
        pass

    @unittest.skip('unimplemented test')
    def test_update_relationship(self):
        """Tests update_relationship"""
        pass

    def test_can_delete_relationships(self):
        """Tests can_delete_relationships"""
        self.assertTrue(isinstance(self.catalog.can_delete_relationships(), bool))

    @unittest.skip('unimplemented test')
    def test_delete_relationship(self):
        """Tests delete_relationship"""
        pass

    @unittest.skip('unimplemented test')
    def test_can_manage_relationship_aliases(self):
        """Tests can_manage_relationship_aliases"""
        pass

    @unittest.skip('unimplemented test')
    def test_alias_relationship(self):
        """Tests alias_relationship"""
        pass


class TestFamilyLookupSession(unittest.TestCase):
    """Tests for FamilyLookupSession"""

    @classmethod
    def setUpClass(cls):
        cls.catalogs = list()
        cls.catalog_ids = list()
        cls.svc_mgr = Runtime().get_service_manager('RELATIONSHIP', proxy=PROXY, implementation='TEST_SERVICE')
        for num in [0, 1]:
            create_form = cls.svc_mgr.get_family_form_for_create([])
            create_form.display_name = 'Test Family ' + str(num)
            create_form.description = 'Test Family for relationship proxy manager tests'
            catalog = cls.svc_mgr.create_family(create_form)
            cls.catalogs.append(catalog)
            cls.catalog_ids.append(catalog.ident)

    @classmethod
    def tearDownClass(cls):
        #for catalog in cls.catalogs:
        #    cls.svc_mgr.delete_family(catalog.ident)
        for catalog in cls.svc_mgr.get_families():
            cls.svc_mgr.delete_family(catalog.ident)

    def test_can_lookup_families(self):
        """Tests can_lookup_families"""
        self.assertTrue(isinstance(self.svc_mgr.can_lookup_families(), bool))

    def test_use_comparative_family_view(self):
        """Tests use_comparative_family_view"""
        self.svc_mgr.use_comparative_family_view()

    def test_use_plenary_family_view(self):
        """Tests use_plenary_family_view"""
        self.svc_mgr.use_plenary_family_view()

    def test_get_family(self):
        """Tests get_family"""
        catalog = self.svc_mgr.get_family(self.catalogs[0].ident)
        self.assertEqual(catalog.ident, self.catalogs[0].ident)

    def test_get_families_by_ids(self):
        """Tests get_families_by_ids"""
        catalogs = self.svc_mgr.get_families_by_ids(self.catalog_ids)

    @unittest.skip('unimplemented test')
    def test_get_families_by_genus_type(self):
        """Tests get_families_by_genus_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_families_by_parent_genus_type(self):
        """Tests get_families_by_parent_genus_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_families_by_record_type(self):
        """Tests get_families_by_record_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_families_by_provider(self):
        """Tests get_families_by_provider"""
        pass

    def test_get_families(self):
        """Tests get_families"""
        catalogs = self.svc_mgr.get_families()


class TestFamilyAdminSession(unittest.TestCase):
    """Tests for FamilyAdminSession"""

    @classmethod
    def setUpClass(cls):
        cls.svc_mgr = Runtime().get_service_manager('RELATIONSHIP', proxy=PROXY, implementation='TEST_SERVICE')
        # Initialize test catalog:
        create_form = cls.svc_mgr.get_family_form_for_create([])
        create_form.display_name = 'Test Family'
        create_form.description = 'Test Family for FamilyAdminSession tests'
        cls.catalog = cls.svc_mgr.create_family(create_form)
        # Initialize catalog to be deleted:
        create_form = cls.svc_mgr.get_family_form_for_create([])
        create_form.display_name = 'Test Family For Deletion'
        create_form.description = 'Test Family for FamilyAdminSession deletion test'
        cls.catalog_to_delete = cls.svc_mgr.create_family(create_form)

    @classmethod
    def tearDownClass(cls):
        #for catalog in cls.catalogs:
        #    cls.svc_mgr.delete_family(catalog.ident)
        for catalog in cls.svc_mgr.get_families():
            cls.svc_mgr.delete_family(catalog.ident)

    def test_can_create_families(self):
        """Tests can_create_families"""
        self.assertTrue(isinstance(self.svc_mgr.can_create_families(), bool))

    def test_can_create_family_with_record_types(self):
        """Tests can_create_family_with_record_types"""
        self.assertTrue(isinstance(self.svc_mgr.can_create_family_with_record_types(DEFAULT_TYPE), bool))

    def test_get_family_form_for_create(self):
        """Tests get_family_form_for_create"""
        from dlkit.abstract_osid.relationship.objects import FamilyForm
        catalog_form = self.svc_mgr.get_family_form_for_create([])
        self.assertTrue(isinstance(catalog_form, FamilyForm))
        self.assertFalse(catalog_form.is_for_update())

    def test_create_family(self):
        """Tests create_family"""
        from dlkit.abstract_osid.relationship.objects import Family
        catalog_form = self.svc_mgr.get_family_form_for_create([])
        catalog_form.display_name = 'Test Family'
        catalog_form.description = 'Test Family for FamilyAdminSession.create_family tests'
        new_catalog = self.svc_mgr.create_family(catalog_form)
        self.assertTrue(isinstance(new_catalog, Family))

    def test_can_update_families(self):
        """Tests can_update_families"""
        self.assertTrue(isinstance(self.svc_mgr.can_update_families(), bool))

    def test_get_family_form_for_update(self):
        """Tests get_family_form_for_update"""
        from dlkit.abstract_osid.relationship.objects import FamilyForm
        catalog_form = self.svc_mgr.get_family_form_for_update(self.catalog.ident)
        self.assertTrue(isinstance(catalog_form, FamilyForm))
        self.assertTrue(catalog_form.is_for_update())

    def test_update_family(self):
        """Tests update_family"""
        catalog_form = self.svc_mgr.get_family_form_for_update(self.catalog.ident)
        # Update some elements here?
        self.svc_mgr.update_family(catalog_form)

    def test_can_delete_families(self):
        """Tests can_delete_families"""
        self.assertTrue(isinstance(self.svc_mgr.can_delete_families(), bool))

    def test_delete_family(self):
        """Tests delete_family"""
        cat_id = self.catalog_to_delete.ident
        self.svc_mgr.delete_family(cat_id)
        with self.assertRaises(errors.NotFound):
            self.svc_mgr.get_family(cat_id)

    @unittest.skip('unimplemented test')
    def test_can_manage_family_aliases(self):
        """Tests can_manage_family_aliases"""
        pass

    @unittest.skip('unimplemented test')
    def test_alias_family(self):
        """Tests alias_family"""
        pass


class TestFamilyHierarchySession(unittest.TestCase):
    """Tests for FamilyHierarchySession"""

    @classmethod
    def setUpClass(cls):
        cls.svc_mgr = Runtime().get_service_manager('RELATIONSHIP', proxy=PROXY, implementation='TEST_SERVICE')
        cls.catalogs = dict()
        for name in ['Root', 'Child 1', 'Child 2', 'Grandchild 1']:
            create_form = cls.svc_mgr.get_family_form_for_create([])
            create_form.display_name = name
            create_form.description = 'Test Family ' + name
            cls.catalogs[name] = cls.svc_mgr.create_family(create_form)
        cls.svc_mgr.add_root_family(cls.catalogs['Root'].ident)
        cls.svc_mgr.add_child_family(cls.catalogs['Root'].ident, cls.catalogs['Child 1'].ident)
        cls.svc_mgr.add_child_family(cls.catalogs['Root'].ident, cls.catalogs['Child 2'].ident)
        cls.svc_mgr.add_child_family(cls.catalogs['Child 1'].ident, cls.catalogs['Grandchild 1'].ident)
    @classmethod
    def tearDownClass(cls):
        cls.svc_mgr.remove_child_family(cls.catalogs['Child 1'].ident, cls.catalogs['Grandchild 1'].ident)
        cls.svc_mgr.remove_child_families(cls.catalogs['Root'].ident)
        for cat_name in cls.catalogs:
            cls.svc_mgr.delete_family(cls.catalogs[cat_name].ident)

    def test_get_family_hierarchy_id(self):
        """Tests get_family_hierarchy_id"""
        hierarchy_id = self.svc_mgr.get_family_hierarchy_id()

    def test_get_family_hierarchy(self):
        """Tests get_family_hierarchy"""
        hierarchy = self.svc_mgr.get_family_hierarchy()

    @unittest.skip('unimplemented test')
    def test_can_access_family_hierarchy(self):
        """Tests can_access_family_hierarchy"""
        pass

    def test_use_comparative_family_view(self):
        """Tests use_comparative_family_view"""
        self.svc_mgr.use_comparative_family_view()

    def test_use_plenary_family_view(self):
        """Tests use_plenary_family_view"""
        self.svc_mgr.use_plenary_family_view()

    def test_get_root_family_ids(self):
        """Tests get_root_family_ids"""
        root_ids = self.svc_mgr.get_root_family_ids()

    def test_get_root_families(self):
        """Tests get_root_families"""
        roots = self.svc_mgr.get_root_families()

    def test_has_parent_families(self):
        """Tests has_parent_families"""
        self.assertTrue(isinstance(self.svc_mgr.has_parent_families(self.catalogs['Child 1'].ident), bool))
        self.assertTrue(self.svc_mgr.has_parent_families(self.catalogs['Child 1'].ident))
        self.assertTrue(self.svc_mgr.has_parent_families(self.catalogs['Child 2'].ident))
        self.assertTrue(self.svc_mgr.has_parent_families(self.catalogs['Grandchild 1'].ident))
        self.assertFalse(self.svc_mgr.has_parent_families(self.catalogs['Root'].ident))

    def test_is_parent_of_family(self):
        """Tests is_parent_of_family"""
        self.assertTrue(isinstance(self.svc_mgr.is_parent_of_family(self.catalogs['Child 1'].ident, self.catalogs['Root'].ident), bool))
        self.assertTrue(self.svc_mgr.is_parent_of_family(self.catalogs['Root'].ident, self.catalogs['Child 1'].ident))
        self.assertTrue(self.svc_mgr.is_parent_of_family(self.catalogs['Child 1'].ident, self.catalogs['Grandchild 1'].ident))
        self.assertFalse(self.svc_mgr.is_parent_of_family(self.catalogs['Child 1'].ident, self.catalogs['Root'].ident))

    def test_get_parent_family_ids(self):
        """Tests get_parent_family_ids"""
        from dlkit.abstract_osid.id.objects import IdList
        catalog_list = self.svc_mgr.get_parent_family_ids(self.catalogs['Child 1'].ident)
        self.assertTrue(isinstance(catalog_list, IdList))
        self.assertEqual(catalog_list.available(), 1)

    def test_get_parent_families(self):
        """Tests get_parent_families"""
        from dlkit.abstract_osid.relationship.objects import FamilyList
        catalog_list = self.svc_mgr.get_parent_families(self.catalogs['Child 1'].ident)
        self.assertTrue(isinstance(catalog_list, FamilyList))
        self.assertEqual(catalog_list.available(), 1)
        self.assertEqual(catalog_list.next().display_name.text, 'Root')

    @unittest.skip('unimplemented test')
    def test_is_ancestor_of_family(self):
        """Tests is_ancestor_of_family"""
        pass

    def test_has_child_families(self):
        """Tests has_child_families"""
        self.assertTrue(isinstance(self.svc_mgr.has_child_families(self.catalogs['Child 1'].ident), bool))
        self.assertTrue(self.svc_mgr.has_child_families(self.catalogs['Root'].ident))
        self.assertTrue(self.svc_mgr.has_child_families(self.catalogs['Child 1'].ident))
        self.assertFalse(self.svc_mgr.has_child_families(self.catalogs['Child 2'].ident))
        self.assertFalse(self.svc_mgr.has_child_families(self.catalogs['Grandchild 1'].ident))

    def test_is_child_of_family(self):
        """Tests is_child_of_family"""
        self.assertTrue(isinstance(self.svc_mgr.is_child_of_family(self.catalogs['Child 1'].ident, self.catalogs['Root'].ident), bool))
        self.assertTrue(self.svc_mgr.is_child_of_family(self.catalogs['Child 1'].ident, self.catalogs['Root'].ident))
        self.assertTrue(self.svc_mgr.is_child_of_family(self.catalogs['Grandchild 1'].ident, self.catalogs['Child 1'].ident))
        self.assertFalse(self.svc_mgr.is_child_of_family(self.catalogs['Root'].ident, self.catalogs['Child 1'].ident))

    def test_get_child_family_ids(self):
        """Tests get_child_family_ids"""
        from dlkit.abstract_osid.id.objects import IdList
        catalog_list = self.svc_mgr.get_child_family_ids(self.catalogs['Child 1'].ident)
        self.assertTrue(isinstance(catalog_list, IdList))
        self.assertEqual(catalog_list.available(), 1)

    def test_get_child_families(self):
        """Tests get_child_families"""
        from dlkit.abstract_osid.relationship.objects import FamilyList
        catalog_list = self.svc_mgr.get_child_families(self.catalogs['Child 1'].ident)
        self.assertTrue(isinstance(catalog_list, FamilyList))
        self.assertEqual(catalog_list.available(), 1)
        self.assertEqual(catalog_list.next().display_name.text, 'Grandchild 1')

    @unittest.skip('unimplemented test')
    def test_is_descendant_of_family(self):
        """Tests is_descendant_of_family"""
        pass

    def test_get_family_node_ids(self):
        """Tests get_family_node_ids"""
        node_ids = self.svc_mgr.get_family_node_ids(self.catalogs['Child 1'].ident, 1, 1, False)
        # add some tests on the returned node

    @unittest.skip('unimplemented test')
    def test_get_family_nodes(self):
        """Tests get_family_nodes"""
        pass


class TestFamilyHierarchyDesignSession(unittest.TestCase):
    """Tests for FamilyHierarchyDesignSession"""

    @classmethod
    def setUpClass(cls):
        cls.svc_mgr = Runtime().get_service_manager('RELATIONSHIP', proxy=PROXY, implementation='TEST_SERVICE')
        cls.catalogs = dict()
        for name in ['Root', 'Child 1', 'Child 2', 'Grandchild 1']:
            create_form = cls.svc_mgr.get_family_form_for_create([])
            create_form.display_name = name
            create_form.description = 'Test Family ' + name
            cls.catalogs[name] = cls.svc_mgr.create_family(create_form)
        cls.svc_mgr.add_root_family(cls.catalogs['Root'].ident)
        cls.svc_mgr.add_child_family(cls.catalogs['Root'].ident, cls.catalogs['Child 1'].ident)
        cls.svc_mgr.add_child_family(cls.catalogs['Root'].ident, cls.catalogs['Child 2'].ident)
        cls.svc_mgr.add_child_family(cls.catalogs['Child 1'].ident, cls.catalogs['Grandchild 1'].ident)
    @classmethod
    def tearDownClass(cls):
        cls.svc_mgr.remove_child_family(cls.catalogs['Child 1'].ident, cls.catalogs['Grandchild 1'].ident)
        cls.svc_mgr.remove_child_families(cls.catalogs['Root'].ident)
        for cat_name in cls.catalogs:
            cls.svc_mgr.delete_family(cls.catalogs[cat_name].ident)

    def test_get_family_hierarchy_id(self):
        """Tests get_family_hierarchy_id"""
        hierarchy_id = self.svc_mgr.get_family_hierarchy_id()

    def test_get_family_hierarchy(self):
        """Tests get_family_hierarchy"""
        hierarchy = self.svc_mgr.get_family_hierarchy()

    @unittest.skip('unimplemented test')
    def test_can_modify_family_hierarchy(self):
        """Tests can_modify_family_hierarchy"""
        pass

    @unittest.skip('unimplemented test')
    def test_add_root_family(self):
        """Tests add_root_family"""
        pass

    @unittest.skip('unimplemented test')
    def test_remove_root_family(self):
        """Tests remove_root_family"""
        pass

    @unittest.skip('unimplemented test')
    def test_add_child_family(self):
        """Tests add_child_family"""
        pass

    @unittest.skip('unimplemented test')
    def test_remove_child_family(self):
        """Tests remove_child_family"""
        pass

    @unittest.skip('unimplemented test')
    def test_remove_child_families(self):
        """Tests remove_child_families"""
        pass


