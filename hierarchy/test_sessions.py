"""Unit tests of hierarchy sessions."""


import unittest
from dlkit_runtime import PROXY_SESSION, proxy_example
from dlkit_runtime.managers import Runtime
REQUEST = proxy_example.TestRequest()
CONDITION = PROXY_SESSION.get_proxy_condition()
CONDITION.set_http_request(REQUEST)
PROXY = PROXY_SESSION.get_proxy(CONDITION)

from dlkit.primordium.type.primitives import Type
DEFAULT_TYPE = Type(**{'identifier': 'DEFAULT', 'namespace': 'DEFAULT', 'authority': 'DEFAULT',})

from dlkit.abstract_osid.osid import errors


class TestHierarchyTraversalSession(unittest.TestCase):
    """Tests for HierarchyTraversalSession"""


    def test_get_hierarchy_id(self):
        """Tests get_hierarchy_id"""
        self.assertEqual(self.catalog.get_hierarchy_id(), self.catalog.ident)

    @unittest.skip('unimplemented test')
    def test_get_hierarchy(self):
        """Tests get_hierarchy"""
        pass

    @unittest.skip('unimplemented test')
    def test_can_access_hierarchy(self):
        """Tests can_access_hierarchy"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_roots(self):
        """Tests get_roots"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_parents(self):
        """Tests has_parents"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_parent(self):
        """Tests is_parent"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_parents(self):
        """Tests get_parents"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_ancestor(self):
        """Tests is_ancestor"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_children(self):
        """Tests has_children"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_child(self):
        """Tests is_child"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_children(self):
        """Tests get_children"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_descendant(self):
        """Tests is_descendant"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_nodes(self):
        """Tests get_nodes"""
        pass


class TestHierarchyDesignSession(unittest.TestCase):
    """Tests for HierarchyDesignSession"""


    def test_get_hierarchy_id(self):
        """Tests get_hierarchy_id"""
        self.assertEqual(self.catalog.get_hierarchy_id(), self.catalog.ident)

    @unittest.skip('unimplemented test')
    def test_get_hierarchy(self):
        """Tests get_hierarchy"""
        pass

    @unittest.skip('unimplemented test')
    def test_can_modify_hierarchy(self):
        """Tests can_modify_hierarchy"""
        pass

    @unittest.skip('unimplemented test')
    def test_add_root(self):
        """Tests add_root"""
        pass

    @unittest.skip('unimplemented test')
    def test_add_child(self):
        """Tests add_child"""
        pass

    @unittest.skip('unimplemented test')
    def test_remove_root(self):
        """Tests remove_root"""
        pass

    @unittest.skip('unimplemented test')
    def test_remove_child(self):
        """Tests remove_child"""
        pass

    @unittest.skip('unimplemented test')
    def test_remove_children(self):
        """Tests remove_children"""
        pass


class TestHierarchyLookupSession(unittest.TestCase):
    """Tests for HierarchyLookupSession"""

    @classmethod
    def setUpClass(cls):
        cls.catalogs = list()
        cls.catalog_ids = list()
        cls.svc_mgr = Runtime().get_service_manager('HIERARCHY', proxy=PROXY, implementation='TEST_SERVICE')
        for num in [0, 1]:
            create_form = cls.svc_mgr.get_hierarchy_form_for_create([])
            create_form.display_name = 'Test Hierarchy ' + str(num)
            create_form.description = 'Test Hierarchy for hierarchy proxy manager tests'
            catalog = cls.svc_mgr.create_hierarchy(create_form)
            cls.catalogs.append(catalog)
            cls.catalog_ids.append(catalog.ident)

    @classmethod
    def tearDownClass(cls):
        #for catalog in cls.catalogs:
        #    cls.svc_mgr.delete_hierarchy(catalog.ident)
        for catalog in cls.svc_mgr.get_hierarchies():
            cls.svc_mgr.delete_hierarchy(catalog.ident)

    def test_can_lookup_hierarchies(self):
        """Tests can_lookup_hierarchies"""
        self.assertTrue(isinstance(self.svc_mgr.can_lookup_hierarchies(), bool))

    def test_use_comparative_hierarchy_view(self):
        """Tests use_comparative_hierarchy_view"""
        self.svc_mgr.use_comparative_hierarchy_view()

    def test_use_plenary_hierarchy_view(self):
        """Tests use_plenary_hierarchy_view"""
        self.svc_mgr.use_plenary_hierarchy_view()

    def test_get_hierarchy(self):
        """Tests get_hierarchy"""
        catalog = self.svc_mgr.get_hierarchy(self.catalogs[0].ident)
        self.assertEqual(catalog.ident, self.catalogs[0].ident)

    def test_get_hierarchies_by_ids(self):
        """Tests get_hierarchies_by_ids"""
        catalogs = self.svc_mgr.get_hierarchies_by_ids(self.catalog_ids)

    @unittest.skip('unimplemented test')
    def test_get_hierarchies_by_genus_type(self):
        """Tests get_hierarchies_by_genus_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_hierarchies_by_parent_genus_type(self):
        """Tests get_hierarchies_by_parent_genus_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_hierarchies_by_record_type(self):
        """Tests get_hierarchies_by_record_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_hierarchies_by_provider(self):
        """Tests get_hierarchies_by_provider"""
        pass

    def test_get_hierarchies(self):
        """Tests get_hierarchies"""
        catalogs = self.svc_mgr.get_hierarchies()


class TestHierarchyAdminSession(unittest.TestCase):
    """Tests for HierarchyAdminSession"""

    @classmethod
    def setUpClass(cls):
        cls.svc_mgr = Runtime().get_service_manager('HIERARCHY', proxy=PROXY, implementation='TEST_SERVICE')
        # Initialize test catalog:
        create_form = cls.svc_mgr.get_hierarchy_form_for_create([])
        create_form.display_name = 'Test Hierarchy'
        create_form.description = 'Test Hierarchy for HierarchyAdminSession tests'
        cls.catalog = cls.svc_mgr.create_hierarchy(create_form)
        # Initialize catalog to be deleted:
        create_form = cls.svc_mgr.get_hierarchy_form_for_create([])
        create_form.display_name = 'Test Hierarchy For Deletion'
        create_form.description = 'Test Hierarchy for HierarchyAdminSession deletion test'
        cls.catalog_to_delete = cls.svc_mgr.create_hierarchy(create_form)

    @classmethod
    def tearDownClass(cls):
        #for catalog in cls.catalogs:
        #    cls.svc_mgr.delete_hierarchy(catalog.ident)
        for catalog in cls.svc_mgr.get_hierarchies():
            cls.svc_mgr.delete_hierarchy(catalog.ident)

    def test_can_create_hierarchies(self):
        """Tests can_create_hierarchies"""
        self.assertTrue(isinstance(self.svc_mgr.can_create_hierarchies(), bool))

    def test_can_create_hierarchy_with_record_types(self):
        """Tests can_create_hierarchy_with_record_types"""
        self.assertTrue(isinstance(self.svc_mgr.can_create_hierarchy_with_record_types(DEFAULT_TYPE), bool))

    def test_get_hierarchy_form_for_create(self):
        """Tests get_hierarchy_form_for_create"""
        from dlkit.abstract_osid.hierarchy.objects import HierarchyForm
        catalog_form = self.svc_mgr.get_hierarchy_form_for_create([])
        self.assertTrue(isinstance(catalog_form, HierarchyForm))
        self.assertFalse(catalog_form.is_for_update())

    def test_create_hierarchy(self):
        """Tests create_hierarchy"""
        from dlkit.abstract_osid.hierarchy.objects import Hierarchy
        catalog_form = self.svc_mgr.get_hierarchy_form_for_create([])
        catalog_form.display_name = 'Test Hierarchy'
        catalog_form.description = 'Test Hierarchy for HierarchyAdminSession.create_hierarchy tests'
        new_catalog = self.svc_mgr.create_hierarchy(catalog_form)
        self.assertTrue(isinstance(new_catalog, Hierarchy))

    def test_can_update_hierarchies(self):
        """Tests can_update_hierarchies"""
        self.assertTrue(isinstance(self.svc_mgr.can_update_hierarchies(), bool))

    def test_get_hierarchy_form_for_update(self):
        """Tests get_hierarchy_form_for_update"""
        from dlkit.abstract_osid.hierarchy.objects import HierarchyForm
        catalog_form = self.svc_mgr.get_hierarchy_form_for_update(self.catalog.ident)
        self.assertTrue(isinstance(catalog_form, HierarchyForm))
        self.assertTrue(catalog_form.is_for_update())

    def test_update_hierarchy(self):
        """Tests update_hierarchy"""
        catalog_form = self.svc_mgr.get_hierarchy_form_for_update(self.catalog.ident)
        # Update some elements here?
        self.svc_mgr.update_hierarchy(catalog_form)

    def test_can_delete_hierarchies(self):
        """Tests can_delete_hierarchies"""
        self.assertTrue(isinstance(self.svc_mgr.can_delete_hierarchies(), bool))

    @unittest.skip('unimplemented test')
    def test_delete_hierarchy(self):
        """Tests delete_hierarchy"""
        pass

    @unittest.skip('unimplemented test')
    def test_can_manage_hierarchy_aliases(self):
        """Tests can_manage_hierarchy_aliases"""
        pass

    @unittest.skip('unimplemented test')
    def test_alias_hierarchy(self):
        """Tests alias_hierarchy"""
        pass


