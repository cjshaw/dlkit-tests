"""Unit tests of hierarchy queries."""


import unittest


class TestHierarchyQuery(unittest.TestCase):
    """Tests for HierarchyQuery"""

    @unittest.skip('unimplemented test')
    def test_match_node_id(self):
        """Tests match_node_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_node_id(self):
        """Tests match_any_node_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_node_id_terms(self):
        """Tests clear_node_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_hierarchy_query_record(self):
        """Tests get_hierarchy_query_record"""
        pass


