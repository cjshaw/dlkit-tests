"""Unit tests of hierarchy managers."""


import unittest
from dlkit_runtime import PROXY_SESSION, proxy_example
from dlkit_runtime.managers import Runtime
REQUEST = proxy_example.TestRequest()
CONDITION = PROXY_SESSION.get_proxy_condition()
CONDITION.set_http_request(REQUEST)
PROXY = PROXY_SESSION.get_proxy(CONDITION)

from dlkit.primordium.type.primitives import Type
DEFAULT_TYPE = Type(**{'identifier': 'DEFAULT', 'namespace': 'DEFAULT', 'authority': 'DEFAULT',})

from dlkit.abstract_osid.type.objects import TypeList as abc_type_list
from dlkit.abstract_osid.osid import errors


class TestHierarchyProfile(unittest.TestCase):
    """Tests for HierarchyProfile"""

    @classmethod
    def setUpClass(cls):
        cls.mgr = Runtime().get_service_manager('HIERARCHY', proxy=PROXY, implementation='TEST_SERVICE')

    def test_supports_hierarchy_traversal(self):
        """Tests supports_hierarchy_traversal"""
        self.assertTrue(isinstance(self.mgr.supports_hierarchy_traversal(), bool))

    def test_supports_hierarchy_design(self):
        """Tests supports_hierarchy_design"""
        self.assertTrue(isinstance(self.mgr.supports_hierarchy_design(), bool))

    def test_supports_hierarchy_lookup(self):
        """Tests supports_hierarchy_lookup"""
        self.assertTrue(isinstance(self.mgr.supports_hierarchy_lookup(), bool))

    def test_supports_hierarchy_admin(self):
        """Tests supports_hierarchy_admin"""
        self.assertTrue(isinstance(self.mgr.supports_hierarchy_admin(), bool))

    def test_get_hierarchy_record_types(self):
        """Tests get_hierarchy_record_types"""
        self.assertTrue(isinstance(self.mgr.get_hierarchy_record_types(), abc_type_list))

    def test_get_hierarchy_search_record_types(self):
        """Tests get_hierarchy_search_record_types"""
        self.assertTrue(isinstance(self.mgr.get_hierarchy_search_record_types(), abc_type_list))


class TestHierarchyManager(unittest.TestCase):
    """Tests for HierarchyManager"""

    @classmethod
    def setUpClass(cls):
        cls.svc_mgr = Runtime().get_service_manager('HIERARCHY', proxy=PROXY, implementation='TEST_SERVICE')
        create_form = cls.svc_mgr.get_hierarchy_form_for_create([])
        create_form.display_name = 'Test Hierarchy'
        create_form.description = 'Test Hierarchy for hierarchy manager tests'
        catalog = cls.svc_mgr.create_hierarchy(create_form)
        cls.catalog_id = catalog.get_id()
        cls.mgr = Runtime().get_manager('HIERARCHY', 'TEST_MONGO_1', (3, 0, 0))

    @classmethod
    def tearDownClass(cls):
        cls.svc_mgr.delete_hierarchy(cls.catalog_id)

    def test_get_hierarchy_traversal_session(self):
        """Tests get_hierarchy_traversal_session"""
        if self.mgr.supports_hierarchy_traversal():
            self.mgr.get_hierarchy_traversal_session()

    def test_get_hierarchy_traversal_session_for_hierarchy(self):
        """Tests get_hierarchy_traversal_session_for_hierarchy"""
        if self.mgr.supports_hierarchy_traversal():
            self.mgr.get_hierarchy_traversal_session_for_hierarchy(self.catalog_id)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_hierarchy_traversal_session_for_hierarchy()

    def test_get_hierarchy_design_session(self):
        """Tests get_hierarchy_design_session"""
        if self.mgr.supports_hierarchy_design():
            self.mgr.get_hierarchy_design_session()

    def test_get_hierarchy_design_session_for_hierarchy(self):
        """Tests get_hierarchy_design_session_for_hierarchy"""
        if self.mgr.supports_hierarchy_design():
            self.mgr.get_hierarchy_design_session_for_hierarchy(self.catalog_id)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_hierarchy_design_session_for_hierarchy()

    def test_get_hierarchy_lookup_session(self):
        """Tests get_hierarchy_lookup_session"""
        if self.mgr.supports_hierarchy_lookup():
            self.mgr.get_hierarchy_lookup_session()

    def test_get_hierarchy_admin_session(self):
        """Tests get_hierarchy_admin_session"""
        if self.mgr.supports_hierarchy_admin():
            self.mgr.get_hierarchy_admin_session()


class TestHierarchyProxyManager(unittest.TestCase):
    """Tests for HierarchyProxyManager"""

    @classmethod
    def setUpClass(cls):
        cls.svc_mgr = Runtime().get_service_manager('HIERARCHY', proxy=PROXY, implementation='TEST_SERVICE')
        create_form = cls.svc_mgr.get_hierarchy_form_for_create([])
        create_form.display_name = 'Test Hierarchy'
        create_form.description = 'Test Hierarchy for hierarchy proxy manager tests'
        catalog = cls.svc_mgr.create_hierarchy(create_form)
        cls.catalog_id = catalog.get_id()
        cls.mgr = Runtime().get_proxy_manager('HIERARCHY', 'TEST_MONGO_1', (3, 0, 0))

    @classmethod
    def tearDownClass(cls):
        cls.svc_mgr.delete_hierarchy(cls.catalog_id)

    def test_get_hierarchy_traversal_session(self):
        """Tests get_hierarchy_traversal_session"""
        if self.mgr.supports_hierarchy_traversal():
            self.mgr.get_hierarchy_traversal_session(PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_hierarchy_traversal_session()

    def test_get_hierarchy_traversal_session_for_hierarchy(self):
        """Tests get_hierarchy_traversal_session_for_hierarchy"""
        if self.mgr.supports_hierarchy_traversal():
            self.mgr.get_hierarchy_traversal_session_for_hierarchy(self.catalog_id, PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_hierarchy_traversal_session_for_hierarchy()

    def test_get_hierarchy_design_session(self):
        """Tests get_hierarchy_design_session"""
        if self.mgr.supports_hierarchy_design():
            self.mgr.get_hierarchy_design_session(PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_hierarchy_design_session()

    def test_get_hierarchy_design_session_for_hierarchy(self):
        """Tests get_hierarchy_design_session_for_hierarchy"""
        if self.mgr.supports_hierarchy_design():
            self.mgr.get_hierarchy_design_session_for_hierarchy(self.catalog_id, PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_hierarchy_design_session_for_hierarchy()

    def test_get_hierarchy_lookup_session(self):
        """Tests get_hierarchy_lookup_session"""
        if self.mgr.supports_hierarchy_lookup():
            self.mgr.get_hierarchy_lookup_session(PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_hierarchy_lookup_session()

    def test_get_hierarchy_admin_session(self):
        """Tests get_hierarchy_admin_session"""
        if self.mgr.supports_hierarchy_admin():
            self.mgr.get_hierarchy_admin_session(PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_hierarchy_admin_session()


