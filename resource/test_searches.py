"""Unit tests of resource searches."""

import unittest


class TestResourceSearch(unittest.TestCase):
    """Tests for ResourceSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_resources(self):
        """Tests search_among_resources"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_resource_results(self):
        """Tests order_resource_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_resource_search_record(self):
        """Tests get_resource_search_record"""
        pass


class TestResourceSearchResults(unittest.TestCase):
    """Tests for ResourceSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_resources(self):
        """Tests get_resources"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_resource_query_inspector(self):
        """Tests get_resource_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_resource_search_results_record(self):
        """Tests get_resource_search_results_record"""
        pass


class TestBinSearch(unittest.TestCase):
    """Tests for BinSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_bins(self):
        """Tests search_among_bins"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_bin_results(self):
        """Tests order_bin_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_bin_search_record(self):
        """Tests get_bin_search_record"""
        pass


class TestBinSearchResults(unittest.TestCase):
    """Tests for BinSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_bins(self):
        """Tests get_bins"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_bin_query_inspector(self):
        """Tests get_bin_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_bin_search_results_record(self):
        """Tests get_bin_search_results_record"""
        pass


