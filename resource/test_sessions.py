"""Unit tests of resource sessions."""

import unittest
from dlkit_runtime import PROXY_SESSION, proxy_example
from dlkit_runtime.managers import Runtime
REQUEST = proxy_example.TestRequest()
CONDITION = PROXY_SESSION.get_proxy_condition()
CONDITION.set_http_request(REQUEST)
PROXY = PROXY_SESSION.get_proxy(CONDITION)

from dlkit.primordium.type.primitives import Type
DEFAULT_TYPE = Type(**{'identifier': 'DEFAULT', 'namespace': 'DEFAULT', 'authority': 'DEFAULT',})

from dlkit.primordium.id.primitives import Id
ALIAS_ID = Id(**{'identifier': 'ALIAS', 'namespace': 'ALIAS', 'authority': 'ALIAS',})

AGENT_ID_0 = Id(**{'identifier': 'jane_doe', 'namespace': 'osid.agent.Agent', 'authority': 'MIT-ODL',})

AGENT_ID_1 = Id(**{'identifier': 'john_doe', 'namespace': 'osid.agent.Agent', 'authority': 'MIT-ODL',})

from dlkit.abstract_osid.osid import errors


class TestResourceLookupSession(unittest.TestCase):
    """Tests for ResourceLookupSession"""

    @classmethod
    def setUpClass(cls):
        # Implemented from init template for ResourceLookupSession
        cls.resource_list = list()
        cls.resource_ids = list()
        cls.svc_mgr = Runtime().get_service_manager('RESOURCE', proxy=PROXY, implementation='TEST_SERVICE')
        create_form = cls.svc_mgr.get_bin_form_for_create([])
        create_form.display_name = 'Test Bin'
        create_form.description = 'Test Bin for ResourceLookupSession tests'
        cls.catalog = cls.svc_mgr.create_bin(create_form)
        for num in [0, 1]:
            create_form = cls.catalog.get_resource_form_for_create([])
            create_form.display_name = 'Test Resource ' + str(num)
            create_form.description = 'Test Resource for ResourceLookupSession tests'
            obj = cls.catalog.create_resource(create_form)
            cls.resource_list.append(obj)
            cls.resource_ids.append(obj.ident)

    @classmethod
    def tearDownClass(cls):
        # Implemented from init template for ResourceLookupSession
        #for obj in cls.catalog.get_resources():
        #    cls.catalog.delete_resource(obj.ident)
        #for catalog in cls.catalogs:
        #    cls.svc_mgr.delete_bin(catalog.ident)
        for catalog in cls.svc_mgr.get_bins():
            for obj in catalog.get_resources():
                catalog.delete_resource(obj.ident)
            cls.svc_mgr.delete_bin(catalog.ident)


    def test_get_bin_id(self):
        """Tests get_bin_id"""
        self.assertEqual(self.catalog.get_bin_id(), self.catalog.ident)

    @unittest.skip('unimplemented test')
    def test_get_bin(self):
        """Tests get_bin"""
        pass

    def test_can_lookup_resources(self):
        """Tests can_lookup_resources"""
        self.assertTrue(isinstance(self.catalog.can_lookup_resources(), bool))

    def test_use_comparative_resource_view(self):
        """Tests use_comparative_resource_view"""
        self.catalog.use_comparative_resource_view()

    def test_use_plenary_resource_view(self):
        """Tests use_plenary_resource_view"""
        self.catalog.use_plenary_resource_view()

    def test_use_federated_bin_view(self):
        """Tests use_federated_bin_view"""
        self.catalog.use_federated_bin_view()

    def test_use_isolated_bin_view(self):
        """Tests use_isolated_bin_view"""
        self.catalog.use_isolated_bin_view()

    def test_get_resource(self):
        """Tests get_resource"""
        self.catalog.use_isolated_bin_view()
        obj = self.catalog.get_resource(self.resource_list[0].ident)
        self.assertEqual(obj.ident, self.resource_list[0].ident)
        self.catalog.use_federated_bin_view()
        obj = self.catalog.get_resource(self.resource_list[0].ident)
        self.assertEqual(obj.ident, self.resource_list[0].ident)

    def test_get_resources_by_ids(self):
        """Tests get_resources_by_ids"""
        from dlkit.abstract_osid.resource.objects import ResourceList
        objects = self.catalog.get_resources_by_ids(self.resource_ids)
        self.assertTrue(isinstance(objects, ResourceList))
        self.catalog.use_federated_bin_view()
        objects = self.catalog.get_resources_by_ids(self.resource_ids)

    def test_get_resources_by_genus_type(self):
        """Tests get_resources_by_genus_type"""
        from dlkit.abstract_osid.resource.objects import ResourceList
        objects = self.catalog.get_resources_by_genus_type(DEFAULT_TYPE)
        self.assertTrue(isinstance(objects, ResourceList))
        self.catalog.use_federated_bin_view()
        objects = self.catalog.get_resources_by_genus_type(DEFAULT_TYPE)

    def test_get_resources_by_parent_genus_type(self):
        """Tests get_resources_by_parent_genus_type"""
        from dlkit.abstract_osid.resource.objects import ResourceList
        objects = self.catalog.get_resources_by_parent_genus_type(DEFAULT_TYPE)
        self.assertTrue(isinstance(objects, ResourceList))
        self.catalog.use_federated_bin_view()
        objects = self.catalog.get_resources_by_parent_genus_type(DEFAULT_TYPE)

    def test_get_resources_by_record_type(self):
        """Tests get_resources_by_record_type"""
        from dlkit.abstract_osid.resource.objects import ResourceList
        objects = self.catalog.get_resources_by_record_type(DEFAULT_TYPE)
        self.assertTrue(isinstance(objects, ResourceList))
        self.catalog.use_federated_bin_view()
        objects = self.catalog.get_resources_by_record_type(DEFAULT_TYPE)

    def test_get_resources(self):
        """Tests get_resources"""
        from dlkit.abstract_osid.resource.objects import ResourceList
        objects = self.catalog.get_resources()
        self.assertTrue(isinstance(objects, ResourceList))
        self.catalog.use_federated_bin_view()
        objects = self.catalog.get_resources()

    def test_get_resource_with_alias(self):
        self.catalog.alias_resource(self.resource_ids[0], ALIAS_ID)
        obj = self.catalog.get_resource(ALIAS_ID)
        self.assertEqual(obj.get_id(), self.resource_ids[0])


class TestResourceQuerySession(unittest.TestCase):
    """Tests for ResourceQuerySession"""

    @classmethod
    def setUpClass(cls):
        cls.resource_list = list()
        cls.resource_ids = list()
        cls.svc_mgr = Runtime().get_service_manager('RESOURCE', proxy=PROXY, implementation='TEST_SERVICE')
        create_form = cls.svc_mgr.get_bin_form_for_create([])
        create_form.display_name = 'Test Bin'
        create_form.description = 'Test Bin for ResourceQuerySession tests'
        cls.catalog = cls.svc_mgr.create_bin(create_form)
        for color in ['Orange', 'Blue', 'Green', 'orange']:
            create_form = cls.catalog.get_resource_form_for_create([])
            create_form.display_name = 'Test Resource ' + color
            create_form.description = (
                'Test Resource for ResourceQuerySession tests, did I mention green')
            obj = cls.catalog.create_resource(create_form)
            cls.resource_list.append(obj)
            cls.resource_ids.append(obj.ident)

    @classmethod
    def tearDownClass(cls):
        #for obj in cls.catalog.get_resources():
        #    cls.catalog.delete_resource(obj.ident)
        #for catalog in cls.catalogs:
        #    cls.svc_mgr.delete_bin(catalog.ident)
        for catalog in cls.svc_mgr.get_bins():
            for obj in catalog.get_resources():
                catalog.delete_resource(obj.ident)
            cls.svc_mgr.delete_bin(catalog.ident)


    def test_get_bin_id(self):
        """Tests get_bin_id"""
        self.assertEqual(self.catalog.get_bin_id(), self.catalog.ident)

    @unittest.skip('unimplemented test')
    def test_get_bin(self):
        """Tests get_bin"""
        pass

    @unittest.skip('unimplemented test')
    def test_can_search_resources(self):
        """Tests can_search_resources"""
        pass

    def test_use_federated_bin_view(self):
        """Tests use_federated_bin_view"""
        self.catalog.use_federated_bin_view()

    def test_use_isolated_bin_view(self):
        """Tests use_isolated_bin_view"""
        self.catalog.use_isolated_bin_view()

    def test_get_resource_query(self):
        """Tests get_resource_query"""
        query = self.catalog.get_resource_query()

    def test_get_resources_by_query(self):
        """Tests get_resources_by_query"""
        # Need to add some tests with string types
        query = self.catalog.get_resource_query()
        query.match_display_name('orange')
        self.assertEqual(self.catalog.get_resources_by_query(query).available(), 2)
        query.clear_display_name_terms()
        query.match_display_name('blue', match=False)
        self.assertEqual(self.catalog.get_resources_by_query(query).available(), 3)


class TestResourceSearchSession(unittest.TestCase):
    """Tests for ResourceSearchSession"""

    @unittest.skip('unimplemented test')
    def test_get_resource_search(self):
        """Tests get_resource_search"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_resource_search_order(self):
        """Tests get_resource_search_order"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_resources_by_search(self):
        """Tests get_resources_by_search"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_resource_query_from_inspector(self):
        """Tests get_resource_query_from_inspector"""
        pass


class TestResourceAdminSession(unittest.TestCase):
    """Tests for ResourceAdminSession"""

    @classmethod
    def setUpClass(cls):
        cls.svc_mgr = Runtime().get_service_manager('RESOURCE', proxy=PROXY, implementation='TEST_SERVICE')
        create_form = cls.svc_mgr.get_bin_form_for_create([])
        create_form.display_name = 'Test Bin'
        create_form.description = 'Test Bin for ResourceAdminSession tests'
        cls.catalog = cls.svc_mgr.create_bin(create_form)

    @classmethod
    def tearDownClass(cls):
        for obj in cls.catalog.get_resources():
            cls.catalog.delete_resource(obj.ident)
        cls.svc_mgr.delete_bin(cls.catalog.ident)


    def test_get_bin_id(self):
        """Tests get_bin_id"""
        self.assertEqual(self.catalog.get_bin_id(), self.catalog.ident)

    @unittest.skip('unimplemented test')
    def test_get_bin(self):
        """Tests get_bin"""
        pass

    def test_can_create_resources(self):
        """Tests can_create_resources"""
        self.assertTrue(isinstance(self.catalog.can_create_resources(), bool))

    def test_can_create_resource_with_record_types(self):
        """Tests can_create_resource_with_record_types"""
        self.assertTrue(isinstance(self.catalog.can_create_resource_with_record_types(DEFAULT_TYPE), bool))

    @unittest.skip('unimplemented test')
    def test_get_resource_form_for_create(self):
        """Tests get_resource_form_for_create"""
        pass

    @unittest.skip('unimplemented test')
    def test_create_resource(self):
        """Tests create_resource"""
        pass

    def test_can_update_resources(self):
        """Tests can_update_resources"""
        self.assertTrue(isinstance(self.catalog.can_update_resources(), bool))

    @unittest.skip('unimplemented test')
    def test_get_resource_form_for_update(self):
        """Tests get_resource_form_for_update"""
        pass

    @unittest.skip('unimplemented test')
    def test_update_resource(self):
        """Tests update_resource"""
        pass

    def test_can_delete_resources(self):
        """Tests can_delete_resources"""
        self.assertTrue(isinstance(self.catalog.can_delete_resources(), bool))

    @unittest.skip('unimplemented test')
    def test_delete_resource(self):
        """Tests delete_resource"""
        pass

    @unittest.skip('unimplemented test')
    def test_can_manage_resource_aliases(self):
        """Tests can_manage_resource_aliases"""
        pass

    @unittest.skip('unimplemented test')
    def test_alias_resource(self):
        """Tests alias_resource"""
        pass


class TestResourceNotificationSession(unittest.TestCase):
    """Tests for ResourceNotificationSession"""

    @classmethod
    def setUpClass(cls):
        # Implemented from init template for ResourceLookupSession
        cls.resource_list = list()
        cls.resource_ids = list()
        cls.svc_mgr = Runtime().get_service_manager('RESOURCE', proxy=PROXY, implementation='TEST_SERVICE')
        create_form = cls.svc_mgr.get_bin_form_for_create([])
        create_form.display_name = 'Test Bin'
        create_form.description = 'Test Bin for ResourceNotificationSession tests'
        cls.catalog = cls.svc_mgr.create_bin(create_form)
        for num in [0, 1]:
            create_form = cls.catalog.get_resource_form_for_create([])
            create_form.display_name = 'Test Resource ' + str(num)
            create_form.description = 'Test Resource for ResourceNotificationSession tests'
            obj = cls.catalog.create_resource(create_form)
            cls.resource_list.append(obj)
            cls.resource_ids.append(obj.ident)

    @classmethod
    def tearDownClass(cls):
        # Implemented from init template for ResourceLookupSession
        #for obj in cls.catalog.get_resources():
        #    cls.catalog.delete_resource(obj.ident)
        #for catalog in cls.catalogs:
        #    cls.svc_mgr.delete_bin(catalog.ident)
        for catalog in cls.svc_mgr.get_bins():
            for obj in catalog.get_resources():
                catalog.delete_resource(obj.ident)
            cls.svc_mgr.delete_bin(catalog.ident)


    def test_get_bin_id(self):
        """Tests get_bin_id"""
        self.assertEqual(self.catalog.get_bin_id(), self.catalog.ident)

    @unittest.skip('unimplemented test')
    def test_get_bin(self):
        """Tests get_bin"""
        pass

    @unittest.skip('unimplemented test')
    def test_can_register_for_resource_notifications(self):
        """Tests can_register_for_resource_notifications"""
        pass

    def test_use_federated_bin_view(self):
        """Tests use_federated_bin_view"""
        self.catalog.use_federated_bin_view()

    def test_use_isolated_bin_view(self):
        """Tests use_isolated_bin_view"""
        self.catalog.use_isolated_bin_view()

    @unittest.skip('unimplemented test')
    def test_register_for_new_resources(self):
        """Tests register_for_new_resources"""
        pass

    @unittest.skip('unimplemented test')
    def test_register_for_changed_resources(self):
        """Tests register_for_changed_resources"""
        pass

    @unittest.skip('unimplemented test')
    def test_register_for_changed_resource(self):
        """Tests register_for_changed_resource"""
        pass

    @unittest.skip('unimplemented test')
    def test_register_for_deleted_resources(self):
        """Tests register_for_deleted_resources"""
        pass

    @unittest.skip('unimplemented test')
    def test_register_for_deleted_resource(self):
        """Tests register_for_deleted_resource"""
        pass

    @unittest.skip('unimplemented test')
    def test_reliable_resource_notifications(self):
        """Tests reliable_resource_notifications"""
        pass

    @unittest.skip('unimplemented test')
    def test_unreliable_resource_notifications(self):
        """Tests unreliable_resource_notifications"""
        pass

    @unittest.skip('unimplemented test')
    def test_acknowledge_resource_notification(self):
        """Tests acknowledge_resource_notification"""
        pass


class TestResourceBinSession(unittest.TestCase):
    """Tests for ResourceBinSession"""

    @classmethod
    def setUpClass(cls):
        cls.resource_list = list()
        cls.resource_ids = list()
        cls.svc_mgr = Runtime().get_service_manager('RESOURCE', proxy=PROXY, implementation='TEST_SERVICE')
        create_form = cls.svc_mgr.get_bin_form_for_create([])
        create_form.display_name = 'Test Bin'
        create_form.description = 'Test Bin for ResourceBinSession tests'
        cls.catalog = cls.svc_mgr.create_bin(create_form)
        create_form = cls.svc_mgr.get_bin_form_for_create([])
        create_form.display_name = 'Test Bin for Assignment'
        create_form.description = 'Test Bin for ResourceBinSession tests assignment'
        cls.assigned_catalog = cls.svc_mgr.create_bin(create_form)
        for num in [0, 1, 2]:
            create_form = cls.catalog.get_resource_form_for_create([])
            create_form.display_name = 'Test Resource ' + str(num)
            create_form.description = 'Test Resource for ResourceBinSession tests'
            obj = cls.catalog.create_resource(create_form)
            cls.resource_list.append(obj)
            cls.resource_ids.append(obj.ident)
        cls.svc_mgr.assign_resource_to_bin(
            cls.resource_ids[1], cls.assigned_catalog.ident)
        cls.svc_mgr.assign_resource_to_bin(
            cls.resource_ids[2], cls.assigned_catalog.ident)

    @classmethod
    def tearDownClass(cls):
        #for obj in cls.catalog.get_resources():
        #    cls.catalog.delete_resource(obj.ident)
        #for catalog in cls.catalogs:
        #    cls.svc_mgr.delete_bin(catalog.ident)
        cls.svc_mgr.unassign_resource_from_bin(
            cls.resource_ids[1], cls.assigned_catalog.ident)
        cls.svc_mgr.unassign_resource_from_bin(
            cls.resource_ids[2], cls.assigned_catalog.ident)
        for catalog in cls.svc_mgr.get_bins():
            for obj in catalog.get_resources():
                catalog.delete_resource(obj.ident)
            cls.svc_mgr.delete_bin(catalog.ident)


    def test_use_comparative_bin_view(self):
        """Tests use_comparative_bin_view"""
        self.svc_mgr.use_comparative_bin_view()

    def test_use_plenary_bin_view(self):
        """Tests use_plenary_bin_view"""
        self.svc_mgr.use_plenary_bin_view()

    @unittest.skip('unimplemented test')
    def test_can_lookup_resource_bin_mappings(self):
        """Tests can_lookup_resource_bin_mappings"""
        pass

    def test_get_resource_ids_by_bin(self):
        """Tests get_resource_ids_by_bin"""
        objects = self.svc_mgr.get_resource_ids_by_bin(self.assigned_catalog.ident)
        self.assertEqual(objects.available(), 2)

    @unittest.skip('unimplemented test')
    def test_get_resources_by_bin(self):
        """Tests get_resources_by_bin"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_resource_ids_by_bins(self):
        """Tests get_resource_ids_by_bins"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_resources_by_bins(self):
        """Tests get_resources_by_bins"""
        pass

    def test_get_bin_ids_by_resource(self):
        """Tests get_bin_ids_by_resource"""
        cats = self.svc_mgr.get_bin_ids_by_resource(self.resource_ids[1])
        self.assertEqual(cats.available(), 2)

    def test_get_bins_by_resource(self):
        """Tests get_bins_by_resource"""
        cats = self.svc_mgr.get_bins_by_resource(self.resource_ids[1])
        self.assertEqual(cats.available(), 2)


class TestResourceBinAssignmentSession(unittest.TestCase):
    """Tests for ResourceBinAssignmentSession"""

    @unittest.skip('unimplemented test')
    def test_can_assign_resources(self):
        """Tests can_assign_resources"""
        pass

    @unittest.skip('unimplemented test')
    def test_can_assign_resources_to_bin(self):
        """Tests can_assign_resources_to_bin"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assignable_bin_ids(self):
        """Tests get_assignable_bin_ids"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assignable_bin_ids_for_resource(self):
        """Tests get_assignable_bin_ids_for_resource"""
        pass

    @unittest.skip('unimplemented test')
    def test_assign_resource_to_bin(self):
        """Tests assign_resource_to_bin"""
        pass

    @unittest.skip('unimplemented test')
    def test_unassign_resource_from_bin(self):
        """Tests unassign_resource_from_bin"""
        pass


class TestResourceAgentSession(unittest.TestCase):
    """Tests for ResourceAgentSession"""

    @classmethod
    def setUpClass(cls):
        cls.resource_list = list()
        cls.resource_ids = list()
        cls.svc_mgr = Runtime().get_service_manager('RESOURCE', proxy=PROXY, implementation='TEST_SERVICE')
        create_form = cls.svc_mgr.get_bin_form_for_create([])
        create_form.display_name = 'Test Bin'
        create_form.description = 'Test Bin for ResourceLookupSession tests'
        cls.catalog = cls.svc_mgr.create_bin(create_form)
        for num in [0, 1]:
            create_form = cls.catalog.get_resource_form_for_create([])
            create_form.display_name = 'Test Resource ' + str(num)
            create_form.description = 'Test Resource for ResourceLookupSession tests'
            obj = cls.catalog.create_resource(create_form)
            cls.resource_list.append(obj)
            cls.resource_ids.append(obj.ident)
        cls.catalog.assign_agent_to_resource(AGENT_ID_0, cls.resource_ids[0])
        cls.catalog.assign_agent_to_resource(AGENT_ID_1, cls.resource_ids[1])

    @classmethod
    def tearDownClass(cls):
        #for obj in cls.catalog.get_resources():
        #    cls.catalog.delete_resource(obj.ident)
        #for catalog in cls.catalogs:
        #    cls.svc_mgr.delete_bin(catalog.ident)
        for catalog in cls.svc_mgr.get_bins():
            for obj in catalog.get_resources():
                catalog.delete_resource(obj.ident)
            cls.svc_mgr.delete_bin(catalog.ident)


    def test_get_bin_id(self):
        """Tests get_bin_id"""
        self.assertEqual(self.catalog.get_bin_id(), self.catalog.ident)

    @unittest.skip('unimplemented test')
    def test_get_bin(self):
        """Tests get_bin"""
        pass

    @unittest.skip('unimplemented test')
    def test_can_lookup_resource_agent_mappings(self):
        """Tests can_lookup_resource_agent_mappings"""
        pass

    def test_use_comparative_agent_view(self):
        """Tests use_comparative_agent_view"""
        self.catalog.use_comparative_agent_view()

    def test_use_plenary_agent_view(self):
        """Tests use_plenary_agent_view"""
        self.catalog.use_plenary_agent_view()

    def test_use_federated_bin_view(self):
        """Tests use_federated_bin_view"""
        self.catalog.use_federated_bin_view()

    def test_use_isolated_bin_view(self):
        """Tests use_isolated_bin_view"""
        self.catalog.use_isolated_bin_view()

    def test_get_resource_id_by_agent(self):
        """Tests get_resource_id_by_agent"""
        resource_id = self.catalog.get_resource_id_by_agent(AGENT_ID_0)

    def test_get_resource_by_agent(self):
        """Tests get_resource_by_agent"""
        resource = self.catalog.get_resource_by_agent(AGENT_ID_1)
        self.assertEqual(resource.display_name.text, 'Test Resource 1')

    def test_get_agent_ids_by_resource(self):
        """Tests get_agent_ids_by_resource"""
        id_list = self.catalog.get_agent_ids_by_resource(self.resource_ids[0])
        self.assertEqual(id_list.next(), AGENT_ID_0)

    def test_get_agents_by_resource(self):
        """Tests get_agents_by_resource"""
        


class TestResourceAgentAssignmentSession(unittest.TestCase):
    """Tests for ResourceAgentAssignmentSession"""

    @classmethod
    def setUpClass(cls):
        cls.resource_list = list()
        cls.resource_ids = list()
        cls.svc_mgr = Runtime().get_service_manager('RESOURCE', proxy=PROXY, implementation='TEST_SERVICE')
        create_form = cls.svc_mgr.get_bin_form_for_create([])
        create_form.display_name = 'Test Bin'
        create_form.description = 'Test Bin for ResourceLookupSession tests'
        cls.catalog = cls.svc_mgr.create_bin(create_form)
        for num in [0, 1]:
            create_form = cls.catalog.get_resource_form_for_create([])
            create_form.display_name = 'Test Resource ' + str(num)
            create_form.description = 'Test Resource for ResourceLookupSession tests'
            obj = cls.catalog.create_resource(create_form)
            cls.resource_list.append(obj)
            cls.resource_ids.append(obj.ident)

    @classmethod
    def tearDownClass(cls):
        #for obj in cls.catalog.get_resources():
        #    cls.catalog.delete_resource(obj.ident)
        #for catalog in cls.catalogs:
        #    cls.svc_mgr.delete_bin(catalog.ident)
        for catalog in cls.svc_mgr.get_bins():
            for obj in catalog.get_resources():
                catalog.delete_resource(obj.ident)
            cls.svc_mgr.delete_bin(catalog.ident)


    def test_get_bin_id(self):
        """Tests get_bin_id"""
        self.assertEqual(self.catalog.get_bin_id(), self.catalog.ident)

    @unittest.skip('unimplemented test')
    def test_get_bin(self):
        """Tests get_bin"""
        pass

    @unittest.skip('unimplemented test')
    def test_can_assign_agents(self):
        """Tests can_assign_agents"""
        pass

    @unittest.skip('unimplemented test')
    def test_can_assign_agents_to_resource(self):
        """Tests can_assign_agents_to_resource"""
        pass

    def test_assign_agent_to_resource(self):
        """Tests assign_agent_to_resource"""
        self.catalog.assign_agent_to_resource(AGENT_ID_0, self.resource_ids[0])
        with self.assertRaises(errors.AlreadyExists):
            self.catalog.assign_agent_to_resource(AGENT_ID_0, self.resource_ids[1])

    def test_unassign_agent_from_resource(self):
        """Tests unassign_agent_from_resource"""
        self.catalog.assign_agent_to_resource(AGENT_ID_1, self.resource_ids[1])
        self.assertEqual(self.catalog.get_resource_by_agent(AGENT_ID_1).display_name.text, 'Test Resource 1')
        self.catalog.unassign_agent_from_resource(AGENT_ID_1, self.resource_ids[1])
        with self.assertRaises(errors.NotFound):
            self.catalog.get_resource_by_agent(AGENT_ID_1)


class TestBinLookupSession(unittest.TestCase):
    """Tests for BinLookupSession"""

    @classmethod
    def setUpClass(cls):
        cls.catalogs = list()
        cls.catalog_ids = list()
        cls.svc_mgr = Runtime().get_service_manager('RESOURCE', proxy=PROXY, implementation='TEST_SERVICE')
        for num in [0, 1]:
            create_form = cls.svc_mgr.get_bin_form_for_create([])
            create_form.display_name = 'Test Bin ' + str(num)
            create_form.description = 'Test Bin for resource proxy manager tests'
            catalog = cls.svc_mgr.create_bin(create_form)
            cls.catalogs.append(catalog)
            cls.catalog_ids.append(catalog.ident)

    @classmethod
    def tearDownClass(cls):
        #for catalog in cls.catalogs:
        #    cls.svc_mgr.delete_bin(catalog.ident)
        for catalog in cls.svc_mgr.get_bins():
            cls.svc_mgr.delete_bin(catalog.ident)


    def test_can_lookup_bins(self):
        """Tests can_lookup_bins"""
        self.assertTrue(isinstance(self.svc_mgr.can_lookup_bins(), bool))

    def test_use_comparative_bin_view(self):
        """Tests use_comparative_bin_view"""
        self.svc_mgr.use_comparative_bin_view()

    def test_use_plenary_bin_view(self):
        """Tests use_plenary_bin_view"""
        self.svc_mgr.use_plenary_bin_view()

    def test_get_bin(self):
        """Tests get_bin"""
        catalog = self.svc_mgr.get_bin(self.catalogs[0].ident)
        self.assertEqual(catalog.ident, self.catalogs[0].ident)

    def test_get_bins_by_ids(self):
        """Tests get_bins_by_ids"""
        catalogs = self.svc_mgr.get_bins_by_ids(self.catalog_ids)

    @unittest.skip('unimplemented test')
    def test_get_bins_by_genus_type(self):
        """Tests get_bins_by_genus_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_bins_by_parent_genus_type(self):
        """Tests get_bins_by_parent_genus_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_bins_by_record_type(self):
        """Tests get_bins_by_record_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_bins_by_provider(self):
        """Tests get_bins_by_provider"""
        pass

    def test_get_bins(self):
        """Tests get_bins"""
        catalogs = self.svc_mgr.get_bins()


class TestBinQuerySession(unittest.TestCase):
    """Tests for BinQuerySession"""

    @unittest.skip('unimplemented test')
    def test_can_search_bins(self):
        """Tests can_search_bins"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_bin_query(self):
        """Tests get_bin_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_bins_by_query(self):
        """Tests get_bins_by_query"""
        pass


class TestBinAdminSession(unittest.TestCase):
    """Tests for BinAdminSession"""

    @classmethod
    def setUpClass(cls):
        cls.svc_mgr = Runtime().get_service_manager('RESOURCE', proxy=PROXY, implementation='TEST_SERVICE')
        # Initialize test catalog:
        create_form = cls.svc_mgr.get_bin_form_for_create([])
        create_form.display_name = 'Test Bin'
        create_form.description = 'Test Bin for BinAdminSession tests'
        cls.catalog = cls.svc_mgr.create_bin(create_form)
        # Initialize catalog to be deleted:
        create_form = cls.svc_mgr.get_bin_form_for_create([])
        create_form.display_name = 'Test Bin For Deletion'
        create_form.description = 'Test Bin for BinAdminSession deletion test'
        cls.catalog_to_delete = cls.svc_mgr.create_bin(create_form)

    @classmethod
    def tearDownClass(cls):
        #for catalog in cls.catalogs:
        #    cls.svc_mgr.delete_bin(catalog.ident)
        for catalog in cls.svc_mgr.get_bins():
            cls.svc_mgr.delete_bin(catalog.ident)


    def test_can_create_bins(self):
        """Tests can_create_bins"""
        self.assertTrue(isinstance(self.svc_mgr.can_create_bins(), bool))

    def test_can_create_bin_with_record_types(self):
        """Tests can_create_bin_with_record_types"""
        self.assertTrue(isinstance(self.svc_mgr.can_create_bin_with_record_types(DEFAULT_TYPE), bool))

    def test_get_bin_form_for_create(self):
        """Tests get_bin_form_for_create"""
        from dlkit.abstract_osid.resource.objects import BinForm
        catalog_form = self.svc_mgr.get_bin_form_for_create([])
        self.assertTrue(isinstance(catalog_form, BinForm))
        self.assertFalse(catalog_form.is_for_update())

    def test_create_bin(self):
        """Tests create_bin"""
        from dlkit.abstract_osid.resource.objects import Bin
        catalog_form = self.svc_mgr.get_bin_form_for_create([])
        catalog_form.display_name = 'Test Bin'
        catalog_form.description = 'Test Bin for BinAdminSession.create_bin tests'
        new_catalog = self.svc_mgr.create_bin(catalog_form)
        self.assertTrue(isinstance(new_catalog, Bin))

    def test_can_update_bins(self):
        """Tests can_update_bins"""
        self.assertTrue(isinstance(self.svc_mgr.can_update_bins(), bool))

    def test_get_bin_form_for_update(self):
        """Tests get_bin_form_for_update"""
        from dlkit.abstract_osid.resource.objects import BinForm
        catalog_form = self.svc_mgr.get_bin_form_for_update(self.catalog.ident)
        self.assertTrue(isinstance(catalog_form, BinForm))
        self.assertTrue(catalog_form.is_for_update())

    def test_update_bin(self):
        """Tests update_bin"""
        catalog_form = self.svc_mgr.get_bin_form_for_update(self.catalog.ident)
        # Update some elements here?
        self.svc_mgr.update_bin(catalog_form)

    def test_can_delete_bins(self):
        """Tests can_delete_bins"""
        self.assertTrue(isinstance(self.svc_mgr.can_delete_bins(), bool))

    def test_delete_bin(self):
        """Tests delete_bin"""
        cat_id = self.catalog_to_delete.ident
        self.svc_mgr.delete_bin(cat_id)
        with self.assertRaises(errors.NotFound):
            self.svc_mgr.get_bin(cat_id)

    @unittest.skip('unimplemented test')
    def test_can_manage_bin_aliases(self):
        """Tests can_manage_bin_aliases"""
        pass

    @unittest.skip('unimplemented test')
    def test_alias_bin(self):
        """Tests alias_bin"""
        pass


class TestBinHierarchySession(unittest.TestCase):
    """Tests for BinHierarchySession"""

    @classmethod
    def setUpClass(cls):
        cls.svc_mgr = Runtime().get_service_manager('RESOURCE', proxy=PROXY, implementation='TEST_SERVICE')
        cls.catalogs = dict()
        for name in ['Root', 'Child 1', 'Child 2', 'Grandchild 1']:
            create_form = cls.svc_mgr.get_bin_form_for_create([])
            create_form.display_name = name
            create_form.description = 'Test Bin ' + name
            cls.catalogs[name] = cls.svc_mgr.create_bin(create_form)
        cls.svc_mgr.add_root_bin(cls.catalogs['Root'].ident)
        cls.svc_mgr.add_child_bin(cls.catalogs['Root'].ident, cls.catalogs['Child 1'].ident)
        cls.svc_mgr.add_child_bin(cls.catalogs['Root'].ident, cls.catalogs['Child 2'].ident)
        cls.svc_mgr.add_child_bin(cls.catalogs['Child 1'].ident, cls.catalogs['Grandchild 1'].ident)
    @classmethod
    def tearDownClass(cls):
        cls.svc_mgr.remove_child_bin(cls.catalogs['Child 1'].ident, cls.catalogs['Grandchild 1'].ident)
        cls.svc_mgr.remove_child_bins(cls.catalogs['Root'].ident)
        for cat_name in cls.catalogs:
            cls.svc_mgr.delete_bin(cls.catalogs[cat_name].ident)


    def test_get_bin_hierarchy_id(self):
        """Tests get_bin_hierarchy_id"""
        hierarchy_id = self.svc_mgr.get_bin_hierarchy_id()

    def test_get_bin_hierarchy(self):
        """Tests get_bin_hierarchy"""
        hierarchy = self.svc_mgr.get_bin_hierarchy()

    @unittest.skip('unimplemented test')
    def test_can_access_bin_hierarchy(self):
        """Tests can_access_bin_hierarchy"""
        pass

    def test_use_comparative_bin_view(self):
        """Tests use_comparative_bin_view"""
        self.svc_mgr.use_comparative_bin_view()

    def test_use_plenary_bin_view(self):
        """Tests use_plenary_bin_view"""
        self.svc_mgr.use_plenary_bin_view()

    def test_get_root_bin_ids(self):
        """Tests get_root_bin_ids"""
        root_ids = self.svc_mgr.get_root_bin_ids()

    def test_get_root_bins(self):
        """Tests get_root_bins"""
        roots = self.svc_mgr.get_root_bins()

    def test_has_parent_bins(self):
        """Tests has_parent_bins"""
        self.assertTrue(isinstance(self.svc_mgr.has_parent_bins(self.catalogs['Child 1'].ident), bool))
        self.assertTrue(self.svc_mgr.has_parent_bins(self.catalogs['Child 1'].ident))
        self.assertTrue(self.svc_mgr.has_parent_bins(self.catalogs['Child 2'].ident))
        self.assertTrue(self.svc_mgr.has_parent_bins(self.catalogs['Grandchild 1'].ident))
        self.assertFalse(self.svc_mgr.has_parent_bins(self.catalogs['Root'].ident))

    def test_is_parent_of_bin(self):
        """Tests is_parent_of_bin"""
        self.assertTrue(isinstance(self.svc_mgr.is_parent_of_bin(self.catalogs['Child 1'].ident, self.catalogs['Root'].ident), bool))
        self.assertTrue(self.svc_mgr.is_parent_of_bin(self.catalogs['Root'].ident, self.catalogs['Child 1'].ident))
        self.assertTrue(self.svc_mgr.is_parent_of_bin(self.catalogs['Child 1'].ident, self.catalogs['Grandchild 1'].ident))
        self.assertFalse(self.svc_mgr.is_parent_of_bin(self.catalogs['Child 1'].ident, self.catalogs['Root'].ident))

    def test_get_parent_bin_ids(self):
        """Tests get_parent_bin_ids"""
        from dlkit.abstract_osid.id.objects import IdList
        catalog_list = self.svc_mgr.get_parent_bin_ids(self.catalogs['Child 1'].ident)
        self.assertTrue(isinstance(catalog_list, IdList))
        self.assertEqual(catalog_list.available(), 1)

    def test_get_parent_bins(self):
        """Tests get_parent_bins"""
        from dlkit.abstract_osid.resource.objects import BinList
        catalog_list = self.svc_mgr.get_parent_bins(self.catalogs['Child 1'].ident)
        self.assertTrue(isinstance(catalog_list, BinList))
        self.assertEqual(catalog_list.available(), 1)
        self.assertEqual(catalog_list.next().display_name.text, 'Root')

    @unittest.skip('unimplemented test')
    def test_is_ancestor_of_bin(self):
        """Tests is_ancestor_of_bin"""
        pass

    def test_has_child_bins(self):
        """Tests has_child_bins"""
        self.assertTrue(isinstance(self.svc_mgr.has_child_bins(self.catalogs['Child 1'].ident), bool))
        self.assertTrue(self.svc_mgr.has_child_bins(self.catalogs['Root'].ident))
        self.assertTrue(self.svc_mgr.has_child_bins(self.catalogs['Child 1'].ident))
        self.assertFalse(self.svc_mgr.has_child_bins(self.catalogs['Child 2'].ident))
        self.assertFalse(self.svc_mgr.has_child_bins(self.catalogs['Grandchild 1'].ident))

    def test_is_child_of_bin(self):
        """Tests is_child_of_bin"""
        self.assertTrue(isinstance(self.svc_mgr.is_child_of_bin(self.catalogs['Child 1'].ident, self.catalogs['Root'].ident), bool))
        self.assertTrue(self.svc_mgr.is_child_of_bin(self.catalogs['Child 1'].ident, self.catalogs['Root'].ident))
        self.assertTrue(self.svc_mgr.is_child_of_bin(self.catalogs['Grandchild 1'].ident, self.catalogs['Child 1'].ident))
        self.assertFalse(self.svc_mgr.is_child_of_bin(self.catalogs['Root'].ident, self.catalogs['Child 1'].ident))

    def test_get_child_bin_ids(self):
        """Tests get_child_bin_ids"""
        from dlkit.abstract_osid.id.objects import IdList
        catalog_list = self.svc_mgr.get_child_bin_ids(self.catalogs['Child 1'].ident)
        self.assertTrue(isinstance(catalog_list, IdList))
        self.assertEqual(catalog_list.available(), 1)

    def test_get_child_bins(self):
        """Tests get_child_bins"""
        from dlkit.abstract_osid.resource.objects import BinList
        catalog_list = self.svc_mgr.get_child_bins(self.catalogs['Child 1'].ident)
        self.assertTrue(isinstance(catalog_list, BinList))
        self.assertEqual(catalog_list.available(), 1)
        self.assertEqual(catalog_list.next().display_name.text, 'Grandchild 1')

    @unittest.skip('unimplemented test')
    def test_is_descendant_of_bin(self):
        """Tests is_descendant_of_bin"""
        pass

    def test_get_bin_node_ids(self):
        """Tests get_bin_node_ids"""
        node_ids = self.svc_mgr.get_bin_node_ids(self.catalogs['Child 1'].ident, 1, 2, False)
        # add some tests on the returned node

    def test_get_bin_nodes(self):
        """Tests get_bin_nodes"""
        nodes = self.svc_mgr.get_bin_nodes(self.catalogs['Child 1'].ident, 1, 2, False)
        # add some tests on the returned node


class TestBinHierarchyDesignSession(unittest.TestCase):
    """Tests for BinHierarchyDesignSession"""

    @classmethod
    def setUpClass(cls):
        cls.svc_mgr = Runtime().get_service_manager('RESOURCE', proxy=PROXY, implementation='TEST_SERVICE')
        cls.catalogs = dict()
        for name in ['Root', 'Child 1', 'Child 2', 'Grandchild 1']:
            create_form = cls.svc_mgr.get_bin_form_for_create([])
            create_form.display_name = name
            create_form.description = 'Test Bin ' + name
            cls.catalogs[name] = cls.svc_mgr.create_bin(create_form)
        cls.svc_mgr.add_root_bin(cls.catalogs['Root'].ident)
        cls.svc_mgr.add_child_bin(cls.catalogs['Root'].ident, cls.catalogs['Child 1'].ident)
        cls.svc_mgr.add_child_bin(cls.catalogs['Root'].ident, cls.catalogs['Child 2'].ident)
        cls.svc_mgr.add_child_bin(cls.catalogs['Child 1'].ident, cls.catalogs['Grandchild 1'].ident)
    @classmethod
    def tearDownClass(cls):
        cls.svc_mgr.remove_child_bin(cls.catalogs['Child 1'].ident, cls.catalogs['Grandchild 1'].ident)
        cls.svc_mgr.remove_child_bins(cls.catalogs['Root'].ident)
        for cat_name in cls.catalogs:
            cls.svc_mgr.delete_bin(cls.catalogs[cat_name].ident)


    def test_get_bin_hierarchy_id(self):
        """Tests get_bin_hierarchy_id"""
        hierarchy_id = self.svc_mgr.get_bin_hierarchy_id()

    def test_get_bin_hierarchy(self):
        """Tests get_bin_hierarchy"""
        hierarchy = self.svc_mgr.get_bin_hierarchy()

    @unittest.skip('unimplemented test')
    def test_can_modify_bin_hierarchy(self):
        """Tests can_modify_bin_hierarchy"""
        pass

    @unittest.skip('unimplemented test')
    def test_add_root_bin(self):
        """Tests add_root_bin"""
        pass

    @unittest.skip('unimplemented test')
    def test_remove_root_bin(self):
        """Tests remove_root_bin"""
        pass

    @unittest.skip('unimplemented test')
    def test_add_child_bin(self):
        """Tests add_child_bin"""
        pass

    @unittest.skip('unimplemented test')
    def test_remove_child_bin(self):
        """Tests remove_child_bin"""
        pass

    @unittest.skip('unimplemented test')
    def test_remove_child_bins(self):
        """Tests remove_child_bins"""
        pass


