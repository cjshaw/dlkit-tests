"""Unit tests of resource objects."""

import unittest


class TestResource(unittest.TestCase):
    """Tests for Resource"""



    @unittest.skip('unimplemented test')
    def test_is_group(self):
        """Tests is_group"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_demographic(self):
        """Tests is_demographic"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_avatar(self):
        """Tests has_avatar"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_avatar_id(self):
        """Tests get_avatar_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_avatar(self):
        """Tests get_avatar"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_resource_record(self):
        """Tests get_resource_record"""
        pass


class TestResourceForm(unittest.TestCase):
    """Tests for ResourceForm"""



    @unittest.skip('unimplemented test')
    def test_get_group_metadata(self):
        """Tests get_group_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_group(self):
        """Tests set_group"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_group(self):
        """Tests clear_group"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_avatar_metadata(self):
        """Tests get_avatar_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_avatar(self):
        """Tests set_avatar"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_avatar(self):
        """Tests clear_avatar"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_resource_form_record(self):
        """Tests get_resource_form_record"""
        pass


class TestResourceList(unittest.TestCase):
    """Tests for ResourceList"""

    @unittest.skip('unimplemented test')
    def test_get_next_resource(self):
        """Tests get_next_resource"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_resources(self):
        """Tests get_next_resources"""
        pass


class TestResourceNode(unittest.TestCase):
    """Tests for ResourceNode"""

    @unittest.skip('unimplemented test')
    def test_get_resource(self):
        """Tests get_resource"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_parent_resource_nodes(self):
        """Tests get_parent_resource_nodes"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_child_resource_nodes(self):
        """Tests get_child_resource_nodes"""
        pass


class TestResourceNodeList(unittest.TestCase):
    """Tests for ResourceNodeList"""

    @unittest.skip('unimplemented test')
    def test_get_next_resource_node(self):
        """Tests get_next_resource_node"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_resource_nodes(self):
        """Tests get_next_resource_nodes"""
        pass


class TestBin(unittest.TestCase):
    """Tests for Bin"""



    @unittest.skip('unimplemented test')
    def test_get_bin_record(self):
        """Tests get_bin_record"""
        pass


class TestBinForm(unittest.TestCase):
    """Tests for BinForm"""



    @unittest.skip('unimplemented test')
    def test_get_bin_form_record(self):
        """Tests get_bin_form_record"""
        pass


class TestBinList(unittest.TestCase):
    """Tests for BinList"""

    @unittest.skip('unimplemented test')
    def test_get_next_bin(self):
        """Tests get_next_bin"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_bins(self):
        """Tests get_next_bins"""
        pass


class TestBinNode(unittest.TestCase):
    """Tests for BinNode"""

    @unittest.skip('unimplemented test')
    def test_get_bin(self):
        """Tests get_bin"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_parent_bin_nodes(self):
        """Tests get_parent_bin_nodes"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_child_bin_nodes(self):
        """Tests get_child_bin_nodes"""
        pass


class TestBinNodeList(unittest.TestCase):
    """Tests for BinNodeList"""

    @unittest.skip('unimplemented test')
    def test_get_next_bin_node(self):
        """Tests get_next_bin_node"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_bin_nodes(self):
        """Tests get_next_bin_nodes"""
        pass


