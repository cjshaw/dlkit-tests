"""Unit tests of grading searches."""

import unittest


class TestGradeSystemSearch(unittest.TestCase):
    """Tests for GradeSystemSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_grade_systems(self):
        """Tests search_among_grade_systems"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_grade_system_results(self):
        """Tests order_grade_system_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_grade_system_search_record(self):
        """Tests get_grade_system_search_record"""
        pass


class TestGradeSystemSearchResults(unittest.TestCase):
    """Tests for GradeSystemSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_grade_systems(self):
        """Tests get_grade_systems"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_grade_system_query_inspector(self):
        """Tests get_grade_system_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_grade_system_search_results_record(self):
        """Tests get_grade_system_search_results_record"""
        pass


class TestGradeEntrySearch(unittest.TestCase):
    """Tests for GradeEntrySearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_grade_entries(self):
        """Tests search_among_grade_entries"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_grade_entry_results(self):
        """Tests order_grade_entry_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_grade_entry_search_record(self):
        """Tests get_grade_entry_search_record"""
        pass


class TestGradeEntrySearchResults(unittest.TestCase):
    """Tests for GradeEntrySearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_grade_entries(self):
        """Tests get_grade_entries"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_grade_entry_query_inspector(self):
        """Tests get_grade_entry_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_grade_entry_search_results_record(self):
        """Tests get_grade_entry_search_results_record"""
        pass


class TestGradebookColumnSearch(unittest.TestCase):
    """Tests for GradebookColumnSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_gradebook_columns(self):
        """Tests search_among_gradebook_columns"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_gradebook_column_results(self):
        """Tests order_gradebook_column_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_gradebook_column_search_record(self):
        """Tests get_gradebook_column_search_record"""
        pass


class TestGradebookColumnSearchResults(unittest.TestCase):
    """Tests for GradebookColumnSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_gradebook_columns(self):
        """Tests get_gradebook_columns"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_gradebook_column_query_inspector(self):
        """Tests get_gradebook_column_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_gradebook_column_search_results_record(self):
        """Tests get_gradebook_column_search_results_record"""
        pass


class TestGradebookSearch(unittest.TestCase):
    """Tests for GradebookSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_gradebooks(self):
        """Tests search_among_gradebooks"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_gradebook_results(self):
        """Tests order_gradebook_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_gradebook_search_record(self):
        """Tests get_gradebook_search_record"""
        pass


class TestGradebookSearchResults(unittest.TestCase):
    """Tests for GradebookSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_gradebooks(self):
        """Tests get_gradebooks"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_gradebook_query_inspector(self):
        """Tests get_gradebook_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_gradebook_search_results_record(self):
        """Tests get_gradebook_search_results_record"""
        pass


