"""Unit tests of assessment objects."""

import unittest


class TestQuestion(unittest.TestCase):
    """Tests for Question"""



    @unittest.skip('unimplemented test')
    def test_get_question_record(self):
        """Tests get_question_record"""
        pass




class TestQuestionForm(unittest.TestCase):
    """Tests for QuestionForm"""



    @unittest.skip('unimplemented test')
    def test_get_question_form_record(self):
        """Tests get_question_form_record"""
        pass


class TestQuestionList(unittest.TestCase):
    """Tests for QuestionList"""

    @unittest.skip('unimplemented test')
    def test_get_next_question(self):
        """Tests get_next_question"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_questions(self):
        """Tests get_next_questions"""
        pass


class TestAnswer(unittest.TestCase):
    """Tests for Answer"""



    @unittest.skip('unimplemented test')
    def test_get_answer_record(self):
        """Tests get_answer_record"""
        pass


class TestAnswerForm(unittest.TestCase):
    """Tests for AnswerForm"""



    @unittest.skip('unimplemented test')
    def test_get_answer_form_record(self):
        """Tests get_answer_form_record"""
        pass


class TestAnswerList(unittest.TestCase):
    """Tests for AnswerList"""

    @unittest.skip('unimplemented test')
    def test_get_next_answer(self):
        """Tests get_next_answer"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_answers(self):
        """Tests get_next_answers"""
        pass


class TestItem(unittest.TestCase):
    """Tests for Item"""



    @unittest.skip('unimplemented test')
    def test_get_learning_objective_ids(self):
        """Tests get_learning_objective_ids"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_learning_objectives(self):
        """Tests get_learning_objectives"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_question_id(self):
        """Tests get_question_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_question(self):
        """Tests get_question"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_answer_ids(self):
        """Tests get_answer_ids"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_answers(self):
        """Tests get_answers"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_item_record(self):
        """Tests get_item_record"""
        pass

        pass


class TestItemForm(unittest.TestCase):
    """Tests for ItemForm"""



    @unittest.skip('unimplemented test')
    def test_get_learning_objectives_metadata(self):
        """Tests get_learning_objectives_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_learning_objectives(self):
        """Tests set_learning_objectives"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_learning_objectives(self):
        """Tests clear_learning_objectives"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_item_form_record(self):
        """Tests get_item_form_record"""
        pass


class TestItemList(unittest.TestCase):
    """Tests for ItemList"""

    @unittest.skip('unimplemented test')
    def test_get_next_item(self):
        """Tests get_next_item"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_items(self):
        """Tests get_next_items"""
        pass


class TestAssessment(unittest.TestCase):
    """Tests for Assessment"""



    @unittest.skip('unimplemented test')
    def test_get_level_id(self):
        """Tests get_level_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_level(self):
        """Tests get_level"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_rubric(self):
        """Tests has_rubric"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_rubric_id(self):
        """Tests get_rubric_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_rubric(self):
        """Tests get_rubric"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_record(self):
        """Tests get_assessment_record"""
        pass


class TestAssessmentForm(unittest.TestCase):
    """Tests for AssessmentForm"""



    @unittest.skip('unimplemented test')
    def test_get_level_metadata(self):
        """Tests get_level_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_level(self):
        """Tests set_level"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_level(self):
        """Tests clear_level"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_rubric_metadata(self):
        """Tests get_rubric_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_rubric(self):
        """Tests set_rubric"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_rubric(self):
        """Tests clear_rubric"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_form_record(self):
        """Tests get_assessment_form_record"""
        pass


class TestAssessmentList(unittest.TestCase):
    """Tests for AssessmentList"""

    @unittest.skip('unimplemented test')
    def test_get_next_assessment(self):
        """Tests get_next_assessment"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_assessments(self):
        """Tests get_next_assessments"""
        pass


class TestAssessmentOffered(unittest.TestCase):
    """Tests for AssessmentOffered"""



    @unittest.skip('unimplemented test')
    def test_get_assessment_id(self):
        """Tests get_assessment_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment(self):
        """Tests get_assessment"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_level_id(self):
        """Tests get_level_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_level(self):
        """Tests get_level"""
        pass

    @unittest.skip('unimplemented test')
    def test_are_items_sequential(self):
        """Tests are_items_sequential"""
        pass

    @unittest.skip('unimplemented test')
    def test_are_items_shuffled(self):
        """Tests are_items_shuffled"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_start_time(self):
        """Tests has_start_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_start_time(self):
        """Tests get_start_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_deadline(self):
        """Tests has_deadline"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_deadline(self):
        """Tests get_deadline"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_duration(self):
        """Tests has_duration"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_duration(self):
        """Tests get_duration"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_scored(self):
        """Tests is_scored"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_score_system_id(self):
        """Tests get_score_system_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_score_system(self):
        """Tests get_score_system"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_graded(self):
        """Tests is_graded"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_grade_system_id(self):
        """Tests get_grade_system_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_grade_system(self):
        """Tests get_grade_system"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_rubric(self):
        """Tests has_rubric"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_rubric_id(self):
        """Tests get_rubric_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_rubric(self):
        """Tests get_rubric"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_offered_record(self):
        """Tests get_assessment_offered_record"""
        pass

        pass


class TestAssessmentOfferedForm(unittest.TestCase):
    """Tests for AssessmentOfferedForm"""



    @unittest.skip('unimplemented test')
    def test_get_level_metadata(self):
        """Tests get_level_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_level(self):
        """Tests set_level"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_level(self):
        """Tests clear_level"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_items_sequential_metadata(self):
        """Tests get_items_sequential_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_items_sequential(self):
        """Tests set_items_sequential"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_items_sequential(self):
        """Tests clear_items_sequential"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_items_shuffled_metadata(self):
        """Tests get_items_shuffled_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_items_shuffled(self):
        """Tests set_items_shuffled"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_items_shuffled(self):
        """Tests clear_items_shuffled"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_start_time_metadata(self):
        """Tests get_start_time_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_start_time(self):
        """Tests set_start_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_start_time(self):
        """Tests clear_start_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_deadline_metadata(self):
        """Tests get_deadline_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_deadline(self):
        """Tests set_deadline"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_deadline(self):
        """Tests clear_deadline"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_duration_metadata(self):
        """Tests get_duration_metadata"""
        pass

    def test_set_duration(self):
        """Tests set_duration"""
       

    @unittest.skip('unimplemented test')
    def test_clear_duration(self):
        """Tests clear_duration"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_score_system_metadata(self):
        """Tests get_score_system_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_score_system(self):
        """Tests set_score_system"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_score_system(self):
        """Tests clear_score_system"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_grade_system_metadata(self):
        """Tests get_grade_system_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_grade_system(self):
        """Tests set_grade_system"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_grade_system(self):
        """Tests clear_grade_system"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_offered_form_record(self):
        """Tests get_assessment_offered_form_record"""
        pass


class TestAssessmentOfferedList(unittest.TestCase):
    """Tests for AssessmentOfferedList"""

    @unittest.skip('unimplemented test')
    def test_get_next_assessment_offered(self):
        """Tests get_next_assessment_offered"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_assessments_offered(self):
        """Tests get_next_assessments_offered"""
        pass


class TestAssessmentTaken(unittest.TestCase):
    """Tests for AssessmentTaken"""



    @unittest.skip('unimplemented test')
    def test_get_assessment_offered_id(self):
        """Tests get_assessment_offered_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_offered(self):
        """Tests get_assessment_offered"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_taker_id(self):
        """Tests get_taker_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_taker(self):
        """Tests get_taker"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_taking_agent_id(self):
        """Tests get_taking_agent_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_taking_agent(self):
        """Tests get_taking_agent"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_started(self):
        """Tests has_started"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_actual_start_time(self):
        """Tests get_actual_start_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_ended(self):
        """Tests has_ended"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_completion_time(self):
        """Tests get_completion_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_time_spent(self):
        """Tests get_time_spent"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_completion(self):
        """Tests get_completion"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_scored(self):
        """Tests is_scored"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_score_system_id(self):
        """Tests get_score_system_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_score_system(self):
        """Tests get_score_system"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_score(self):
        """Tests get_score"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_graded(self):
        """Tests is_graded"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_grade_id(self):
        """Tests get_grade_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_grade(self):
        """Tests get_grade"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_feedback(self):
        """Tests get_feedback"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_rubric(self):
        """Tests has_rubric"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_rubric_id(self):
        """Tests get_rubric_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_rubric(self):
        """Tests get_rubric"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_taken_record(self):
        """Tests get_assessment_taken_record"""
        pass




class TestAssessmentTakenForm(unittest.TestCase):
    """Tests for AssessmentTakenForm"""



    @unittest.skip('unimplemented test')
    def test_get_taker_metadata(self):
        """Tests get_taker_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_taker(self):
        """Tests set_taker"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_taker(self):
        """Tests clear_taker"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_taken_form_record(self):
        """Tests get_assessment_taken_form_record"""
        pass


class TestAssessmentTakenList(unittest.TestCase):
    """Tests for AssessmentTakenList"""

    @unittest.skip('unimplemented test')
    def test_get_next_assessment_taken(self):
        """Tests get_next_assessment_taken"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_assessments_taken(self):
        """Tests get_next_assessments_taken"""
        pass


class TestAssessmentSection(unittest.TestCase):
    """Tests for AssessmentSection"""



    @unittest.skip('unimplemented test')
    def test_get_assessment_taken_id(self):
        """Tests get_assessment_taken_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_taken(self):
        """Tests get_assessment_taken"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_allocated_time(self):
        """Tests has_allocated_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_allocated_time(self):
        """Tests get_allocated_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_are_items_sequential(self):
        """Tests are_items_sequential"""
        pass

    @unittest.skip('unimplemented test')
    def test_are_items_shuffled(self):
        """Tests are_items_shuffled"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_section_record(self):
        """Tests get_assessment_section_record"""
        pass


class TestAssessmentSectionList(unittest.TestCase):
    """Tests for AssessmentSectionList"""

    @unittest.skip('unimplemented test')
    def test_get_next_assessment_section(self):
        """Tests get_next_assessment_section"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_assessment_sections(self):
        """Tests get_next_assessment_sections"""
        pass


class TestBank(unittest.TestCase):
    """Tests for Bank"""



    @unittest.skip('unimplemented test')
    def test_get_bank_record(self):
        """Tests get_bank_record"""
        pass


class TestBankForm(unittest.TestCase):
    """Tests for BankForm"""



    @unittest.skip('unimplemented test')
    def test_get_bank_form_record(self):
        """Tests get_bank_form_record"""
        pass


class TestBankList(unittest.TestCase):
    """Tests for BankList"""

    @unittest.skip('unimplemented test')
    def test_get_next_bank(self):
        """Tests get_next_bank"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_banks(self):
        """Tests get_next_banks"""
        pass


class TestBankNode(unittest.TestCase):
    """Tests for BankNode"""

    @unittest.skip('unimplemented test')
    def test_get_bank(self):
        """Tests get_bank"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_parent_bank_nodes(self):
        """Tests get_parent_bank_nodes"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_child_bank_nodes(self):
        """Tests get_child_bank_nodes"""
        pass


class TestBankNodeList(unittest.TestCase):
    """Tests for BankNodeList"""

    @unittest.skip('unimplemented test')
    def test_get_next_bank_node(self):
        """Tests get_next_bank_node"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_bank_nodes(self):
        """Tests get_next_bank_nodes"""
        pass


class TestResponseList(unittest.TestCase):
    """Tests for ResponseList"""

    @unittest.skip('unimplemented test')
    def test_get_next_response(self):
        """Tests get_next_response"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_responses(self):
        """Tests get_next_responses"""
        pass


