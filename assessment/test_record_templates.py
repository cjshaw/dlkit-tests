"""Unit tests of assessment records."""

import unittest


class TestQuestionRecord(unittest.TestCase):
    """Tests for QuestionRecord"""




class TestQuestionQueryRecord(unittest.TestCase):
    """Tests for QuestionQueryRecord"""




class TestQuestionFormRecord(unittest.TestCase):
    """Tests for QuestionFormRecord"""




class TestAnswerRecord(unittest.TestCase):
    """Tests for AnswerRecord"""




class TestAnswerQueryRecord(unittest.TestCase):
    """Tests for AnswerQueryRecord"""




class TestAnswerFormRecord(unittest.TestCase):
    """Tests for AnswerFormRecord"""




class TestItemRecord(unittest.TestCase):
    """Tests for ItemRecord"""




class TestItemQueryRecord(unittest.TestCase):
    """Tests for ItemQueryRecord"""




class TestItemFormRecord(unittest.TestCase):
    """Tests for ItemFormRecord"""




class TestItemSearchRecord(unittest.TestCase):
    """Tests for ItemSearchRecord"""




class TestAssessmentRecord(unittest.TestCase):
    """Tests for AssessmentRecord"""




class TestAssessmentQueryRecord(unittest.TestCase):
    """Tests for AssessmentQueryRecord"""




class TestAssessmentFormRecord(unittest.TestCase):
    """Tests for AssessmentFormRecord"""




class TestAssessmentSearchRecord(unittest.TestCase):
    """Tests for AssessmentSearchRecord"""




class TestAssessmentOfferedRecord(unittest.TestCase):
    """Tests for AssessmentOfferedRecord"""




class TestAssessmentOfferedQueryRecord(unittest.TestCase):
    """Tests for AssessmentOfferedQueryRecord"""




class TestAssessmentOfferedFormRecord(unittest.TestCase):
    """Tests for AssessmentOfferedFormRecord"""




class TestAssessmentOfferedSearchRecord(unittest.TestCase):
    """Tests for AssessmentOfferedSearchRecord"""




class TestAssessmentTakenRecord(unittest.TestCase):
    """Tests for AssessmentTakenRecord"""




class TestAssessmentTakenQueryRecord(unittest.TestCase):
    """Tests for AssessmentTakenQueryRecord"""




class TestAssessmentTakenFormRecord(unittest.TestCase):
    """Tests for AssessmentTakenFormRecord"""




class TestAssessmentTakenSearchRecord(unittest.TestCase):
    """Tests for AssessmentTakenSearchRecord"""




class TestAssessmentSectionRecord(unittest.TestCase):
    """Tests for AssessmentSectionRecord"""




class TestBankRecord(unittest.TestCase):
    """Tests for BankRecord"""




class TestBankQueryRecord(unittest.TestCase):
    """Tests for BankQueryRecord"""




class TestBankFormRecord(unittest.TestCase):
    """Tests for BankFormRecord"""




class TestBankSearchRecord(unittest.TestCase):
    """Tests for BankSearchRecord"""




class TestResponseRecord(unittest.TestCase):
    """Tests for ResponseRecord"""




