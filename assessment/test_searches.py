"""Unit tests of assessment searches."""

import unittest


class TestItemSearch(unittest.TestCase):
    """Tests for ItemSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_items(self):
        """Tests search_among_items"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_item_results(self):
        """Tests order_item_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_item_search_record(self):
        """Tests get_item_search_record"""
        pass


class TestItemSearchResults(unittest.TestCase):
    """Tests for ItemSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_items(self):
        """Tests get_items"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_item_query_inspector(self):
        """Tests get_item_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_item_search_results_record(self):
        """Tests get_item_search_results_record"""
        pass


class TestAssessmentSearch(unittest.TestCase):
    """Tests for AssessmentSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_assessments(self):
        """Tests search_among_assessments"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_assessment_results(self):
        """Tests order_assessment_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_search_record(self):
        """Tests get_assessment_search_record"""
        pass


class TestAssessmentSearchResults(unittest.TestCase):
    """Tests for AssessmentSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_assessments(self):
        """Tests get_assessments"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_query_inspector(self):
        """Tests get_assessment_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_search_results_record(self):
        """Tests get_assessment_search_results_record"""
        pass


class TestAssessmentOfferedSearch(unittest.TestCase):
    """Tests for AssessmentOfferedSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_assessments_offered(self):
        """Tests search_among_assessments_offered"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_assessment_offered_results(self):
        """Tests order_assessment_offered_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_offered_search_record(self):
        """Tests get_assessment_offered_search_record"""
        pass


class TestAssessmentOfferedSearchResults(unittest.TestCase):
    """Tests for AssessmentOfferedSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_assessments_offered(self):
        """Tests get_assessments_offered"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_offered_query_inspector(self):
        """Tests get_assessment_offered_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_offered_search_results_record(self):
        """Tests get_assessment_offered_search_results_record"""
        pass


class TestAssessmentTakenSearch(unittest.TestCase):
    """Tests for AssessmentTakenSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_assessments_taken(self):
        """Tests search_among_assessments_taken"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_assessment_taken_results(self):
        """Tests order_assessment_taken_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_taken_search_record(self):
        """Tests get_assessment_taken_search_record"""
        pass


class TestAssessmentTakenSearchResults(unittest.TestCase):
    """Tests for AssessmentTakenSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_assessments_taken(self):
        """Tests get_assessments_taken"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_taken_query_inspector(self):
        """Tests get_assessment_taken_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_taken_search_results_record(self):
        """Tests get_assessment_taken_search_results_record"""
        pass


class TestBankSearch(unittest.TestCase):
    """Tests for BankSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_banks(self):
        """Tests search_among_banks"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_bank_results(self):
        """Tests order_bank_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_bank_search_record(self):
        """Tests get_bank_search_record"""
        pass


class TestBankSearchResults(unittest.TestCase):
    """Tests for BankSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_banks(self):
        """Tests get_banks"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_bank_query_inspector(self):
        """Tests get_bank_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_bank_search_results_record(self):
        """Tests get_bank_search_results_record"""
        pass


