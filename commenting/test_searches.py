"""Unit tests of commenting searches."""

import unittest


class TestCommentSearch(unittest.TestCase):
    """Tests for CommentSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_comments(self):
        """Tests search_among_comments"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_comment_results(self):
        """Tests order_comment_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_comment_search_record(self):
        """Tests get_comment_search_record"""
        pass


class TestCommentSearchResults(unittest.TestCase):
    """Tests for CommentSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_comments(self):
        """Tests get_comments"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_comment_query_inspector(self):
        """Tests get_comment_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_comment_search_results_record(self):
        """Tests get_comment_search_results_record"""
        pass


class TestBookSearch(unittest.TestCase):
    """Tests for BookSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_books(self):
        """Tests search_among_books"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_book_results(self):
        """Tests order_book_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_book_search_record(self):
        """Tests get_book_search_record"""
        pass


class TestBookSearchResults(unittest.TestCase):
    """Tests for BookSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_books(self):
        """Tests get_books"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_book_query_inspector(self):
        """Tests get_book_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_book_search_results_record(self):
        """Tests get_book_search_results_record"""
        pass


