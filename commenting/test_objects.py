"""Unit tests of commenting objects."""

import unittest


class TestComment(unittest.TestCase):
    """Tests for Comment"""



    @unittest.skip('unimplemented test')
    def test_get_reference_id(self):
        """Tests get_reference_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_commentor_id(self):
        """Tests get_commentor_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_commentor(self):
        """Tests get_commentor"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_commenting_agent_id(self):
        """Tests get_commenting_agent_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_commenting_agent(self):
        """Tests get_commenting_agent"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_text(self):
        """Tests get_text"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_rating(self):
        """Tests has_rating"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_rating_id(self):
        """Tests get_rating_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_rating(self):
        """Tests get_rating"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_comment_record(self):
        """Tests get_comment_record"""
        pass


class TestCommentForm(unittest.TestCase):
    """Tests for CommentForm"""



    @unittest.skip('unimplemented test')
    def test_get_text_metadata(self):
        """Tests get_text_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_text(self):
        """Tests set_text"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_text(self):
        """Tests clear_text"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_rating_metadata(self):
        """Tests get_rating_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_rating(self):
        """Tests set_rating"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_rating(self):
        """Tests clear_rating"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_comment_form_record(self):
        """Tests get_comment_form_record"""
        pass


class TestCommentList(unittest.TestCase):
    """Tests for CommentList"""

    @unittest.skip('unimplemented test')
    def test_get_next_comment(self):
        """Tests get_next_comment"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_comments(self):
        """Tests get_next_comments"""
        pass


class TestBook(unittest.TestCase):
    """Tests for Book"""



    @unittest.skip('unimplemented test')
    def test_get_book_record(self):
        """Tests get_book_record"""
        pass


class TestBookForm(unittest.TestCase):
    """Tests for BookForm"""



    @unittest.skip('unimplemented test')
    def test_get_book_form_record(self):
        """Tests get_book_form_record"""
        pass


class TestBookList(unittest.TestCase):
    """Tests for BookList"""

    @unittest.skip('unimplemented test')
    def test_get_next_book(self):
        """Tests get_next_book"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_books(self):
        """Tests get_next_books"""
        pass


class TestBookNode(unittest.TestCase):
    """Tests for BookNode"""

    @unittest.skip('unimplemented test')
    def test_get_book(self):
        """Tests get_book"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_parent_book_nodes(self):
        """Tests get_parent_book_nodes"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_child_book_nodes(self):
        """Tests get_child_book_nodes"""
        pass


class TestBookNodeList(unittest.TestCase):
    """Tests for BookNodeList"""

    @unittest.skip('unimplemented test')
    def test_get_next_book_node(self):
        """Tests get_next_book_node"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_book_nodes(self):
        """Tests get_next_book_nodes"""
        pass


