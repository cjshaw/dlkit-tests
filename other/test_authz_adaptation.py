"""Unit tests of authz adapter functionality."""

from ..authorization.test_sessions import TestAuthorizationSession
import unittest
from dlkit_runtime import PROXY_SESSION, proxy_example
from dlkit_runtime.managers import Runtime
JANE_REQUEST = proxy_example.TestRequest(username='jane_doe')
JANE_CONDITION = PROXY_SESSION.get_proxy_condition()
JANE_CONDITION.set_http_request(JANE_REQUEST)
JANE_PROXY = PROXY_SESSION.get_proxy(JANE_CONDITION)

from dlkit.abstract_osid.osid import errors
from dlkit.primordium.type.primitives import Type
BLUE_TYPE = Type(authority='BLUE',
                 namespace='BLUE',
                 identifier='BLUE')

class TestAuthzAdapter(TestAuthorizationSession):

    @classmethod
    def setUpClass(cls):
        TestAuthorizationSession.setUpClass()
        cls.resource_id_lists = []
        count = 0
        for bin in cls.bin_list:
            # print bin.display_name.text, "ID =", bin.ident.identifier
            cls.resource_id_lists.append([])
            for color in ['Red', 'Blue', 'Red']:
                create_form = bin.get_resource_form_for_create([])
                create_form.display_name = color + ' ' + str(count) + ' Resource'
                create_form.description = color + ' resource for authz adapter tests from Bin number ' + str(count)
                if color == 'Blue':
                    create_form.genus_type = BLUE_TYPE
                resource = bin.create_resource(create_form)
                if count == 2 and color == 'Blue':
                    cls.resource_mgr.assign_resource_to_bin(resource.ident, cls.bin_id_list[7])
                cls.resource_id_lists[count].append(resource.ident)
            count += 1

    @classmethod
    def tearDownClass(cls):
        # print 'BIN COUNT', len(cls.bin_list)
        # for bin in cls.bin_list:
        #     print 'RESOURCES COUNT', bin.get_resources().available()
        #     for resource in bin.get_resources():
        #         bin.delete_resource(resource.ident)
        TestAuthorizationSession.tearDownClass()

        # The hierarchy should look like this. (t) indicates where lookup is
        # explicitely authorized:
        #
        #            _____ 0 _____
        #           |             |
        #        _ 1(t) _         2
        #       |        |        |     not in hierarchy:
        #       3        4       5(t)      6     7(t)   (the 'blue' resource in bin 2 is also assigned to bin 7)

    def test_lookup_bank_0_plenary_isolated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[0])
        bin.use_isolated_bin_view()
        bin.use_plenary_resource_view()
        # with self.assertRaises(errors.NotFound):
        #     resources = bin.get_resources()
        # with self.assertRaises(errors.NotFound):
        #     resources = bin.get_resources_by_genus_type(BLUE_TYPE)
        # for resource_id in self.resource_id_lists[0]:
        #     with self.assertRaises(errors.NotFound):
        #         resource = bin.get_resource(resource_id)
        # with self.assertRaises(errors.NotFound):
        #     resources = bin.get_resources_by_ids(self.resource_id_lists[0])

    def test_lookup_bank_0_plenary_federated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[0])
        bin.use_federated_bin_view()
        bin.use_plenary_resource_view()
        self.assertTrue(bin.can_lookup_resources())
        self.assertEqual(bin.get_resources().available(), 1)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 1)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).next().ident, self.resource_id_lists[2][1])
        bin.get_resource(self.resource_id_lists[2][1])
        for resource_num in [0, 2]:
            with self.assertRaises(errors.NotFound): # Is this right?  Perhaps PermissionDenied
                resource = bin.get_resource(self.resource_id_lists[2][resource_num])

    def test_lookup_bank_0_comparative_federated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[0])
        bin.use_federated_bin_view()
        bin.use_comparative_resource_view()
        # print "START"
        self.assertEqual(bin.get_resources().available(), 13)
        # print "1"
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 5)
        # print "2"
        for resource in bin.get_resources():
            bin.get_resource(resource.ident)
        # print "3" (The previous is the long one)
        resource_ids = [resource.ident for resource in bin.get_resources()]
        bin.get_resources_by_ids(resource_ids)
        # print "4"
        for resource_id in self.resource_id_lists[0]:
            with self.assertRaises(errors.NotFound):
                resource = bin.get_resource(resource_id)
        # print "5"
        resource = bin.get_resource(self.resource_id_lists[2][1])
        # print "6"
        for resource_num in [0, 2]:
            with self.assertRaises(errors.NotFound):
                resource = bin.get_resource(self.resource_id_lists[2][resource_num])
        # print "7"
        for resource_id in self.resource_id_lists[1]:
                resource = bin.get_resource(resource_id)
        # print "8"
        for resource_id in self.resource_id_lists[3]:
                resource = bin.get_resource(resource_id)
        # print "9"
        for resource_id in self.resource_id_lists[4]:
                resource = bin.get_resource(resource_id)
        # print "10"
        for resource_id in self.resource_id_lists[5]:
                resource = bin.get_resource(resource_id)

    def test_lookup_bank_0_comparative_isolated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[0])
        bin.use_isolated_bin_view()
        bin.use_comparative_resource_view()
        self.assertEqual(bin.get_resources().available(), 0)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 0)

    def test_lookup_bank_1_plenary_isolated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[1])
        bin.use_isolated_bin_view()
        bin.use_plenary_resource_view()
        self.assertEqual(bin.get_resources().available(), 3)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 1)

    def test_lookup_bank_1_plenary_federated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[1])
        bin.use_federated_bin_view()
        bin.use_plenary_resource_view()
        self.assertEqual(bin.get_resources().available(), 9)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 3)

    def test_lookup_bank_1_comparative_federated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[1])
        bin.use_federated_bin_view()
        bin.use_comparative_resource_view()
        self.assertEqual(bin.get_resources().available(), 9)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 3)

    def test_lookup_bank_1_comparative_isolated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[1])
        bin.use_isolated_bin_view()
        bin.use_comparative_resource_view()
        self.assertEqual(bin.get_resources().available(), 3)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 1)


    def test_lookup_bank_2_plenary_isolated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[2])
        bin.use_isolated_bin_view()
        bin.use_plenary_resource_view()
        self.assertEqual(bin.get_resources().available(), 1)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 1)
        # with self.assertRaises(errors.PermissionDenied):
        #     resources = bin.get_resources()
        # with self.assertRaises(errors.PermissionDenied):
        #     resources = bin.get_resources_by_genus_type(BLUE_TYPE)

    def test_lookup_bank_2_plenary_federated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[2])
        bin.use_federated_bin_view()
        bin.use_plenary_resource_view()
        self.assertEqual(bin.get_resources().available(), 1)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 1)
        # with self.assertRaises(errors.PermissionDenied):
        #     resources = bin.get_resources()
        # with self.assertRaises(errors.PermissionDenied):
        #     resources = bin.get_resources_by_genus_type(BLUE_TYPE)

    def test_lookup_bank_2_comparative_federated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[2])
        bin.use_federated_bin_view()
        bin.use_comparative_resource_view()
        self.assertEqual(bin.get_resources().available(), 4)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 2)
        # self.assertEqual(bin.get_resources().available(), 3)
        # self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 1)

    def test_lookup_bank_2_comparative_isolated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[2])
        bin.use_isolated_bin_view()
        bin.use_comparative_resource_view()
        self.assertEqual(bin.get_resources().available(), 1)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 1)
        # with self.assertRaises(errors.PermissionDenied):
        #     resources = bin.get_resources()
        # with self.assertRaises(errors.PermissionDenied):
        #     resources = bin.get_resources_by_genus_type(BLUE_TYPE)


    def test_lookup_bank_3_plenary_isolated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[3])
        bin.use_isolated_bin_view()
        bin.use_plenary_resource_view()
        self.assertEqual(bin.get_resources().available(), 3)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 1)

    def test_lookup_bank_3_plenary_federated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[3])
        bin.use_federated_bin_view()
        bin.use_plenary_resource_view()
        self.assertEqual(bin.get_resources().available(), 3)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 1)

    def test_lookup_bank_3_comparative_federated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[3])
        bin.use_federated_bin_view()
        bin.use_comparative_resource_view()
        self.assertEqual(bin.get_resources().available(), 3)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 1)

    def test_lookup_bank_3_comparative_isolated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[3])
        bin.use_isolated_bin_view()
        bin.use_comparative_resource_view()
        self.assertEqual(bin.get_resources().available(), 3)
        self.assertEqual(bin.get_resources_by_genus_type(BLUE_TYPE).available(), 1)

    def test_query_bank_0_isolated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[0])
        bin.use_isolated_bin_view()
        with self.assertRaises(errors.PermissionDenied):
            query = bin.get_resource_query()

    def test_query_bank_0_federated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[0])
        bin.use_federated_bin_view()
        query = bin.get_resource_query()
        query.match_display_name('red')
        self.assertEqual(bin.get_resources_by_query(query).available(), 8)
        query.clear_display_name_terms()
        query.match_display_name('blue')
        self.assertEqual(bin.get_resources_by_query(query).available(), 5)
        

    def test_query_bank_1_isolated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[1])
        bin.use_isolated_bin_view()
        query = bin.get_resource_query()
        query.match_display_name('red')
        self.assertEqual(bin.get_resources_by_query(query).available(), 2)

    def test_query_bank_1_federated(self):
        janes_resource_mgr = Runtime().get_service_manager('RESOURCE', proxy=JANE_PROXY, implementation='TEST_SERVICE')
        bin = janes_resource_mgr.get_bin(self.bin_id_list[1])
        bin.use_federated_bin_view()
        query = bin.get_resource_query()
        query.match_display_name('red')
        self.assertEqual(bin.get_resources_by_query(query).available(), 6)
