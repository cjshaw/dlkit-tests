"""useful utility methods"""
import os
import json
import envoy

from django.conf import settings
from dlkit_runtime import RUNTIME, PROXY_SESSION
import dlkit_runtime.configs

from minimocktest import MockTestCase
from django.test import TestCase
from django.test.utils import override_settings

from dlkit_runtime.primordium import Id, DataInputStream, Type
from dlkit_runtime.proxy_example import TestRequest

from records.registry import ASSESSMENT_RECORD_TYPES

SIMPLE_SEQUENCE_ASSESSMENT_RECORD = Type(**ASSESSMENT_RECORD_TYPES['simple-child-sequencing'])

from .authorization import create_authz_superuser, add_user_authz_to_settings


PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
ABS_PATH = os.path.abspath(os.path.join(PROJECT_PATH, os.pardir))

TEST_BANK_GENUS = Type('assessment.Bank%3Atest-catalog%40ODL.MIT.EDU')
TEST_BIN_GENUS = Type('resource.Bin%3Atest-catalog%40ODL.MIT.EDU')
TEST_GRADEBOOK_GENUS = Type('grading.Gradebook%3Atest-catalog%40ODL.MIT.EDU')
TEST_LOG_GENUS = Type('logging.Log%3Atest-catalog%40ODL.MIT.EDU')
TEST_OBJECTIVE_BANK_GENUS = Type('learning.ObjectiveBank%3Atest-catalog%40ODL.MIT.EDU')
TEST_REPOSITORY_GENUS = Type('repository.Repository%3Atest-catalog%40ODL.MIT.EDU')


@override_settings(DLKIT_TEST_MONGO_DB_PREFIX='test_dlkit_functional_')
class DjangoTestCase(TestCase, MockTestCase):
    """
    A TestCase class that combines minimocktest and django.test.TestCase

    http://pykler.github.io/MiniMockTest/
    """
    def _pre_setup(self):
        MockTestCase.setUp(self)

    def _post_teardown(self):
        MockTestCase.tearDown(self)

    def code(self, _req, _code):
        self.assertEqual(_req.status_code, _code)

    def create_assessment_for_items(self, bank, item_list):
        form = bank.get_assessment_form_for_create([SIMPLE_SEQUENCE_ASSESSMENT_RECORD])
        form.display_name = 'a test assessment'
        form.description = 'for testing with'
        new_assessment = bank.create_assessment(form)

        for item in item_list:
            bank.add_item(new_assessment.ident, item.ident)

        return new_assessment

    def _get_test_bank(self):
        am = get_manager(self.req, 'assessment')
        querier = am.get_bank_query()
        querier.match_genus_type(TEST_BANK_GENUS, True)
        bank = am.get_banks_by_query(querier).next()
        return am.get_bank(bank.ident)  # to make sure we get a services bank

    def create_new_bank(self, name="my new assessment bank"):
        am = get_manager(self.req, 'assessment')
        form = am.get_bank_form_for_create([])
        form.display_name = name
        form.description = 'for testing with'
        form.set_genus_type(TEST_BANK_GENUS)
        bank = am.create_bank(form)

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=bank.ident)
        add_user_authz_to_settings('student',
                                   self.student_name,
                                   catalog_id=bank.ident)

        return bank

    def _get_test_bin(self):
        # assume the first one -- we're missing permissions to query?
        rm = get_manager(self.req, 'resource')
        return rm.get_bins().next()

    def create_new_bin(self):
        rm = get_manager(self.req, 'resource')
        form = rm.get_bin_form_for_create([])
        form.display_name = 'my new bin'
        form.description = 'for testing with'
        form.set_genus_type(TEST_BIN_GENUS)
        bin = rm.create_bin(form)

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=bin.ident)
        add_user_authz_to_settings('student',
                                   self.student_name,
                                   catalog_id=bin.ident)

        return bin

    def _get_test_gradebook(self):
        # no gradebook query, so assume first gradebook
        gm = get_manager(self.req, 'grading')
        return gm.get_gradebooks().next()

    def create_new_gradebook(self):
        gm = get_manager(self.req, 'grading')
        form = gm.get_gradebook_form_for_create([])
        form.display_name = 'my new grade book'
        form.description = 'for testing with'
        form.set_genus_type(TEST_GRADEBOOK_GENUS)
        gradebook = gm.create_gradebook(form)

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=gradebook.ident)
        add_user_authz_to_settings('student',
                                   self.student_name,
                                   catalog_id=gradebook.ident)

        return gradebook

    def _get_test_log(self):
        # we don't have log query enabled ... so assume first log found
        logm = get_manager(self.req, 'logging')
        return logm.get_logs().next()

    def create_new_log(self):
        logm = get_manager(self.req, 'logging')
        form = logm.get_log_form_for_create([])
        form.display_name = 'my new log'
        form.description = 'for testing with'
        form.set_genus_type(TEST_LOG_GENUS)
        log = logm.create_log(form)

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=log.ident)
        add_user_authz_to_settings('student',
                                   self.student_name,
                                   catalog_id=log.ident)

        return log

    def _get_test_objective_bank(self):
        # get the first one because no objective bank query
        lm = get_manager(self.req, 'learning')
        return lm.get_objective_banks().next()

    def create_new_objective_bank(self):
        lm = get_manager(self.req, 'learning')
        form = lm.get_objective_bank_form_for_create([])
        form.display_name = 'my new objective bank'
        form.description = 'for testing with'
        form.set_genus_type(TEST_OBJECTIVE_BANK_GENUS)
        objective_bank = lm.create_objective_bank(form)

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=objective_bank.ident)
        add_user_authz_to_settings('student',
                                   self.student_name,
                                   catalog_id=objective_bank.ident)

        return objective_bank

    def _get_test_repository(self):
        rm = get_manager(self.req, 'repository')
        querier = rm.get_repository_query()
        querier.match_genus_type(TEST_REPOSITORY_GENUS, True)
        repo = rm.get_repositories_by_query(querier).next()
        return rm.get_repository(repo.ident)  # to make sure we get a services repository

    def create_new_repo(self):
        rm = get_manager(self.req, 'repository')
        form = rm.get_repository_form_for_create([])
        form.display_name = 'my new repository'
        form.description = 'for testing with'
        form.set_genus_type(TEST_REPOSITORY_GENUS)
        repo = rm.create_repository(form)

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=repo.ident)
        add_user_authz_to_settings('student',
                                   self.student_name,
                                   catalog_id=repo.ident)

        return repo

    def create_taken_for_items(self, bank, item_list):
        new_assessment = self.create_assessment_for_items(bank, item_list)

        form = bank.get_assessment_offered_form_for_create(new_assessment.ident, [])
        new_offered = bank.create_assessment_offered(form)

        form = bank.get_assessment_taken_form_for_create(new_offered.ident, [])
        taken = bank.create_assessment_taken(form)
        return taken

    def created(self, _req):
        self.code(_req, 201)

    def deleted(self, _req):
        self.code(_req, 204)

    def filename(self, file_):
        try:
            return file_.name.split('/')[-1].split('.')[0]
        except AttributeError:
            return file_.split('/')[-1].split('.')[0]

    def get_book(self, book_id):
        cm = get_manager(self.req, 'commenting')
        if isinstance(book_id, basestring):
            book_id = Id(book_id)
        book = cm.get_book(book_id)

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=book.ident)
        add_user_authz_to_settings('student',
                                   self.student_name,
                                   catalog_id=book.ident)

        return book

    def get_repo(self, repo_id):
        rm = get_manager(self.req, 'repository')
        if isinstance(repo_id, basestring):
            repo_id = Id(repo_id)
        repo = rm.get_repository(repo_id)

        add_user_authz_to_settings('instructor',
                                   self.username,
                                   catalog_id=repo.ident)
        add_user_authz_to_settings('student',
                                   self.student_name,
                                   catalog_id=repo.ident)

        return repo

    def is_cloudfront_url(self, _url):
        self.assertIn(
            'https://{0}/'.format(settings.CLOUDFRONT_TEST_DISTRO),
            _url
        )

        expected_params = ['?Expires=','&Signature=','&Key-Pair-Id={0}'.format(settings.CLOUDFRONT_TEST_SIGNING_KEYPAIR_ID)]

        for param in expected_params:
            self.assertIn(
                param,
                _url
            )

    def json(self, _req):
        return json.loads(_req.content)

    def message(self, _req, _msg):
        self.assertIn(_msg, str(_req.content))

    def ok(self, _req):
        self.assertEqual(_req.status_code, 200)

    def setUp(self):
        envoy.run('mongo test_dlkit_functional_assessment --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_assessment_authoring --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_authorization --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_commenting --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_hierarchy --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_learning --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_logging --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_grading --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_relationship --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_repository --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_resource --eval "db.dropDatabase()"')

        configure_test_bucket()
        self.url = '/api/v2/repository/'
        self.username = 'instructor@mit.edu'
        self.instructor_req = TestRequest(self.username)

        self.student_name = 'student@mit.edu'
        self.student_req = TestRequest(self.student_name)

        self.unauthenticated_req = TestRequest(self.username, authenticated=False)

        self.req = self.instructor_req

        self.test_file1 = open(ABS_PATH + '/files/Backstage_v2_quick_guide.docx')
        self.test_file2 = open(ABS_PATH + '/files/ps_2015_beam_2gages.pdf')

        # add_user_authz_to_settings('instructor',
        #                            self.username)
        # add_user_authz_to_settings('student',
        #                            self.student_name)
        #
        # import pdb
        # pdb.set_trace()

    def setup_asset(self, repository_id):
        test_file = '/functional/files/Flexure_structure_with_hints.pdf'

        repo = self.get_repo(repository_id)
        asset_form = repo.get_asset_form_for_create([])
        asset_form.display_name = 'test'
        asset_form.description = 'ing'
        new_asset = repo.create_asset(asset_form)

        # now add the new data
        asset_content_type_list = []
        try:
            config = repo._runtime.get_configuration()
            parameter_id = Id('parameter:assetContentRecordTypeForFiles@mongo')
            asset_content_type_list.append(
                config.get_value_by_parameter(parameter_id).get_type_value())
        except AttributeError:
            pass

        asset_content_form = repo.get_asset_content_form_for_create(new_asset.ident,
                                                                    asset_content_type_list)

        self.default_asset_file = ABS_PATH + test_file
        with open(self.default_asset_file, 'r') as file_:
            asset_content_form.set_data(DataInputStream(file_))

        repo.create_asset_content(asset_content_form)

        new_asset = repo.get_asset(new_asset.ident)
        return new_asset.object_map

    def tearDown(self):
        envoy.run('mongo test_dlkit_functional_assessment --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_assessment_authoring --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_authorization --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_commenting --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_hierarchy --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_learning --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_logging --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_grading --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_relationship --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_repository --eval "db.dropDatabase()"')
        envoy.run('mongo test_dlkit_functional_resource --eval "db.dropDatabase()"')

        self.test_file1.close()
        self.test_file2.close()

    def updated(self, _req):
        self.code(_req, 202)


def configure_test_bucket():
    """use test settings, not the production settings"""
    dlkit_runtime.configs.AWS_ADAPTER_1 = {
        'id': 'aws_adapter_configuration_1',
        'displayName': 'AWS Adapter Configuration',
        'description': 'Configuration for AWS Adapter',
        'parameters': {
            'implKey': {
                'syntax': 'STRING',
                'displayName': 'Implementation Key',
                'description': 'Implementation key used by Runtime for class loading',
                'values': [
                    {'value': 'aws_adapter', 'priority': 1}
                ]
            },
            'cloudFrontPublicKey': {
                'syntax': 'STRING',
                'displayName': 'CloudFront Public Key',
                'description': 'Public key for Amazon CloudFront service.',
                'values': [
                    {'value': settings.CLOUDFRONT_TEST_PUBLIC_KEY, 'priority': 1}
                ]
            },
            'cloudFrontPrivateKey': {
                'syntax': 'STRING',
                'displayName': 'CloudFront Private Key',
                'description': 'Private key for Amazon CloudFront service.',
                'values': [
                    {'value': settings.CLOUDFRONT_TEST_PRIVATE_KEY, 'priority': 1}
                ]
            },
            'cloudFrontSigningKeypairId': {
                'syntax': 'STRING',
                'displayName': 'CloudFront Signing Keypair ID',
                'description': 'Signing keypair id for Amazon CloudFront service.',
                'values': [
                    {'value': settings.CLOUDFRONT_TEST_SIGNING_KEYPAIR_ID, 'priority': 1}
                ]
            },
            'cloudFrontSigningPrivateKeyFile': {
                'syntax': 'STRING',
                'displayName': 'CloudFront Signing Private Key File',
                'description': 'Signing Private Key File for Amazon CloudFront service.',
                'values': [
                    {'value': settings.CLOUDFRONT_TEST_SIGNING_PRIVATE_KEY_FILE, 'priority': 1}
                ]
            },
            'cloudFrontDistro': {
                'syntax': 'STRING',
                'displayName': 'CloudFront Distro',
                'description': 'CloudFront Distr-o-bution.',
                'values': [
                    {'value': settings.CLOUDFRONT_TEST_DISTRO, 'priority': 1}
                ]
            },
            'cloudFrontDistroId': {
                'syntax': 'STRING',
                'displayName': 'CloudFront Distro Id',
                'description': 'CloudFront Distr-o-bution Id.',
                'values': [
                    {'value': settings.CLOUDFRONT_TEST_DISTRO_ID, 'priority': 1}
                ]
            },
            'S3PrivateKey': {
                'syntax': 'STRING',
                'displayName': 'S3 Private Key',
                'description': 'Private Key for Amazon S3.',
                'values': [
                    {'value': settings.S3_TEST_PRIVATE_KEY, 'priority': 1}
                ]
            },
            'S3PublicKey': {
                'syntax': 'STRING',
                'displayName': 'S3 Public Key',
                'description': 'Public Key for Amazon S3.',
                'values': [
                    {'value': settings.S3_TEST_PUBLIC_KEY, 'priority': 1}
                ]
            },
            'S3Bucket': {
                'syntax': 'STRING',
                'displayName': 'S3 Bucket',
                'description': 'Bucket for Amazon S3.',
                'values': [
                    {'value': settings.S3_TEST_BUCKET, 'priority': 1}
                ]
            },
            'repositoryProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Repository Provider Implementation',
                'description': 'Implementation for repository service provider',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
        }
    }

    dlkit_runtime.configs.JSON_1 = {
        'id': 'mongo_configuration_1',
        'displayName': 'Mongo Configuration',
        'description': 'Configuration for Mongo Implementation',
        'parameters': {
            'implKey': {
                'syntax': 'STRING',
                'displayName': 'Implementation Key',
                'description': 'Implementation key used by Runtime for class loading',
                'values': [
                    {'value': 'json', 'priority': 1}
                ]
            },
            'authority': {
                'syntax': 'STRING',
                'displayName': 'Mongo Authority',
                'description': 'Authority.',
                'values': [
                    {'value': settings.DLKIT_AUTHORITY, 'priority': 1}
                ]
            },
            'indexes': {
                'syntax': 'OBJECT',
                'displayName': 'Mongo DB Indexes',
                'description': 'Indexes to set in MongoDB',
                'values': [
                    {'value': settings.DLKIT_MONGO_DB_INDEXES, 'priority': 1}
                ]
            },
            'mongoDBNamePrefix': {
                'syntax': 'STRING',
                'displayName': 'Mongo DB Name Prefix',
                'description': 'Prefix for naming mongo databases.',
                'values': [
                    {'value': settings.DLKIT_TEST_MONGO_DB_PREFIX, 'priority': 1}
                ]
            },
            'repositoryProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Repository Provider Implementation',
                'description': 'Implementation for repository service provider',
                'values': [
                    {'value': 'AWS_ADAPTER_1', 'priority': 1}
                ]
            },
            'assetContentRecordTypeForFiles': {
                'syntax': 'TYPE',
                'displayName': 'Asset Content Type for Files',
                'description': 'Asset Content Type for Records that store Files in a repository',
                'values': [
                    {'value': Type(**{
                        'authority': 'odl.mit.edu',
                        'namespace': 'asset_content_record_type',
                        'identifier': 'amazon-web-services'
                    }), 'priority': 1}
                ]
            },
            'recordsRegistry': {
                'syntax': 'STRING',
                'displayName': 'Python path to the extension records registry file',
                'description': 'dot-separated path to the extension records registry file',
                'values': [
                    {'value': 'records.registry', 'priority': 1}
                ]
            },
            'learningProviderImpl': {
                'syntax': 'STRING',
                'displayName': 'Learning Provider Implementation',
                'description': 'Implementation for learning service provider',
                'values': [
                    {'value': 'HANDCAR_MC3_DEV', 'priority': 1}
                ]
            },
            'magicItemLookupSessions': {
                'syntax': 'STRING',
                'displayName': 'Which magic item lookup sessions to try',
                'description': 'To handle magic IDs.',
                'values': [
                    {'value': 'records.fbw_dlkit_adapters.multi_choice_questions.randomized_questions.RandomizedMCItemLookupSession', 'priority': 1}
                ]
            },
            'magicAssessmentPartLookupSessions': {
                'syntax': 'STRING',
                'displayName': 'Which magic assessment part lookup sessions to try',
                'description': 'To handle magic IDs.',
                'values': [
                    {'value': 'records.fbw_dlkit_adapters.magic_parts.assessment_part_records.MagicAssessmentPartLookupSession', 'priority': 1}
                ]
            },
            'localImpl': {
                'syntax': 'STRING',
                'displayName': 'Implementation identifier for local service provider',
                'description': 'Implementation identifier for local service provider.  Typically the same identifier as the Mongo configuration',
                'values': [
                    {'value': 'JSON_1', 'priority': 1}
                ]
            },
        }
    }

    # create a super-user who can create authorizations
    # create_authz_superuser()
    envoy.run('mongorestore --db test_dlkit_functional_assessment --drop dlkit_tests/functional/fixtures/test_dlkit_functional_assessment')
    envoy.run('mongorestore --db test_dlkit_functional_authorization --drop dlkit_tests/functional/fixtures/test_dlkit_functional_authorization')
    envoy.run('mongorestore --db test_dlkit_functional_grading --drop dlkit_tests/functional/fixtures/test_dlkit_functional_grading')
    envoy.run('mongorestore --db test_dlkit_functional_learning --drop dlkit_tests/functional/fixtures/test_dlkit_functional_learning')
    envoy.run('mongorestore --db test_dlkit_functional_logging --drop dlkit_tests/functional/fixtures/test_dlkit_functional_logging')
    envoy.run('mongorestore --db test_dlkit_functional_repository --drop dlkit_tests/functional/fixtures/test_dlkit_functional_repository')
    envoy.run('mongorestore --db test_dlkit_functional_resource --drop dlkit_tests/functional/fixtures/test_dlkit_functional_resource')

def create_test_bank(test_instance):
    """
    helper method to create a test assessment bank
    """
    test_endpoint = '/api/v2/assessment/banks/'
    test_instance.login()
    payload = {
        "name": "a test bank",
        "description": "for testing"
    }
    req = test_instance.client.post(test_endpoint, payload, format='json')
    return json.loads(req.content)

def create_test_request(test_user):
    from django.http import HttpRequest
    from django.conf import settings
    from django.utils.importlib import import_module
    #http://stackoverflow.com/questions/16865947/django-httprequest-object-has-no-attribute-session
    test_request = HttpRequest()
    engine = import_module(settings.SESSION_ENGINE)
    session_key = None
    test_request.user = test_user
    test_request.session = engine.SessionStore(session_key)
    return test_request

def get_agent_id(agent_id):
    """Not a great hack...depends too much on internal DLKit knowledge"""
    # TODO: change this for FBW
    if '@mit.edu' not in agent_id:
        agent_id += '@mit.edu'
    test_request = TestRequest(agent_id)
    condition = PROXY_SESSION.get_proxy_condition()
    condition.set_http_request(test_request)
    proxy = PROXY_SESSION.get_proxy(condition)
    resm = RUNTIME.get_service_manager('RESOURCE', proxy=proxy)
    return resm.effective_agent_id

def get_manager(request, manager_type):
    condition = PROXY_SESSION.get_proxy_condition()
    condition.set_http_request(request)
    proxy = PROXY_SESSION.get_proxy(condition)
    return RUNTIME.get_service_manager(manager_type.upper(), proxy=proxy)

def serialize_date(date):
    return {
        'day': date.day,
        'month': date.month,
        'year': date.year,
        'hour': date.hour,
        'minute': date.minute,
        'second': date.second,
        'microsecond': date.microsecond
    }
