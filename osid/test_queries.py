"""Unit tests of osid queries."""


import unittest


class TestOsidQuery(unittest.TestCase):
    """Tests for OsidQuery"""


    @unittest.skip('unimplemented test')
    def test_get_string_match_types(self):
        """Tests get_string_match_types"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_string_match_type(self):
        """Tests supports_string_match_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_keyword(self):
        """Tests match_keyword"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_keyword_terms(self):
        """Tests clear_keyword_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any(self):
        """Tests match_any"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_any_terms(self):
        """Tests clear_any_terms"""
        pass


class TestOsidIdentifiableQuery(unittest.TestCase):
    """Tests for OsidIdentifiableQuery"""

    @unittest.skip('unimplemented test')
    def test_match_id(self):
        """Tests match_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_id_terms(self):
        """Tests clear_id_terms"""
        pass


class TestOsidExtensibleQuery(unittest.TestCase):
    """Tests for OsidExtensibleQuery"""


    @unittest.skip('unimplemented test')
    def test_match_record_type(self):
        """Tests match_record_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_record(self):
        """Tests match_any_record"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_record_terms(self):
        """Tests clear_record_terms"""
        pass


class TestOsidBrowsableQuery(unittest.TestCase):
    """Tests for OsidBrowsableQuery"""




class TestOsidTemporalQuery(unittest.TestCase):
    """Tests for OsidTemporalQuery"""

    @unittest.skip('unimplemented test')
    def test_match_effective(self):
        """Tests match_effective"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_effective_terms(self):
        """Tests clear_effective_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_start_date(self):
        """Tests match_start_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_start_date(self):
        """Tests match_any_start_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_start_date_terms(self):
        """Tests clear_start_date_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_end_date(self):
        """Tests match_end_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_end_date(self):
        """Tests match_any_end_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_end_date_terms(self):
        """Tests clear_end_date_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_date(self):
        """Tests match_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_date_terms(self):
        """Tests clear_date_terms"""
        pass


class TestOsidSubjugateableQuery(unittest.TestCase):
    """Tests for OsidSubjugateableQuery"""




class TestOsidAggregateableQuery(unittest.TestCase):
    """Tests for OsidAggregateableQuery"""




class TestOsidContainableQuery(unittest.TestCase):
    """Tests for OsidContainableQuery"""

    @unittest.skip('unimplemented test')
    def test_match_sequestered(self):
        """Tests match_sequestered"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_sequestered_terms(self):
        """Tests clear_sequestered_terms"""
        pass


class TestOsidSourceableQuery(unittest.TestCase):
    """Tests for OsidSourceableQuery"""

    @unittest.skip('unimplemented test')
    def test_match_provider_id(self):
        """Tests match_provider_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_provider_id_terms(self):
        """Tests clear_provider_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_provider_query(self):
        """Tests supports_provider_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_provider_query(self):
        """Tests get_provider_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_provider(self):
        """Tests match_any_provider"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_provider_terms(self):
        """Tests clear_provider_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_branding_id(self):
        """Tests match_branding_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_branding_id_terms(self):
        """Tests clear_branding_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_branding_query(self):
        """Tests supports_branding_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_branding_query(self):
        """Tests get_branding_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_branding(self):
        """Tests match_any_branding"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_branding_terms(self):
        """Tests clear_branding_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_license(self):
        """Tests match_license"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_license(self):
        """Tests match_any_license"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_license_terms(self):
        """Tests clear_license_terms"""
        pass


class TestOsidFederateableQuery(unittest.TestCase):
    """Tests for OsidFederateableQuery"""




class TestOsidOperableQuery(unittest.TestCase):
    """Tests for OsidOperableQuery"""

    @unittest.skip('unimplemented test')
    def test_match_active(self):
        """Tests match_active"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_active_terms(self):
        """Tests clear_active_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_enabled(self):
        """Tests match_enabled"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_enabled_terms(self):
        """Tests clear_enabled_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_disabled(self):
        """Tests match_disabled"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_disabled_terms(self):
        """Tests clear_disabled_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_operational(self):
        """Tests match_operational"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_operational_terms(self):
        """Tests clear_operational_terms"""
        pass


class TestOsidObjectQuery(unittest.TestCase):
    """Tests for OsidObjectQuery"""

    @unittest.skip('unimplemented test')
    def test_match_display_name(self):
        """Tests match_display_name"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_display_name(self):
        """Tests match_any_display_name"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_display_name_terms(self):
        """Tests clear_display_name_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_description(self):
        """Tests match_description"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_description(self):
        """Tests match_any_description"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_description_terms(self):
        """Tests clear_description_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_genus_type(self):
        """Tests match_genus_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_genus_type(self):
        """Tests match_any_genus_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_genus_type_terms(self):
        """Tests clear_genus_type_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_parent_genus_type(self):
        """Tests match_parent_genus_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_parent_genus_type_terms(self):
        """Tests clear_parent_genus_type_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_subject_id(self):
        """Tests match_subject_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_subject_id_terms(self):
        """Tests clear_subject_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_subject_query(self):
        """Tests supports_subject_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_subject_query(self):
        """Tests get_subject_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_subject(self):
        """Tests match_any_subject"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_subject_terms(self):
        """Tests clear_subject_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_subject_relevancy_query(self):
        """Tests supports_subject_relevancy_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_subject_relevancy_query(self):
        """Tests get_subject_relevancy_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_subject_relevancy_terms(self):
        """Tests clear_subject_relevancy_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_state_id(self):
        """Tests match_state_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_state_id_terms(self):
        """Tests clear_state_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_state_query(self):
        """Tests supports_state_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_state_query(self):
        """Tests get_state_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_state(self):
        """Tests match_any_state"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_state_terms(self):
        """Tests clear_state_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_comment_id(self):
        """Tests match_comment_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_comment_id_terms(self):
        """Tests clear_comment_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_comment_query(self):
        """Tests supports_comment_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_comment_query(self):
        """Tests get_comment_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_comment(self):
        """Tests match_any_comment"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_comment_terms(self):
        """Tests clear_comment_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_journal_entry_id(self):
        """Tests match_journal_entry_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_journal_entry_id_terms(self):
        """Tests clear_journal_entry_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_journal_entry_query(self):
        """Tests supports_journal_entry_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_journal_entry_query(self):
        """Tests get_journal_entry_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_journal_entry(self):
        """Tests match_any_journal_entry"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_journal_entry_terms(self):
        """Tests clear_journal_entry_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_statistic_query(self):
        """Tests supports_statistic_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_statistic_query(self):
        """Tests get_statistic_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_statistic(self):
        """Tests match_any_statistic"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_statistic_terms(self):
        """Tests clear_statistic_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_credit_id(self):
        """Tests match_credit_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_credit_id_terms(self):
        """Tests clear_credit_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_credit_query(self):
        """Tests supports_credit_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_credit_query(self):
        """Tests get_credit_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_credit(self):
        """Tests match_any_credit"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_credit_terms(self):
        """Tests clear_credit_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_relationship_id(self):
        """Tests match_relationship_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_relationship_id_terms(self):
        """Tests clear_relationship_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_relationship_query(self):
        """Tests supports_relationship_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_relationship_query(self):
        """Tests get_relationship_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_relationship(self):
        """Tests match_any_relationship"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_relationship_terms(self):
        """Tests clear_relationship_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_relationship_peer_id(self):
        """Tests match_relationship_peer_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_relationship_peer_id_terms(self):
        """Tests clear_relationship_peer_id_terms"""
        pass


class TestOsidRelationshipQuery(unittest.TestCase):
    """Tests for OsidRelationshipQuery"""

    @unittest.skip('unimplemented test')
    def test_match_end_reason_id(self):
        """Tests match_end_reason_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_end_reason_id_terms(self):
        """Tests clear_end_reason_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_end_reason_query(self):
        """Tests supports_end_reason_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_end_reason_query(self):
        """Tests get_end_reason_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_end_reason(self):
        """Tests match_any_end_reason"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_end_reason_terms(self):
        """Tests clear_end_reason_terms"""
        pass


class TestOsidCatalogQuery(unittest.TestCase):
    """Tests for OsidCatalogQuery"""




class TestOsidRuleQuery(unittest.TestCase):
    """Tests for OsidRuleQuery"""

    @unittest.skip('unimplemented test')
    def test_match_rule_id(self):
        """Tests match_rule_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_rule_id_terms(self):
        """Tests clear_rule_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_rule_query(self):
        """Tests supports_rule_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_rule_query(self):
        """Tests get_rule_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_rule(self):
        """Tests match_any_rule"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_rule_terms(self):
        """Tests clear_rule_terms"""
        pass


class TestOsidEnablerQuery(unittest.TestCase):
    """Tests for OsidEnablerQuery"""

    @unittest.skip('unimplemented test')
    def test_match_schedule_id(self):
        """Tests match_schedule_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_schedule_id_terms(self):
        """Tests clear_schedule_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_schedule_query(self):
        """Tests supports_schedule_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_schedule_query(self):
        """Tests get_schedule_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_schedule(self):
        """Tests match_any_schedule"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_schedule_terms(self):
        """Tests clear_schedule_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_event_id(self):
        """Tests match_event_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_event_id_terms(self):
        """Tests clear_event_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_event_query(self):
        """Tests supports_event_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_event_query(self):
        """Tests get_event_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_event(self):
        """Tests match_any_event"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_event_terms(self):
        """Tests clear_event_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_cyclic_event_id(self):
        """Tests match_cyclic_event_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_cyclic_event_id_terms(self):
        """Tests clear_cyclic_event_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_cyclic_event_query(self):
        """Tests supports_cyclic_event_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_cyclic_event_query(self):
        """Tests get_cyclic_event_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_cyclic_event(self):
        """Tests match_any_cyclic_event"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_cyclic_event_terms(self):
        """Tests clear_cyclic_event_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_demographic_id(self):
        """Tests match_demographic_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_demographic_id_terms(self):
        """Tests clear_demographic_id_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_demographic_query(self):
        """Tests supports_demographic_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_demographic_query(self):
        """Tests get_demographic_query"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_demographic(self):
        """Tests match_any_demographic"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_demographic_terms(self):
        """Tests clear_demographic_terms"""
        pass


class TestOsidConstrainerQuery(unittest.TestCase):
    """Tests for OsidConstrainerQuery"""




class TestOsidProcessorQuery(unittest.TestCase):
    """Tests for OsidProcessorQuery"""




class TestOsidGovernatorQuery(unittest.TestCase):
    """Tests for OsidGovernatorQuery"""




class TestOsidCompendiumQuery(unittest.TestCase):
    """Tests for OsidCompendiumQuery"""

    @unittest.skip('unimplemented test')
    def test_match_start_date(self):
        """Tests match_start_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_start_date(self):
        """Tests match_any_start_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_start_date_terms(self):
        """Tests clear_start_date_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_end_date(self):
        """Tests match_end_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_any_end_date(self):
        """Tests match_any_end_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_end_date_terms(self):
        """Tests clear_end_date_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_interpolated(self):
        """Tests match_interpolated"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_interpolated_terms(self):
        """Tests clear_interpolated_terms"""
        pass

    @unittest.skip('unimplemented test')
    def test_match_extrapolated(self):
        """Tests match_extrapolated"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_extrapolated_terms(self):
        """Tests clear_extrapolated_terms"""
        pass


class TestOsidCapsuleQuery(unittest.TestCase):
    """Tests for OsidCapsuleQuery"""




