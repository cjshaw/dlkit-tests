"""Unit tests of osid sessions."""


import unittest


class TestOsidSession(unittest.TestCase):
    """Tests for OsidSession"""


    @unittest.skip('unimplemented test')
    def test_get_locale(self):
        """Tests get_locale"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_authenticated(self):
        """Tests is_authenticated"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_authenticated_agent_id(self):
        """Tests get_authenticated_agent_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_authenticated_agent(self):
        """Tests get_authenticated_agent"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_effective_agent_id(self):
        """Tests get_effective_agent_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_effective_agent(self):
        """Tests get_effective_agent"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_date(self):
        """Tests get_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_clock_rate(self):
        """Tests get_clock_rate"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_format_type(self):
        """Tests get_format_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_transactions(self):
        """Tests supports_transactions"""
        pass

    @unittest.skip('unimplemented test')
    def test_start_transaction(self):
        """Tests start_transaction"""
        pass


