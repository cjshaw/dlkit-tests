"""Unit tests of osid markers."""


import unittest


class TestOsidPrimitive(unittest.TestCase):
    """Tests for OsidPrimitive"""




class TestIdentifiable(unittest.TestCase):
    """Tests for Identifiable"""


    @unittest.skip('unimplemented test')
    def test_get_id(self):
        """Tests get_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_current(self):
        """Tests is_current"""
        pass


class TestExtensible(unittest.TestCase):
    """Tests for Extensible"""


    @unittest.skip('unimplemented test')
    def test_get_record_types(self):
        """Tests get_record_types"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_record_type(self):
        """Tests has_record_type"""
        pass


class TestBrowsable(unittest.TestCase):
    """Tests for Browsable"""

    @unittest.skip('unimplemented test')
    def test_get_properties(self):
        """Tests get_properties"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_properties_by_record_type(self):
        """Tests get_properties_by_record_type"""
        pass


class TestSuppliable(unittest.TestCase):
    """Tests for Suppliable"""




class TestTemporal(unittest.TestCase):
    """Tests for Temporal"""


    @unittest.skip('unimplemented test')
    def test_is_effective(self):
        """Tests is_effective"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_start_date(self):
        """Tests get_start_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_end_date(self):
        """Tests get_end_date"""
        pass


class TestSubjugateable(unittest.TestCase):
    """Tests for Subjugateable"""




class TestAggregateable(unittest.TestCase):
    """Tests for Aggregateable"""




class TestContainable(unittest.TestCase):
    """Tests for Containable"""


    @unittest.skip('unimplemented test')
    def test_is_sequestered(self):
        """Tests is_sequestered"""
        pass


class TestSourceable(unittest.TestCase):
    """Tests for Sourceable"""

    @unittest.skip('unimplemented test')
    def test_get_provider_id(self):
        """Tests get_provider_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_provider(self):
        """Tests get_provider"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_branding_ids(self):
        """Tests get_branding_ids"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_branding(self):
        """Tests get_branding"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_license(self):
        """Tests get_license"""
        pass


class TestFederateable(unittest.TestCase):
    """Tests for Federateable"""




class TestOperable(unittest.TestCase):
    """Tests for Operable"""

    @unittest.skip('unimplemented test')
    def test_is_active(self):
        """Tests is_active"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_enabled(self):
        """Tests is_enabled"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_disabled(self):
        """Tests is_disabled"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_operational(self):
        """Tests is_operational"""
        pass


