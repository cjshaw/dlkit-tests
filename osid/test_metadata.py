"""Unit tests of osid metadata."""


import unittest


class TestMetadata(unittest.TestCase):
    """Tests for Metadata"""


    @unittest.skip('unimplemented test')
    def test_get_element_id(self):
        """Tests get_element_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_element_label(self):
        """Tests get_element_label"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_instructions(self):
        """Tests get_instructions"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_syntax(self):
        """Tests get_syntax"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_array(self):
        """Tests is_array"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_required(self):
        """Tests is_required"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_read_only(self):
        """Tests is_read_only"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_linked(self):
        """Tests is_linked"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_value_known(self):
        """Tests is_value_known"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_value(self):
        """Tests has_value"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_units(self):
        """Tests get_units"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_elements(self):
        """Tests get_minimum_elements"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_elements(self):
        """Tests get_maximum_elements"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_cardinal(self):
        """Tests get_minimum_cardinal"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_cardinal(self):
        """Tests get_maximum_cardinal"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_cardinal_set(self):
        """Tests get_cardinal_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_cardinal_values(self):
        """Tests get_default_cardinal_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_cardinal_values(self):
        """Tests get_existing_cardinal_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_coordinate_types(self):
        """Tests get_coordinate_types"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_coordinate_type(self):
        """Tests supports_coordinate_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_axes_for_coordinate_type(self):
        """Tests get_axes_for_coordinate_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_coordinate_values(self):
        """Tests get_minimum_coordinate_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_coordinate_values(self):
        """Tests get_maximum_coordinate_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_coordinate_set(self):
        """Tests get_coordinate_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_coordinate_values(self):
        """Tests get_default_coordinate_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_coordinate_values(self):
        """Tests get_existing_coordinate_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_currency_types(self):
        """Tests get_currency_types"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_currency_type(self):
        """Tests supports_currency_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_currency(self):
        """Tests get_minimum_currency"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_currency(self):
        """Tests get_maximum_currency"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_currency_set(self):
        """Tests get_currency_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_currency_values(self):
        """Tests get_default_currency_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_currency_values(self):
        """Tests get_existing_currency_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_date_time_resolution(self):
        """Tests get_date_time_resolution"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_calendar_types(self):
        """Tests get_calendar_types"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_calendar_type(self):
        """Tests supports_calendar_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_time_types(self):
        """Tests get_time_types"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_time_type(self):
        """Tests supports_time_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_date_time(self):
        """Tests get_minimum_date_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_date_time(self):
        """Tests get_maximum_date_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_date_time_set(self):
        """Tests get_date_time_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_date_time_values(self):
        """Tests get_default_date_time_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_date_time_values(self):
        """Tests get_existing_date_time_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_decimal_scale(self):
        """Tests get_decimal_scale"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_decimal(self):
        """Tests get_minimum_decimal"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_decimal(self):
        """Tests get_maximum_decimal"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_decimal_set(self):
        """Tests get_decimal_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_decimal_values(self):
        """Tests get_default_decimal_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_decimal_values(self):
        """Tests get_existing_decimal_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_distance_resolution(self):
        """Tests get_distance_resolution"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_distance(self):
        """Tests get_minimum_distance"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_distance(self):
        """Tests get_maximum_distance"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_distance_set(self):
        """Tests get_distance_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_distance_values(self):
        """Tests get_default_distance_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_distance_values(self):
        """Tests get_existing_distance_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_duration(self):
        """Tests get_minimum_duration"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_duration(self):
        """Tests get_maximum_duration"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_duration_set(self):
        """Tests get_duration_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_duration_values(self):
        """Tests get_default_duration_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_duration_values(self):
        """Tests get_existing_duration_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_heading_types(self):
        """Tests get_heading_types"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_heading_type(self):
        """Tests supports_heading_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_axes_for_heading_type(self):
        """Tests get_axes_for_heading_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_heading_values(self):
        """Tests get_minimum_heading_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_heading_values(self):
        """Tests get_maximum_heading_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_heading_set(self):
        """Tests get_heading_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_heading_values(self):
        """Tests get_default_heading_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_heading_values(self):
        """Tests get_existing_heading_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_id_set(self):
        """Tests get_id_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_id_values(self):
        """Tests get_default_id_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_id_values(self):
        """Tests get_existing_id_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_integer(self):
        """Tests get_minimum_integer"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_integer(self):
        """Tests get_maximum_integer"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_integer_set(self):
        """Tests get_integer_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_integer_values(self):
        """Tests get_default_integer_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_integer_values(self):
        """Tests get_existing_integer_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_object_types(self):
        """Tests get_object_types"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_object_type(self):
        """Tests supports_object_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_object_set(self):
        """Tests get_object_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_object_values(self):
        """Tests get_default_object_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_object_values(self):
        """Tests get_existing_object_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_spatial_unit_record_types(self):
        """Tests get_spatial_unit_record_types"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_spatial_unit_record_type(self):
        """Tests supports_spatial_unit_record_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_spatial_unit_set(self):
        """Tests get_spatial_unit_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_spatial_unit_values(self):
        """Tests get_default_spatial_unit_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_spatial_unit_values(self):
        """Tests get_existing_spatial_unit_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_speed(self):
        """Tests get_minimum_speed"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_speed(self):
        """Tests get_maximum_speed"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_speed_set(self):
        """Tests get_speed_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_speed_values(self):
        """Tests get_default_speed_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_speed_values(self):
        """Tests get_existing_speed_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_string_length(self):
        """Tests get_minimum_string_length"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_string_length(self):
        """Tests get_maximum_string_length"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_string_match_types(self):
        """Tests get_string_match_types"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_string_match_type(self):
        """Tests supports_string_match_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_string_expression(self):
        """Tests get_string_expression"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_string_format_types(self):
        """Tests get_string_format_types"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_string_set(self):
        """Tests get_string_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_string_values(self):
        """Tests get_default_string_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_string_values(self):
        """Tests get_existing_string_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_time(self):
        """Tests get_minimum_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_time(self):
        """Tests get_maximum_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_time_set(self):
        """Tests get_time_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_time_values(self):
        """Tests get_default_time_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_time_values(self):
        """Tests get_existing_time_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_type_set(self):
        """Tests get_type_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_type_values(self):
        """Tests get_default_type_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_type_values(self):
        """Tests get_existing_type_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_version_types(self):
        """Tests get_version_types"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_version_type(self):
        """Tests supports_version_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_version(self):
        """Tests get_minimum_version"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_version(self):
        """Tests get_maximum_version"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_version_set(self):
        """Tests get_version_set"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_version_values(self):
        """Tests get_default_version_values"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_existing_version_values(self):
        """Tests get_existing_version_values"""
        pass


