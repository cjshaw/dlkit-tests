"""Unit tests of osid managers."""


import unittest


class TestOsidProfile(unittest.TestCase):
    """Tests for OsidProfile"""


    @unittest.skip('unimplemented test')
    def test_get_id(self):
        """Tests get_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_display_name(self):
        """Tests get_display_name"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_description(self):
        """Tests get_description"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_version(self):
        """Tests get_version"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_release_date(self):
        """Tests get_release_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_osid_version(self):
        """Tests supports_osid_version"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_locales(self):
        """Tests get_locales"""
        pass

    def test_supports_journal_rollback(self):
        """Tests supports_journal_rollback"""
       

    @unittest.skip('unimplemented test')
    def test_supports_journal_branching(self):
        """Tests supports_journal_branching"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_branch_id(self):
        """Tests get_branch_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_branch(self):
        """Tests get_branch"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_proxy_record_types(self):
        """Tests get_proxy_record_types"""
        pass

    @unittest.skip('unimplemented test')
    def test_supports_proxy_record_type(self):
        """Tests supports_proxy_record_type"""
        pass


class TestOsidManager(unittest.TestCase):
    """Tests for OsidManager"""


    @unittest.skip('unimplemented test')
    def test_initialize(self):
        """Tests initialize"""
        pass

    @unittest.skip('unimplemented test')
    def test_rollback_service(self):
        """Tests rollback_service"""
        pass

    @unittest.skip('unimplemented test')
    def test_change_branch(self):
        """Tests change_branch"""
        pass


class TestOsidProxyManager(unittest.TestCase):
    """Tests for OsidProxyManager"""


    @unittest.skip('unimplemented test')
    def test_initialize(self):
        """Tests initialize"""
        pass

    @unittest.skip('unimplemented test')
    def test_rollback_service(self):
        """Tests rollback_service"""
        pass

    @unittest.skip('unimplemented test')
    def test_change_branch(self):
        """Tests change_branch"""
        pass


class TestOsidRuntimeProfile(unittest.TestCase):
    """Tests for OsidRuntimeProfile"""

    @unittest.skip('unimplemented test')
    def test_supports_configuration(self):
        """Tests supports_configuration"""
        pass


class TestOsidRuntimeManager(unittest.TestCase):
    """Tests for OsidRuntimeManager"""


    @unittest.skip('unimplemented test')
    def test_get_manager(self):
        """Tests get_manager"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_proxy_manager(self):
        """Tests get_proxy_manager"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_configuration(self):
        """Tests get_configuration"""
        pass


