"""Unit tests of osid objects."""


import unittest


class TestOsidObject(unittest.TestCase):
    """Tests for OsidObject"""


    @unittest.skip('unimplemented test')
    def test_get_display_name(self):
        """Tests get_display_name"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_description(self):
        """Tests get_description"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_genus_type(self):
        """Tests get_genus_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_of_genus_type(self):
        """Tests is_of_genus_type"""
        pass


class TestOsidRelationship(unittest.TestCase):
    """Tests for OsidRelationship"""

    @unittest.skip('unimplemented test')
    def test_has_end_reason(self):
        """Tests has_end_reason"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_end_reason_id(self):
        """Tests get_end_reason_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_end_reason(self):
        """Tests get_end_reason"""
        pass


class TestOsidCatalog(unittest.TestCase):
    """Tests for OsidCatalog"""




class TestOsidRule(unittest.TestCase):
    """Tests for OsidRule"""

    @unittest.skip('unimplemented test')
    def test_has_rule(self):
        """Tests has_rule"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_rule_id(self):
        """Tests get_rule_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_rule(self):
        """Tests get_rule"""
        pass


class TestOsidEnabler(unittest.TestCase):
    """Tests for OsidEnabler"""

    @unittest.skip('unimplemented test')
    def test_is_effective_by_schedule(self):
        """Tests is_effective_by_schedule"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_schedule_id(self):
        """Tests get_schedule_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_schedule(self):
        """Tests get_schedule"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_effective_by_event(self):
        """Tests is_effective_by_event"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_event_id(self):
        """Tests get_event_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_event(self):
        """Tests get_event"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_effective_by_cyclic_event(self):
        """Tests is_effective_by_cyclic_event"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_cyclic_event_id(self):
        """Tests get_cyclic_event_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_cyclic_event(self):
        """Tests get_cyclic_event"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_effective_for_demographic(self):
        """Tests is_effective_for_demographic"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_demographic_id(self):
        """Tests get_demographic_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_demographic(self):
        """Tests get_demographic"""
        pass


class TestOsidConstrainer(unittest.TestCase):
    """Tests for OsidConstrainer"""




class TestOsidProcessor(unittest.TestCase):
    """Tests for OsidProcessor"""




class TestOsidGovernator(unittest.TestCase):
    """Tests for OsidGovernator"""




class TestOsidCompendium(unittest.TestCase):
    """Tests for OsidCompendium"""

    @unittest.skip('unimplemented test')
    def test_get_start_date(self):
        """Tests get_start_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_end_date(self):
        """Tests get_end_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_interpolated(self):
        """Tests is_interpolated"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_extrapolated(self):
        """Tests is_extrapolated"""
        pass


class TestOsidCapsule(unittest.TestCase):
    """Tests for OsidCapsule"""




class TestOsidForm(unittest.TestCase):
    """Tests for OsidForm"""


    @unittest.skip('unimplemented test')
    def test_is_for_update(self):
        """Tests is_for_update"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_default_locale(self):
        """Tests get_default_locale"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_locales(self):
        """Tests get_locales"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_locale(self):
        """Tests set_locale"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_journal_comment_metadata(self):
        """Tests get_journal_comment_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_journal_comment(self):
        """Tests set_journal_comment"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_valid(self):
        """Tests is_valid"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_validation_messages(self):
        """Tests get_validation_messages"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_invalid_metadata(self):
        """Tests get_invalid_metadata"""
        pass


class TestOsidIdentifiableForm(unittest.TestCase):
    """Tests for OsidIdentifiableForm"""




class TestOsidExtensibleForm(unittest.TestCase):
    """Tests for OsidExtensibleForm"""


    @unittest.skip('unimplemented test')
    def test_get_required_record_types(self):
        """Tests get_required_record_types"""
        pass


class TestOsidBrowsableForm(unittest.TestCase):
    """Tests for OsidBrowsableForm"""




class TestOsidTemporalForm(unittest.TestCase):
    """Tests for OsidTemporalForm"""


    @unittest.skip('unimplemented test')
    def test_get_start_date_metadata(self):
        """Tests get_start_date_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_start_date(self):
        """Tests set_start_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_start_date(self):
        """Tests clear_start_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_end_date_metadata(self):
        """Tests get_end_date_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_end_date(self):
        """Tests set_end_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_end_date(self):
        """Tests clear_end_date"""
        pass


class TestOsidSubjugateableForm(unittest.TestCase):
    """Tests for OsidSubjugateableForm"""




class TestOsidAggregateableForm(unittest.TestCase):
    """Tests for OsidAggregateableForm"""




class TestOsidContainableForm(unittest.TestCase):
    """Tests for OsidContainableForm"""

    @unittest.skip('unimplemented test')
    def test_get_sequestered_metadata(self):
        """Tests get_sequestered_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_sequestered(self):
        """Tests set_sequestered"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_sequestered(self):
        """Tests clear_sequestered"""
        pass


class TestOsidSourceableForm(unittest.TestCase):
    """Tests for OsidSourceableForm"""

    @unittest.skip('unimplemented test')
    def test_get_provider_metadata(self):
        """Tests get_provider_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_provider(self):
        """Tests set_provider"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_provider(self):
        """Tests clear_provider"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_branding_metadata(self):
        """Tests get_branding_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_branding(self):
        """Tests set_branding"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_branding(self):
        """Tests clear_branding"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_license_metadata(self):
        """Tests get_license_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_license(self):
        """Tests set_license"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_license(self):
        """Tests clear_license"""
        pass


class TestOsidFederateableForm(unittest.TestCase):
    """Tests for OsidFederateableForm"""




class TestOsidOperableForm(unittest.TestCase):
    """Tests for OsidOperableForm"""

    @unittest.skip('unimplemented test')
    def test_get_enabled_metadata(self):
        """Tests get_enabled_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_enabled(self):
        """Tests set_enabled"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_enabled(self):
        """Tests clear_enabled"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_disabled_metadata(self):
        """Tests get_disabled_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_disabled(self):
        """Tests set_disabled"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_disabled(self):
        """Tests clear_disabled"""
        pass


class TestOsidObjectForm(unittest.TestCase):
    """Tests for OsidObjectForm"""


    @unittest.skip('unimplemented test')
    def test_get_display_name_metadata(self):
        """Tests get_display_name_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_display_name(self):
        """Tests set_display_name"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_display_name(self):
        """Tests clear_display_name"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_description_metadata(self):
        """Tests get_description_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_description(self):
        """Tests set_description"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_description(self):
        """Tests clear_description"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_genus_type_metadata(self):
        """Tests get_genus_type_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_genus_type(self):
        """Tests set_genus_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_genus_type(self):
        """Tests clear_genus_type"""
        pass


class TestOsidRelationshipForm(unittest.TestCase):
    """Tests for OsidRelationshipForm"""





class TestOsidCatalogForm(unittest.TestCase):
    """Tests for OsidCatalogForm"""




class TestOsidRuleForm(unittest.TestCase):
    """Tests for OsidRuleForm"""

    @unittest.skip('unimplemented test')
    def test_get_rule_metadata(self):
        """Tests get_rule_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_rule(self):
        """Tests set_rule"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_rule(self):
        """Tests clear_rule"""
        pass


class TestOsidEnablerForm(unittest.TestCase):
    """Tests for OsidEnablerForm"""

    @unittest.skip('unimplemented test')
    def test_get_schedule_metadata(self):
        """Tests get_schedule_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_schedule(self):
        """Tests set_schedule"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_schedule(self):
        """Tests clear_schedule"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_event_metadata(self):
        """Tests get_event_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_event(self):
        """Tests set_event"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_event(self):
        """Tests clear_event"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_cyclic_event_metadata(self):
        """Tests get_cyclic_event_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_cyclic_event(self):
        """Tests set_cyclic_event"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_cyclic_event(self):
        """Tests clear_cyclic_event"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_demographic_metadata(self):
        """Tests get_demographic_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_demographic(self):
        """Tests set_demographic"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_demographic(self):
        """Tests clear_demographic"""
        pass


class TestOsidConstrainerForm(unittest.TestCase):
    """Tests for OsidConstrainerForm"""




class TestOsidProcessorForm(unittest.TestCase):
    """Tests for OsidProcessorForm"""




class TestOsidGovernatorForm(unittest.TestCase):
    """Tests for OsidGovernatorForm"""




class TestOsidCompendiumForm(unittest.TestCase):
    """Tests for OsidCompendiumForm"""

    @unittest.skip('unimplemented test')
    def test_get_start_date_metadata(self):
        """Tests get_start_date_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_start_date(self):
        """Tests set_start_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_start_date(self):
        """Tests clear_start_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_end_date_metadata(self):
        """Tests get_end_date_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_end_date(self):
        """Tests set_end_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_end_date(self):
        """Tests clear_end_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_interpolated_metadata(self):
        """Tests get_interpolated_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_interpolated(self):
        """Tests set_interpolated"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_interpolated(self):
        """Tests clear_interpolated"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_extrapolated_metadata(self):
        """Tests get_extrapolated_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_extrapolated(self):
        """Tests set_extrapolated"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_extrapolated(self):
        """Tests clear_extrapolated"""
        pass


class TestOsidCapsuleForm(unittest.TestCase):
    """Tests for OsidCapsuleForm"""




class TestOsidList(unittest.TestCase):
    """Tests for OsidList"""


    @unittest.skip('unimplemented test')
    def test_has_next(self):
        """Tests has_next"""
        pass

    @unittest.skip('unimplemented test')
    def test_available(self):
        """Tests available"""
        pass

    @unittest.skip('unimplemented test')
    def test_skip(self):
        """Tests skip"""
        pass


class TestOsidNode(unittest.TestCase):
    """Tests for OsidNode"""


    @unittest.skip('unimplemented test')
    def test_is_root(self):
        """Tests is_root"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_parents(self):
        """Tests has_parents"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_parent_ids(self):
        """Tests get_parent_ids"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_leaf(self):
        """Tests is_leaf"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_children(self):
        """Tests has_children"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_child_ids(self):
        """Tests get_child_ids"""
        pass


