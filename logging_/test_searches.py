"""Unit tests of logging searches."""

import unittest


class TestLogEntrySearch(unittest.TestCase):
    """Tests for LogEntrySearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_log_entries(self):
        """Tests search_among_log_entries"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_log_entry_results(self):
        """Tests order_log_entry_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_log_entry_search_record(self):
        """Tests get_log_entry_search_record"""
        pass


class TestLogEntrySearchResults(unittest.TestCase):
    """Tests for LogEntrySearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_log_entries(self):
        """Tests get_log_entries"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_log_entry_query_inspector(self):
        """Tests get_log_entry_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_log_entry_search_results_record(self):
        """Tests get_log_entry_search_results_record"""
        pass


class TestLogSearch(unittest.TestCase):
    """Tests for LogSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_logs(self):
        """Tests search_among_logs"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_log_results(self):
        """Tests order_log_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_log_search_record(self):
        """Tests get_log_search_record"""
        pass


class TestLogSearchResults(unittest.TestCase):
    """Tests for LogSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_logs(self):
        """Tests get_logs"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_log_query_inspector(self):
        """Tests get_log_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_log_search_results_record(self):
        """Tests get_log_search_results_record"""
        pass


