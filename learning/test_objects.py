"""Unit tests of learning objects."""

import unittest


class TestObjective(unittest.TestCase):
    """Tests for Objective"""



    @unittest.skip('unimplemented test')
    def test_has_assessment(self):
        """Tests has_assessment"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_id(self):
        """Tests get_assessment_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment(self):
        """Tests get_assessment"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_knowledge_category(self):
        """Tests has_knowledge_category"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_knowledge_category_id(self):
        """Tests get_knowledge_category_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_knowledge_category(self):
        """Tests get_knowledge_category"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_cognitive_process(self):
        """Tests has_cognitive_process"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_cognitive_process_id(self):
        """Tests get_cognitive_process_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_cognitive_process(self):
        """Tests get_cognitive_process"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_objective_record(self):
        """Tests get_objective_record"""
        pass


class TestObjectiveForm(unittest.TestCase):
    """Tests for ObjectiveForm"""



    @unittest.skip('unimplemented test')
    def test_get_assessment_metadata(self):
        """Tests get_assessment_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_assessment(self):
        """Tests set_assessment"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_assessment(self):
        """Tests clear_assessment"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_knowledge_category_metadata(self):
        """Tests get_knowledge_category_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_knowledge_category(self):
        """Tests set_knowledge_category"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_knowledge_category(self):
        """Tests clear_knowledge_category"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_cognitive_process_metadata(self):
        """Tests get_cognitive_process_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_cognitive_process(self):
        """Tests set_cognitive_process"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_cognitive_process(self):
        """Tests clear_cognitive_process"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_objective_form_record(self):
        """Tests get_objective_form_record"""
        pass


class TestObjectiveList(unittest.TestCase):
    """Tests for ObjectiveList"""

    @unittest.skip('unimplemented test')
    def test_get_next_objective(self):
        """Tests get_next_objective"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_objectives(self):
        """Tests get_next_objectives"""
        pass


class TestObjectiveNode(unittest.TestCase):
    """Tests for ObjectiveNode"""

    @unittest.skip('unimplemented test')
    def test_get_objective(self):
        """Tests get_objective"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_parent_objective_nodes(self):
        """Tests get_parent_objective_nodes"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_child_objective_nodes(self):
        """Tests get_child_objective_nodes"""
        pass


class TestObjectiveNodeList(unittest.TestCase):
    """Tests for ObjectiveNodeList"""

    @unittest.skip('unimplemented test')
    def test_get_next_objective_node(self):
        """Tests get_next_objective_node"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_objective_nodes(self):
        """Tests get_next_objective_nodes"""
        pass


class TestActivity(unittest.TestCase):
    """Tests for Activity"""



    @unittest.skip('unimplemented test')
    def test_get_objective_id(self):
        """Tests get_objective_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_objective(self):
        """Tests get_objective"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_asset_based_activity(self):
        """Tests is_asset_based_activity"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_asset_ids(self):
        """Tests get_asset_ids"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assets(self):
        """Tests get_assets"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_course_based_activity(self):
        """Tests is_course_based_activity"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_course_ids(self):
        """Tests get_course_ids"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_courses(self):
        """Tests get_courses"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_assessment_based_activity(self):
        """Tests is_assessment_based_activity"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_ids(self):
        """Tests get_assessment_ids"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessments(self):
        """Tests get_assessments"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_activity_record(self):
        """Tests get_activity_record"""
        pass


class TestActivityForm(unittest.TestCase):
    """Tests for ActivityForm"""



    @unittest.skip('unimplemented test')
    def test_get_assets_metadata(self):
        """Tests get_assets_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_assets(self):
        """Tests set_assets"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_assets(self):
        """Tests clear_assets"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_courses_metadata(self):
        """Tests get_courses_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_courses(self):
        """Tests set_courses"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_courses(self):
        """Tests clear_courses"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessments_metadata(self):
        """Tests get_assessments_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_assessments(self):
        """Tests set_assessments"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_assessments(self):
        """Tests clear_assessments"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_activity_form_record(self):
        """Tests get_activity_form_record"""
        pass


class TestActivityList(unittest.TestCase):
    """Tests for ActivityList"""

    @unittest.skip('unimplemented test')
    def test_get_next_activity(self):
        """Tests get_next_activity"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_activities(self):
        """Tests get_next_activities"""
        pass


class TestProficiency(unittest.TestCase):
    """Tests for Proficiency"""



    @unittest.skip('unimplemented test')
    def test_get_resource_id(self):
        """Tests get_resource_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_resource(self):
        """Tests get_resource"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_objective_id(self):
        """Tests get_objective_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_objective(self):
        """Tests get_objective"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_completion(self):
        """Tests get_completion"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_level(self):
        """Tests has_level"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_level_id(self):
        """Tests get_level_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_level(self):
        """Tests get_level"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_proficiency_record(self):
        """Tests get_proficiency_record"""
        pass


class TestProficiencyForm(unittest.TestCase):
    """Tests for ProficiencyForm"""



    @unittest.skip('unimplemented test')
    def test_get_completion_metadata(self):
        """Tests get_completion_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_completion(self):
        """Tests set_completion"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_completion(self):
        """Tests clear_completion"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_level_metadata(self):
        """Tests get_level_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_level(self):
        """Tests set_level"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_level(self):
        """Tests clear_level"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_proficiency_form_record(self):
        """Tests get_proficiency_form_record"""
        pass


class TestProficiencyList(unittest.TestCase):
    """Tests for ProficiencyList"""

    @unittest.skip('unimplemented test')
    def test_get_next_proficiency(self):
        """Tests get_next_proficiency"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_proficiencies(self):
        """Tests get_next_proficiencies"""
        pass


class TestObjectiveBank(unittest.TestCase):
    """Tests for ObjectiveBank"""



    @unittest.skip('unimplemented test')
    def test_get_objective_bank_record(self):
        """Tests get_objective_bank_record"""
        pass


class TestObjectiveBankForm(unittest.TestCase):
    """Tests for ObjectiveBankForm"""



    @unittest.skip('unimplemented test')
    def test_get_objective_bank_form_record(self):
        """Tests get_objective_bank_form_record"""
        pass


class TestObjectiveBankList(unittest.TestCase):
    """Tests for ObjectiveBankList"""

    @unittest.skip('unimplemented test')
    def test_get_next_objective_bank(self):
        """Tests get_next_objective_bank"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_objective_banks(self):
        """Tests get_next_objective_banks"""
        pass


class TestObjectiveBankNode(unittest.TestCase):
    """Tests for ObjectiveBankNode"""

    @unittest.skip('unimplemented test')
    def test_get_objective_bank(self):
        """Tests get_objective_bank"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_parent_objective_bank_nodes(self):
        """Tests get_parent_objective_bank_nodes"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_child_objective_bank_nodes(self):
        """Tests get_child_objective_bank_nodes"""
        pass


class TestObjectiveBankNodeList(unittest.TestCase):
    """Tests for ObjectiveBankNodeList"""

    @unittest.skip('unimplemented test')
    def test_get_next_objective_bank_node(self):
        """Tests get_next_objective_bank_node"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_objective_bank_nodes(self):
        """Tests get_next_objective_bank_nodes"""
        pass


