"""Unit tests of learning searches."""

import unittest


class TestObjectiveSearch(unittest.TestCase):
    """Tests for ObjectiveSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_objectives(self):
        """Tests search_among_objectives"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_objective_results(self):
        """Tests order_objective_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_objective_search_record(self):
        """Tests get_objective_search_record"""
        pass


class TestObjectiveSearchResults(unittest.TestCase):
    """Tests for ObjectiveSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_objectives(self):
        """Tests get_objectives"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_objective_query_inspector(self):
        """Tests get_objective_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_objective_search_results_record(self):
        """Tests get_objective_search_results_record"""
        pass


class TestActivitySearch(unittest.TestCase):
    """Tests for ActivitySearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_activities(self):
        """Tests search_among_activities"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_activity_results(self):
        """Tests order_activity_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_activity_search_record(self):
        """Tests get_activity_search_record"""
        pass


class TestActivitySearchResults(unittest.TestCase):
    """Tests for ActivitySearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_activities(self):
        """Tests get_activities"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_activity_query_inspector(self):
        """Tests get_activity_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_activity_search_results_record(self):
        """Tests get_activity_search_results_record"""
        pass


class TestProficiencySearch(unittest.TestCase):
    """Tests for ProficiencySearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_proficiencies(self):
        """Tests search_among_proficiencies"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_proficiency_results(self):
        """Tests order_proficiency_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_proficiency_search_record(self):
        """Tests get_proficiency_search_record"""
        pass


class TestProficiencySearchResults(unittest.TestCase):
    """Tests for ProficiencySearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_proficiencies(self):
        """Tests get_proficiencies"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_proficiency_query_inspector(self):
        """Tests get_proficiency_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_proficiency_search_results_record(self):
        """Tests get_proficiency_search_results_record"""
        pass


class TestObjectiveBankSearch(unittest.TestCase):
    """Tests for ObjectiveBankSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_objective_banks(self):
        """Tests search_among_objective_banks"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_objective_bank_results(self):
        """Tests order_objective_bank_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_objective_bank_search_record(self):
        """Tests get_objective_bank_search_record"""
        pass


class TestObjectiveBankSearchResults(unittest.TestCase):
    """Tests for ObjectiveBankSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_objective_banks(self):
        """Tests get_objective_banks"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_objective_bank_query_inspector(self):
        """Tests get_objective_bank_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_objective_bank_search_results_record(self):
        """Tests get_objective_bank_search_results_record"""
        pass


