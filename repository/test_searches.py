"""Unit tests of repository searches."""

import unittest


class TestAssetSearch(unittest.TestCase):
    """Tests for AssetSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_assets(self):
        """Tests search_among_assets"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_asset_results(self):
        """Tests order_asset_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_asset_search_record(self):
        """Tests get_asset_search_record"""
        pass


class TestAssetSearchResults(unittest.TestCase):
    """Tests for AssetSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_assets(self):
        """Tests get_assets"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_asset_query_inspector(self):
        """Tests get_asset_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_asset_search_results_record(self):
        """Tests get_asset_search_results_record"""
        pass


class TestCompositionSearch(unittest.TestCase):
    """Tests for CompositionSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_compositions(self):
        """Tests search_among_compositions"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_composition_results(self):
        """Tests order_composition_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_composition_search_record(self):
        """Tests get_composition_search_record"""
        pass


class TestCompositionSearchResults(unittest.TestCase):
    """Tests for CompositionSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_compositions(self):
        """Tests get_compositions"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_composition_query_inspector(self):
        """Tests get_composition_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_composition_search_results_record(self):
        """Tests get_composition_search_results_record"""
        pass


class TestRepositorySearch(unittest.TestCase):
    """Tests for RepositorySearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_repositories(self):
        """Tests search_among_repositories"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_repository_results(self):
        """Tests order_repository_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_repository_search_record(self):
        """Tests get_repository_search_record"""
        pass


class TestRepositorySearchResults(unittest.TestCase):
    """Tests for RepositorySearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_repositories(self):
        """Tests get_repositories"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_repository_query_inspector(self):
        """Tests get_repository_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_repository_search_results_record(self):
        """Tests get_repository_search_results_record"""
        pass


