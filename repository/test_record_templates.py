"""Unit tests of repository records."""

import unittest


class TestAssetRecord(unittest.TestCase):
    """Tests for AssetRecord"""




class TestAssetQueryRecord(unittest.TestCase):
    """Tests for AssetQueryRecord"""




class TestAssetFormRecord(unittest.TestCase):
    """Tests for AssetFormRecord"""




class TestAssetSearchRecord(unittest.TestCase):
    """Tests for AssetSearchRecord"""




class TestAssetContentRecord(unittest.TestCase):
    """Tests for AssetContentRecord"""




class TestAssetContentQueryRecord(unittest.TestCase):
    """Tests for AssetContentQueryRecord"""




class TestAssetContentFormRecord(unittest.TestCase):
    """Tests for AssetContentFormRecord"""




class TestCompositionRecord(unittest.TestCase):
    """Tests for CompositionRecord"""




class TestCompositionQueryRecord(unittest.TestCase):
    """Tests for CompositionQueryRecord"""




class TestCompositionFormRecord(unittest.TestCase):
    """Tests for CompositionFormRecord"""




class TestCompositionSearchRecord(unittest.TestCase):
    """Tests for CompositionSearchRecord"""




class TestRepositoryRecord(unittest.TestCase):
    """Tests for RepositoryRecord"""




class TestRepositoryQueryRecord(unittest.TestCase):
    """Tests for RepositoryQueryRecord"""




class TestRepositoryFormRecord(unittest.TestCase):
    """Tests for RepositoryFormRecord"""




class TestRepositorySearchRecord(unittest.TestCase):
    """Tests for RepositorySearchRecord"""




