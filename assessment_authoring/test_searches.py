"""Unit tests of assessment.authoring searches."""

import unittest


class TestAssessmentPartSearch(unittest.TestCase):
    """Tests for AssessmentPartSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_assessment_parts(self):
        """Tests search_among_assessment_parts"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_assessment_part_results(self):
        """Tests order_assessment_part_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_part_search_record(self):
        """Tests get_assessment_part_search_record"""
        pass


class TestAssessmentPartSearchResults(unittest.TestCase):
    """Tests for AssessmentPartSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_assessment_parts(self):
        """Tests get_assessment_parts"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_part_query_inspector(self):
        """Tests get_assessment_part_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_part_search_results_record(self):
        """Tests get_assessment_part_search_results_record"""
        pass


class TestSequenceRuleSearch(unittest.TestCase):
    """Tests for SequenceRuleSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_sequence_rules(self):
        """Tests search_among_sequence_rules"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_sequence_rule_results(self):
        """Tests order_sequence_rule_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_sequence_rule_search_record(self):
        """Tests get_sequence_rule_search_record"""
        pass


class TestSequenceRuleSearchResults(unittest.TestCase):
    """Tests for SequenceRuleSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_sequence_rules(self):
        """Tests get_sequence_rules"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_sequence_rule_query_inspector(self):
        """Tests get_sequence_rule_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_sequence_rule_search_results_record(self):
        """Tests get_sequence_rule_search_results_record"""
        pass


