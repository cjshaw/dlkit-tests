"""Unit tests of assessment.authoring objects."""

import unittest


class TestAssessmentPart(unittest.TestCase):
    """Tests for AssessmentPart"""



    @unittest.skip('unimplemented test')
    def test_get_assessment_id(self):
        """Tests get_assessment_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment(self):
        """Tests get_assessment"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_parent_part(self):
        """Tests has_parent_part"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_part_id(self):
        """Tests get_assessment_part_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_part(self):
        """Tests get_assessment_part"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_section(self):
        """Tests is_section"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_weight(self):
        """Tests get_weight"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_allocated_time(self):
        """Tests get_allocated_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_child_assessment_part_ids(self):
        """Tests get_child_assessment_part_ids"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_child_assessment_parts(self):
        """Tests get_child_assessment_parts"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_part_record(self):
        """Tests get_assessment_part_record"""
        pass


class TestAssessmentPartForm(unittest.TestCase):
    """Tests for AssessmentPartForm"""



    @unittest.skip('unimplemented test')
    def test_get_weight_metadata(self):
        """Tests get_weight_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_weight(self):
        """Tests set_weight"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_weight(self):
        """Tests clear_weight"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_allocated_time_metadata(self):
        """Tests get_allocated_time_metadata"""
        pass

    def test_set_allocated_time(self):
        """Tests set_allocated_time"""
       

    @unittest.skip('unimplemented test')
    def test_clear_allocated_time(self):
        """Tests clear_allocated_time"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_part_form_record(self):
        """Tests get_assessment_part_form_record"""
        pass


class TestAssessmentPartList(unittest.TestCase):
    """Tests for AssessmentPartList"""

    @unittest.skip('unimplemented test')
    def test_get_next_assessment_part(self):
        """Tests get_next_assessment_part"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_assessment_parts(self):
        """Tests get_next_assessment_parts"""
        pass


class TestSequenceRule(unittest.TestCase):
    """Tests for SequenceRule"""



    @unittest.skip('unimplemented test')
    def test_get_assessment_part_id(self):
        """Tests get_assessment_part_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_assessment_part(self):
        """Tests get_assessment_part"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_assessment_part_id(self):
        """Tests get_next_assessment_part_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_assessment_part(self):
        """Tests get_next_assessment_part"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_minimum_score(self):
        """Tests get_minimum_score"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_score(self):
        """Tests get_maximum_score"""
        pass

    @unittest.skip('unimplemented test')
    def test_is_cumulative(self):
        """Tests is_cumulative"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_applied_assessment_part_ids(self):
        """Tests get_applied_assessment_part_ids"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_applied_assessment_parts(self):
        """Tests get_applied_assessment_parts"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_sequence_rule_record(self):
        """Tests get_sequence_rule_record"""
        pass


class TestSequenceRuleForm(unittest.TestCase):
    """Tests for SequenceRuleForm"""

    @unittest.skip('unimplemented test')
    def test_get_minimum_score_metadata(self):
        """Tests get_minimum_score_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_minimum_score(self):
        """Tests set_minimum_score"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_maximum_score_metadata(self):
        """Tests get_maximum_score_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_maximum_score(self):
        """Tests set_maximum_score"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_cumulative_metadata(self):
        """Tests get_cumulative_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_cumulative(self):
        """Tests set_cumulative"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_applied_assessment_parts_metadata(self):
        """Tests get_applied_assessment_parts_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_apply_assessment_parts(self):
        """Tests apply_assessment_parts"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_sequence_rule_form_record(self):
        """Tests get_sequence_rule_form_record"""
        pass


class TestSequenceRuleList(unittest.TestCase):
    """Tests for SequenceRuleList"""

    @unittest.skip('unimplemented test')
    def test_get_next_sequence_rule(self):
        """Tests get_next_sequence_rule"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_sequence_rules(self):
        """Tests get_next_sequence_rules"""
        pass


