"""Unit tests of type objects."""


import unittest


class TestTypeForm(unittest.TestCase):
    """Tests for TypeForm"""


    @unittest.skip('unimplemented test')
    def test_get_display_name_metadata(self):
        """Tests get_display_name_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_display_name(self):
        """Tests set_display_name"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_display_name(self):
        """Tests clear_display_name"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_display_label_metadata(self):
        """Tests get_display_label_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_display_label(self):
        """Tests set_display_label"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_display_label(self):
        """Tests clear_display_label"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_description_metadata(self):
        """Tests get_description_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_description(self):
        """Tests set_description"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_description(self):
        """Tests clear_description"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_domain_metadata(self):
        """Tests get_domain_metadata"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_domain(self):
        """Tests set_domain"""
        pass

    @unittest.skip('unimplemented test')
    def test_clear_domain(self):
        """Tests clear_domain"""
        pass


class TestTypeList(unittest.TestCase):
    """Tests for TypeList"""

    @unittest.skip('unimplemented test')
    def test_get_next_type(self):
        """Tests get_next_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_types(self):
        """Tests get_next_types"""
        pass


