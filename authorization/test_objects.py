"""Unit tests of authorization objects."""

import unittest


class TestAuthorization(unittest.TestCase):
    """Tests for Authorization"""



    @unittest.skip('unimplemented test')
    def test_is_implicit(self):
        """Tests is_implicit"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_resource(self):
        """Tests has_resource"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_resource_id(self):
        """Tests get_resource_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_resource(self):
        """Tests get_resource"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_trust(self):
        """Tests has_trust"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_trust_id(self):
        """Tests get_trust_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_trust(self):
        """Tests get_trust"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_agent(self):
        """Tests has_agent"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_agent_id(self):
        """Tests get_agent_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_agent(self):
        """Tests get_agent"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_function_id(self):
        """Tests get_function_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_function(self):
        """Tests get_function"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_qualifier_id(self):
        """Tests get_qualifier_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_qualifier(self):
        """Tests get_qualifier"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_authorization_record(self):
        """Tests get_authorization_record"""
        pass


class TestAuthorizationForm(unittest.TestCase):
    """Tests for AuthorizationForm"""



    @unittest.skip('unimplemented test')
    def test_get_authorization_form_record(self):
        """Tests get_authorization_form_record"""
        pass


class TestAuthorizationList(unittest.TestCase):
    """Tests for AuthorizationList"""

    @unittest.skip('unimplemented test')
    def test_get_next_authorization(self):
        """Tests get_next_authorization"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_authorizations(self):
        """Tests get_next_authorizations"""
        pass


class TestVault(unittest.TestCase):
    """Tests for Vault"""



    @unittest.skip('unimplemented test')
    def test_get_vault_record(self):
        """Tests get_vault_record"""
        pass


class TestVaultForm(unittest.TestCase):
    """Tests for VaultForm"""



    @unittest.skip('unimplemented test')
    def test_get_vault_form_record(self):
        """Tests get_vault_form_record"""
        pass


class TestVaultList(unittest.TestCase):
    """Tests for VaultList"""

    @unittest.skip('unimplemented test')
    def test_get_next_vault(self):
        """Tests get_next_vault"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_vaults(self):
        """Tests get_next_vaults"""
        pass


class TestVaultNode(unittest.TestCase):
    """Tests for VaultNode"""

    @unittest.skip('unimplemented test')
    def test_get_vault(self):
        """Tests get_vault"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_parent_vault_nodes(self):
        """Tests get_parent_vault_nodes"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_child_vault_nodes(self):
        """Tests get_child_vault_nodes"""
        pass


class TestVaultNodeList(unittest.TestCase):
    """Tests for VaultNodeList"""

    @unittest.skip('unimplemented test')
    def test_get_next_vault_node(self):
        """Tests get_next_vault_node"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_next_vault_nodes(self):
        """Tests get_next_vault_nodes"""
        pass


