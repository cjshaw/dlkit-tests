"""Unit tests of authorization searches."""

import unittest


class TestAuthorizationSearch(unittest.TestCase):
    """Tests for AuthorizationSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_authorizations(self):
        """Tests search_among_authorizations"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_authorization_results(self):
        """Tests order_authorization_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_authorization_search_record(self):
        """Tests get_authorization_search_record"""
        pass


class TestAuthorizationSearchResults(unittest.TestCase):
    """Tests for AuthorizationSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_authorizations(self):
        """Tests get_authorizations"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_authorization_query_inspector(self):
        """Tests get_authorization_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_authorization_search_results_record(self):
        """Tests get_authorization_search_results_record"""
        pass


class TestVaultSearch(unittest.TestCase):
    """Tests for VaultSearch"""

    @unittest.skip('unimplemented test')
    def test_search_among_vaults(self):
        """Tests search_among_vaults"""
        pass

    @unittest.skip('unimplemented test')
    def test_order_vault_results(self):
        """Tests order_vault_results"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_vault_search_record(self):
        """Tests get_vault_search_record"""
        pass


class TestVaultSearchResults(unittest.TestCase):
    """Tests for VaultSearchResults"""

    @unittest.skip('unimplemented test')
    def test_get_vaults(self):
        """Tests get_vaults"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_vault_query_inspector(self):
        """Tests get_vault_query_inspector"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_vault_search_results_record(self):
        """Tests get_vault_search_results_record"""
        pass


