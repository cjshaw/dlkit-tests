"""Unit tests of proxy managers."""


import unittest
from dlkit_runtime import PROXY_SESSION, proxy_example
from dlkit_runtime.managers import Runtime
REQUEST = proxy_example.TestRequest()
CONDITION = PROXY_SESSION.get_proxy_condition()
CONDITION.set_http_request(REQUEST)
PROXY = PROXY_SESSION.get_proxy(CONDITION)

from dlkit.primordium.type.primitives import Type
DEFAULT_TYPE = Type(**{'identifier': 'DEFAULT', 'namespace': 'DEFAULT', 'authority': 'DEFAULT',})

from dlkit.abstract_osid.type.objects import TypeList as abc_type_list
from dlkit.abstract_osid.osid import errors


class TestProxyProfile(unittest.TestCase):
    """Tests for ProxyProfile"""

    @classmethod
    def setUpClass(cls):
        cls.mgr = Runtime().get_service_manager('PROXY', proxy=PROXY, implementation='TEST_SERVICE')

    def test_supports_proxy(self):
        """Tests supports_proxy"""
        self.assertTrue(isinstance(self.mgr.supports_proxy(), bool))

    def test_get_proxy_record_types(self):
        """Tests get_proxy_record_types"""
        self.assertTrue(isinstance(self.mgr.get_proxy_record_types(), abc_type_list))

    def test_get_proxy_condition_record_types(self):
        """Tests get_proxy_condition_record_types"""
        self.assertTrue(isinstance(self.mgr.get_proxy_condition_record_types(), abc_type_list))


class TestProxyManager(unittest.TestCase):
    """Tests for ProxyManager"""


    def test_get_proxy_session(self):
        """Tests get_proxy_session"""
        if self.mgr.supports_proxy():
            self.mgr.get_proxy_session()


class TestProxyProxyManager(unittest.TestCase):
    """Tests for ProxyProxyManager"""


    def test_get_proxy_session(self):
        """Tests get_proxy_session"""
        if self.mgr.supports_proxy():
            self.mgr.get_proxy_session(PROXY)
        with self.assertRaises(errors.NullArgument):
            self.mgr.get_proxy_session()


