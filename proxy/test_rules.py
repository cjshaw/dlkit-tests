"""Unit tests of proxy rules."""


import unittest


class TestProxy(unittest.TestCase):
    """Tests for Proxy"""


    @unittest.skip('unimplemented test')
    def test_has_authentication(self):
        """Tests has_authentication"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_authentication(self):
        """Tests get_authentication"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_effective_agent(self):
        """Tests has_effective_agent"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_effective_agent_id(self):
        """Tests get_effective_agent_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_effective_agent(self):
        """Tests get_effective_agent"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_effective_date(self):
        """Tests has_effective_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_effective_date(self):
        """Tests get_effective_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_effective_clock_rate(self):
        """Tests get_effective_clock_rate"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_locale(self):
        """Tests get_locale"""
        pass

    @unittest.skip('unimplemented test')
    def test_has_format_type(self):
        """Tests has_format_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_format_type(self):
        """Tests get_format_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_get_proxy_record(self):
        """Tests get_proxy_record"""
        pass


class TestProxyCondition(unittest.TestCase):
    """Tests for ProxyCondition"""


    @unittest.skip('unimplemented test')
    def test_set_effective_agent_id(self):
        """Tests set_effective_agent_id"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_effective_date(self):
        """Tests set_effective_date"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_language_type(self):
        """Tests set_language_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_script_type(self):
        """Tests set_script_type"""
        pass

    @unittest.skip('unimplemented test')
    def test_set_calendar_type(self):
        """Tests set_calendar_type"""
        pass

    def test_set_time_type(self):
        """Tests set_time_type"""
        self._time_type = time_type

    def test_set_currency_type(self):
        """Tests set_currency_type"""
        self._currency_type = currency_type

    def test_set_unit_system_type(self):
        """Tests set_unit_system_type"""
        self._unit_system_type = unit_system_type

    def test_set_format_type(self):
        """Tests set_format_type"""
        self._format_type = format_type

    def test_get_proxy_condition_record(self):
        """Tests get_proxy_condition_record"""
        if self.has_record_type(proxy_condition_type):
            return self
        else:
            raise errors.Unsupported()

    def set_http_request(self, http_request):
        """Support the HTTPRequest ProxyConditionRecordType and checks for special effective agent ids"""
        self._http_request = http_request
        if 'HTTP_LTI_USER_ID' in http_request.META:
            try:
                authority = http_request.META['HTTP_LTI_TOOL_CONSUMER_INSTANCE_GUID']
            except (AttributeError, KeyError):
                authority = 'unknown_lti_consumer_instance'
            self.set_effective_agent_id(Id(
                authority=authority,
                namespace='agent.Agent',
                identifier=http_request.META['HTTP_LTI_USER_ID']))

    http_request = property(fset=set_http_request)


